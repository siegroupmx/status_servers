$(document).ready(function(){
	$('#nomina_nomina').click(function(event){
		$('.nomina_empleados').css("display", "none");
		$('.nomina_extras').css("display", "none");
		$('.nomina_depto').css("display", "none");
		$('.nomina_catalogo').css("display", "none");
		$('.nomina_ecomer').css("display", "none");
		$('.nomina_nomina').fadeIn("slow");
	});
	$('#nomina_empleados').click(function(event){
		$('.nomina_extras').css("display", "none");
		$('.nomina_nomina').css("display", "none");
		$('.nomina_depto').css("display", "none");
		$('.nomina_catalogo').css("display", "none");
		$('.nomina_ecomer').css("display", "none");
		$('.nomina_empleados').fadeIn("slow");
	});
	$('#nomina_depto').click(function(event){
		$('.nomina_empleados').css("display", "none");
		$('.nomina_nomina').css("display", "none");
		$('.nomina_extras').css("display", "none");
		$('.nomina_catalogo').css("display", "none");
		$('.nomina_ecomer').css("display", "none");
		$('.nomina_depto').fadeIn("slow");
	});
	$('#nomina_ecomer').click(function(event){
		$('.nomina_empleados').css("display", "none");
		$('.nomina_nomina').css("display", "none");
		$('.nomina_extras').css("display", "none");
		$('.nomina_catalogo').css("display", "none");
		$('.nomina_depto').css("display", "none");
		$('.nomina_ecomer').fadeIn("slow");
	});
	$('#nomina_catalogo').click(function(event){
		$('.nomina_empleados').css("display", "none");
		$('.nomina_nomina').css("display", "none");
		$('.nomina_extras').css("display", "none");
		$('.nomina_depto').css("display", "none");
		$('.nomina_ecomer').css("display", "none");
		$('.nomina_catalogo').fadeIn("slow");
	});
	$('#nomina_extras').click(function(event){
		$('.nomina_empleados').css("display", "none");
		$('.nomina_nomina').css("display", "none");
		$('.nomina_depto').css("display", "none");
		$('.nomina_catalogo').css("display", "none");
		$('.nomina_ecomer').css("display", "none");
		$('.nomina_extras').fadeIn("slow");
	});
});