<?php
function comprimir_web($buffer) {
    $busca = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
    $reemplaza = array('>','<','\\1');
    return preg_replace($busca, $reemplaza, $buffer); 
}

ob_start("comprimir_web"); # calcular peso web y compresion
session_start();
date_default_timezone_set("America/El_Salvador");
include("libs/config.php");
include("libs/base.php");
include("libs/funciones.php");
include("libs/recaptcha.php");
include("libs/UsO_tricks.php");

echo '<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">';

define('HTTPS', false);
$home= (HTTPS ? 'https://':'http://').$_SERVER['HTTP_HOST'];
$ref= (HTTPS ? 'https://':'http://').$_SERVER['HTTP_REFERENCE'];
$ip= get_ip();
$mhServer= array(
	"prod"=>array(
		"mh_send"=>array("sandbox"=>false, "service"=>"Envio de Documentos", "url"=>"https://api.dtes.mh.gob.sv/fesv/recepciondte", "id"=>1), 
		"mh_cancel"=>array("sandbox"=>false, "service"=>"Cancelaciones", "url"=>"https://api.dtes.mh.gob.sv/fesv/anulardte", "id"=>2), 
		"mh_contingencia"=>array("sandbox"=>false, "service"=>"Contingencia","url"=>"https://api.dtes.mh.gob.sv/fesv/contingencia", "id"=>3), 
		"mh_consulta"=>array("sandbox"=>false, "service"=>"Consulta de Documentos", "url"=>"https://api.dtes.mh.gob.sv/fesv/recepcion/consultadte", "id"=>4)
		// "mh_firma"=>array("sandbox"=>false, "service"=>"Autenticacion de Token", "url"=>"https://api.dtes.mh.gob.sv/seguridad/auth", "id"=>5)
	), 
	"sandbox"=>array(
		"mh_send_sandbox"=>array("sandbox"=>true, "service"=>"Envio de Documentos", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/recepciondte", "id"=>5), 
		"mh_cancel_sandbox"=>array("sandbox"=>true, "service"=>"Cancelaciones", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/anulardte", "id"=>6), 
		"mh_contingencia_sandbox"=>array("sandbox"=>true, "service"=>"Contingencia", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/contingencia", "id"=>7), 
		"mh_consulta_sandbox"=>array("sandbox"=>true, "service"=>"Consulta de Documentos", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/recepcion/consultadte", "id"=>8)
		// "mh_firma_sandbox"=>array("sandbox"=>true, "service"=>"Autenticacion de Token", "url"=>"https://apitest.dtes.mh.gob.sv/seguridad/auth", "id"=>1)
	)
);

if( !strcmp($_GET["id"], "post") ) {
	if( !strcmp($_GET["op"], "list_servers") ) {
		if( strcmp($_POST["server_type"], "1") && strcmp($_POST["server_type"], "2") )
			echo '<div class="w3-panel w3-padding w3-leftbar w3-border-red w3-pale-red w3-left-align">Tipo de servidor invalido..</div>';
		else {
			$tipo= (!strcmp($_POST["server_type"], "1") ? "prod":"sandbox");

			echo '<select class="w3-input" name="server_name" id="server_name">
				<option value="0">-- Subtipo Servidor</option>';
			foreach( $mhServer[$tipo] as $k=>$v ) {
				echo '<option value="'. $k. '">'. $v["service"]. '</option>';
			}
			echo '</select>';
			unset($tipo);
		}
	}
	else if( !strcmp($_GET["op"], "new") ) {
		$gl= new googleService();

		echo '
		<h4 class="w3-border-0 w3-border-black w3-border-bottom w3-text-orange">Nuevo Mensaje</h4>
		<div id="new_post_inaction"></div>
		<div class="w3-row" id="formPost">
			<div class="w3-row">
				<div class="w3-half">
					<select class="w3-input" name="server_type" id="server_type" onchange="cargar_datos(\'id=post&op=list_servers\', \'server_list_post_action\', \'POST\', \'server_type\');">
						<option value="0">-- Tipo de Servidor reportar</option>
						<option value="1">Servidores Producci&oacute;n</option>
						<option value="2">Servidores Prueba</option>
					</select>
				</div>
				<div class="w3-half" id="server_list_post_action">
					<select class="w3-input" name="server_name" id="server_name" disabled>
						<option value="0">-- Subtipo Servidor</option>
					</select>
				</div>
			</div>
			<div class="w3-row">
				<input type="text" class="w3-input w3-half" name="nombre" id="nombre" placeholder="Inidque su nombre">
				<input type="email" class="w3-input w3-half" name="mail" id="mail" placeholder="Inidque su email">
			</div>
			<div class="w3-row w3-margin-bottom">
				<textarea class="w3-input" name="mensaje" id="mensaje" placeholder="Inidque su mensaje"></textarea>
			</div>
			<div class="w3-row">
				<button id="formPostData" class="w3-btn w3-blue w3-small w3-right w3-round-large mar5l">Publicar</button>
				<button class="w3-btn w3-red w3-small w3-right w3-round-large" onclick="cargar_datos(\'id=post&op=clean\', \'new_post\', \'GET\', \'0\');">Cancelar</button>
			</div>
		</div>
		<script>
		$(document).ready(function(){
			$(\'#formPostData\').click(function(){
				grecaptcha.ready(function() {
					grecaptcha.execute(\''. $gl->getRecaptchaKey(). '\', {action: \'submit\'}).then(function(token) {
						let form= document.getElementById(\'formPost\');
						let input= document.createElement(\'input\');
						let recaptchaButton= document.getElementById(\'g-recaptcha-response\');
						input.type= \'hidden\';
						input.name= \'g-recaptcha-response\';
						input.id= \'g-recaptcha-response\';
						input.value= token;

						if( document.body.contains(recaptchaButton) ) {
							recaptchaButton.remove();
							form.appendChild(input);
						}
						else {
							form.appendChild(input);
						}

						cargar_datos(\'id=post&op=save\', \'new_post_inaction\', \'POST\', \'nombre:mail:mensaje:server_type:server_name:g-recaptcha-response\');
					});
				});
			});
		});
		</script>
		';
	}
	else if( !strcmp($_GET["op"], "clean") ) {
	}
	else if( !strcmp($_GET["op"], "save") ) {
		$gl= new googleService($_POST["g-recaptcha-response"]);
		$gl->setServiceType("recaptcha");
		$gl->sendRecaptcha();

		if( $gl->getError() ) {
			echo '<div class="w3-panel w3-padding w3-leftbar w3-border-red w3-pale-red w3-left-align">La captcha fallo :(..</div>';
		}
		else {
			$patron='/([a-zA-Z0-9\-\.\_]{2,})\@([a-zA-Z0-9\-\.\_]{2,})/is';
			$ultimoPost= consultar_datos_general("POST", "IP='". proteger_cadena($ip). "' ORDER BY FECHA DESC", "FECHA");
			$canAddPost= (strcmp(date("Y-m-d", time()), date("Y-m-d", $ultimoPost)) ? true:false);
			$tipo= (!strcmp($_POST["server_type"], "1") ? "prod":"sandbox");

			if( !$_POST["nombre"] )
				echo '<div class="w3-panel w3-padding w3-leftbar w3-border-red w3-pale-red w3-left-align">No indico su nombre..</div>';
			else if( !$_POST["mail"] )
				echo '<div class="w3-panel w3-padding w3-leftbar w3-border-red w3-pale-red w3-left-align">No indico su mail..</div>';
			else if( !preg_match($patron, $_POST["mail"]) )
				echo '<div class="w3-panel w3-padding w3-leftbar w3-border-red w3-pale-red w3-left-align">No indico un mail valido..</div>';
			else if( !$_POST["mensaje"] )
				echo '<div class="w3-panel w3-padding w3-leftbar w3-border-red w3-pale-red w3-left-align">No indico su mensaje..</div>';
			else if( !$canAddPost )
				echo '<div class="w3-panel w3-padding w3-leftbar w3-border-red w3-pale-red w3-left-align">Solo puedes publicar un mensaje por d&iacute;a..</div>';
			else if( !$_POST["server_type"] || !count($mhServer[$tipo]) )
				echo '<div class="w3-panel w3-padding w3-leftbar w3-border-red w3-pale-red w3-left-align">No indico un tipo de servidor valido..</div>';
			else if( !$_POST["server_name"] || !count($mhServer[$tipo][$_POST["server_name"]]) )
				echo '<div class="w3-panel w3-padding w3-leftbar w3-border-red w3-pale-red w3-left-align">No indico un subtipo de servidor valido..</div>';
			else {
				$jsonData= json_encode(
					array(
						"server"=>$tipo, 
						"subserver"=>proteger_cadena($_POST["server_name"])
					)
				);
				$trama= array(
					"nombre"=>"'". proteger_cadena($_POST["nombre"]). "'", 
					"mail"=>"'". proteger_cadena($_POST["mail"]). "'", 
					"mensaje"=>"'". proteger_cadena($_POST["mensaje"]). "'", 
					"ip"=>"'". proteger_cadena($ip). "'", 
					"fecha"=>"'". proteger_cadena(time()). "'",
					"status"=>"'1'", 
					"megusta"=>"'0'", 
					"no_megusta"=>"'0'", 
					"data_json"=>"'". proteger_cadena($jsonData). "'"
				);

				if( !insertar_bdd("POST", $trama) )
					echo '<div class="w3-panel w3-padding w3-leftbar w3-border-red w3-pale-red w3-left-align">Problemas para guardar mensaje..</div>';
				else {
					echo '<div class="w3-panel w3-padding w3-leftbar w3-border-green w3-pale-green w3-left-align">Incidencia guardada con exito..</div>';
					uso_reload(2200);
				}
			}
			unset($patron, $ultimoPost, $canAddPost, $tipo);
		}
	}
	else if( !strcmp($_GET["op"], "list") ) {
	}
}

ob_end_flush(); # fin objeto
?>