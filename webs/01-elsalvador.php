<?php
function comprimir_web($buffer) {
    $busca = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s');
    $reemplaza = array('>','<','\\1');
    return preg_replace($busca, $reemplaza, $buffer); 
}

ob_start("comprimir_web"); # calcular peso web y compresion
session_start();
date_default_timezone_set("America/El_Salvador");
include("libs/config.php");
include("libs/base.php");
include("libs/funciones.php");
include("libs/recaptcha.php");
include("libs/UsO_tricks.php");

define('HTTPS', true);
$home= (HTTPS ? 'https://':'http://').$_SERVER['HTTP_HOST'];
$ip= get_ip();
$mhServer= array(
	"prod"=>array(
		"mh_send"=>array("sandbox"=>false, "service"=>"Envio de Documentos", "url"=>"https://api.dtes.mh.gob.sv/fesv/recepciondte", "id"=>1), 
		"mh_cancel"=>array("sandbox"=>false, "service"=>"Cancelaciones", "url"=>"https://api.dtes.mh.gob.sv/fesv/anulardte", "id"=>2), 
		"mh_contingencia"=>array("sandbox"=>false, "service"=>"Contingencia","url"=>"https://api.dtes.mh.gob.sv/fesv/contingencia", "id"=>3), 
		"mh_consulta"=>array("sandbox"=>false, "service"=>"Consulta de Documentos", "url"=>"https://api.dtes.mh.gob.sv/fesv/recepcion/consultadte", "id"=>4)
		// "mh_firma"=>array("sandbox"=>false, "service"=>"Autenticacion de Token", "url"=>"https://api.dtes.mh.gob.sv/seguridad/auth", "id"=>5)
	), 
	"sandbox"=>array(
		"mh_send_sandbox"=>array("sandbox"=>true, "service"=>"Envio de Documentos", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/recepciondte", "id"=>5), 
		"mh_cancel_sandbox"=>array("sandbox"=>true, "service"=>"Cancelaciones", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/anulardte", "id"=>6), 
		"mh_contingencia_sandbox"=>array("sandbox"=>true, "service"=>"Contingencia", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/contingencia", "id"=>7), 
		"mh_consulta_sandbox"=>array("sandbox"=>true, "service"=>"Consulta de Documentos", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/recepcion/consultadte", "id"=>8)
		// "mh_firma_sandbox"=>array("sandbox"=>true, "service"=>"Autenticacion de Token", "url"=>"https://apitest.dtes.mh.gob.sv/seguridad/auth", "id"=>1)
	)
);
$mhServer2= array(
	1=>array("sandbox"=>false, "service"=>"Envio de Documentos", "url"=>"https://api.dtes.mh.gob.sv/fesv/recepciondte", "id"=>1, "tagname"=>"mh_send"), 
	2=>array("sandbox"=>false, "service"=>"Cancelaciones", "url"=>"https://api.dtes.mh.gob.sv/fesv/anulardte", "id"=>2, "tagname"=>"mh_cancel"), 
	3=>array("sandbox"=>false, "service"=>"Contingencia","url"=>"https://api.dtes.mh.gob.sv/fesv/contingencia", "id"=>3, "tagname"=>"mh_contingencia"), 
	4=>array("sandbox"=>false, "service"=>"Consulta de Documentos", "url"=>"https://api.dtes.mh.gob.sv/fesv/recepcion/consultadte", "id"=>4, "tagname"=>"mh_consulta"),
	5=>array("sandbox"=>true, "service"=>"Envio de Documentos", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/recepciondte", "id"=>5, "tagname"=>"mh_send_sandbox"), 
	6=>array("sandbox"=>true, "service"=>"Cancelaciones", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/anulardte", "id"=>6, "tagname"=>"mh_cancel_sandbox"), 
	7=>array("sandbox"=>true, "service"=>"Contingencia", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/contingencia", "id"=>7, "tagname"=>"mh_contingencia_sandbox"), 
	8=>array("sandbox"=>true, "service"=>"Consulta de Documentos", "url"=>"https://apitest.dtes.mh.gob.sv/fesv/recepcion/consultadte", "id"=>8, "tagname"=>"mh_consulta_sandbox")
);

echo '
<!DOCTYPE html>
<html>
	<head>
		<title>Status Servidores Ministerio de Haciendo en El Salvador - www.facturaelectronica.sv</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="robots" content="index, follow, max-image-preview:none, max-snippet:-1, max-video-preview:-1, noimageindex" />
		<meta name="description" content="estatus y monitoreo de servidores ministerio de hacienda en el salvador" />
		<link rel="canonical" href="https://status.facturaelectronica.sv/" />
		<meta property="og:locale" content="es_ES" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="estatus y monitoreo de servidores ministerio de hacienda en el salvador" />
		<meta property="og:description" content="estatus y monitoreo de servidores ministerio de hacienda en el salvador" />
		<meta property="og:url" content="https://www.facturaelectronica.sv/" />
		<meta property="og:site_name" content="estatus y monitoreo de servidores ministerio de hacienda en el salvador" />
		<meta property="article:publisher" content="https://www.facebook.com/profile.php?id=100090402546299" />
		<meta property="article:modified_time" content="2024-04-21T03:14:21+00:00" />
		<meta property="og:image" content="https://status.facturaelectronica.sv/logo.png" />
		<meta property="og:image:width" content="512" />
		<meta property="og:image:height" content="512" />
		<meta property="og:image:type" content="image/png" />
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:title" content="estatus y monitoreo de servidores ministerio de hacienda en el salvador" />
		<meta name="twitter:description" content="estatus y monitoreo de servidores ministerio de hacienda en el salvador" />
		<meta name="twitter:image" content="https://status.facturaelectronica.sv/logo.png" />
		<meta name="twitter:site" content="@moneyboxsv" />
		<meta content="Divi v.4.25.0" name="generator"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<meta name="msapplication-TileImage" content="https://status.facturaelectronica.sv/logo.png" />
		<script type="text/javascript" src="'. $home. '/webs/js/jquery.js"></script>
		<script type="text/javascript" src="'. $home. '/webs/js/script.js"></script>
		<script src="https://www.google.com/recaptcha/api.js?render=6Le7-6wkAAAAAHLeOxi5exh9XwSO6_d6FRRH_2LH"></script>
		<link href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined" rel="stylesheet"/>
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<style>
			<!--
			.nd{text-decoration:none;}
			.mar5l{margin-left:5px;}
			.mar5r{margin-right:5px;}
			//-->
		</style>
	</head>

	<body class="w3-light-grey">
		<div class="w3-bar w3-border w3-light-grey">
			<a href="'. $home. '" class="w3-bar-item w3-button"><img class="w3-left" src="'. $home. '/webs/logo.png" style="width:20px;"></a>
			<a href="'. $home. '" class="w3-bar-item w3-button">Home</a>
			<a href="'. $home. '/?id=about" class="w3-bar-item w3-button">Acerca de..</a>
			<a href="https://www.facturaelectronica.sv" target="_blank" class="w3-bar-item w3-right w3-button">facturaelectronica.sv</a>
		</div>
		<div class="w3-container">
			<div class="w3-panel w3-pale-yellow w3-padding">
				Para mayor informaci&oacute;n sobre los estatus del servidor y compartir otros datos de mayor relevancia unete a nuestro grupo de whtsapp: <a href="https://chat.whatsapp.com/JmmFytCD0tF2o62snx2Tjb" target="_blank"><b>https://chat.whatsapp.com/JmmFytCD0tF2o62snx2Tjb</b></a>
			</div>
		</div>
		<div class="w3-container w3-padding">';

	if( !strcmp($_GET["id"], "about") ) {
		echo '
		Este sitio web es un monitor <b>no oficial</b> de los servidores del Ministerio de Hacienda en El Salvador, con la finalidad de presentar a los usuarios de emision de DTS\'s mayor claridad en el estatus e historico del funcionamiento de los mismos.<br><br>Este servicio es gratuito y no tiene fines de lucro.<br><br>Att. Moneybox El Salvador SA de CV - <a href="https://www.facturaelectronica.sv" target="_blank">https://www.facturaelectronica.sv</a>
		';
	}
	else {
		echo '
		<div class="w3-container w3-white w3-border w3-padding w3-margin-bottom">
			<h4><span class="material-symbols-outlined w3-medium">dns</span> Servidores de Producci&oacute;n</h4>
			<div class="w3-container">';
			foreach( $mhServer["prod"] as $k=>$v ) {
				$estado= consultar_datos_general("SERVER_TARGETS","TAGNAME='". $k. "'", "STATUS");
				$ticketsTotal=0;
				$ticketsSolved=0;
				$ticketsOpen=0;
				$bar= ($estado ? 'w3-green':'w3-red');
				$colorIcon= ($estado ? 'w3-blue':'w3-red');

				echo '
				<div class="w3-row w3-margin-bottom">
					<div class="w3-row">
						<div class="w3-third w3-col s12 m6 l4">'. $v["service"]. ' - '. $v["url"]. '</div>
						<div class="w3-third w3-right-align w3-small w3-text-grey w3-col s12 m6 l4">
						 	'. $ticketsTotal. ' reportes / '. $ticketsSolved. ' resueltos / '. $ticketsOpen. ' abiertos
						</div>
						<div class="w3-third w3-right-align w3-col s12 m6 l4">
							'. ($estado ? 'En l&iacute;nea':'Ca&iacute;do :\'('). '
							<span class="material-symbols-outlined w3-small '.$colorIcon.' w3-round-large mar5l" style="padding:2px;">done</span>
						</div>
					</div>
					<div class="w3-row w3-col s12 m12 l12">
						<div class="w3-border">
							<div class="'. $bar. '" style="height:6px;width:100%"></div>
						</div>
					</div>
				</div>
				';
				unset($estado, $ticketsTotal, $ticketsSolved, $ticketsOpen);
			}
			echo '
			</div>
		</div>

		<div class="w3-container w3-white w3-border w3-padding w3-margin-bottom">
			<h4><span class="material-symbols-outlined w3-medium">dns</span> Servidores de Pruebas</h4>
			<div class="w3-container">';
			foreach( $mhServer["sandbox"] as $k=>$v ) {
				$estado= consultar_datos_general("SERVER_TARGETS","TAGNAME='". $k. "'", "STATUS");
				$ticketsTotal=0;
				$ticketsSolved=0;
				$ticketsOpen=0;
				$bar= ($estado ? 'w3-green':'w3-red');
				$colorIcon= ($estado ? 'w3-blue':'w3-red');

				echo '
				<div class="w3-row w3-margin-bottom">
					<div class="w3-row">
						<div class="w3-third w3-col s12 m6 l4">'. $v["service"]. ' - '. $v["url"]. '</div>
						<div class="w3-third w3-right-align w3-small w3-text-grey w3-col s12 m6 l4">
						 	'. $ticketsTotal. ' reportes / '. $ticketsSolved. ' resueltos / '. $ticketsOpen. ' abiertos
						</div>
						<div class="w3-third w3-right-align w3-col s12 m6 l4">
							'. ($estado ? 'En linea':'Caido :\'('). '
							<span class="material-symbols-outlined w3-small '.$colorIcon.' w3-round-large mar5l" style="padding:2px;">done</span>
						</div>
					</div>
					<div class="w3-row w3-col s12 m12 l12">
						<div class="w3-border">
							<div class="'. $bar. '" style="height:6px;width:100%"></div>
						</div>
					</div>
				</div>
				';
				unset($estado, $ticketsTotal, $ticketsSolved, $ticketsOpen);
			}
			echo '
			</div>
		</div>
		';

	$cons= consultar_enorden("POST", "FECHA DESC");
	$mes= date("m", time());
	$year= date("Y", time());
	$fInicio= strtotime(date("Y-m", time()).'-01T12:00:00');
	$fFin= strtotime(($mes==12 ? ($year+1):$year).'-'.($mes==12 ? '01':($mes+1)).'-01T12:00:00');
	$consBot= consultar_limite_enorden("BOT_INCIDENT", "0,30", "FECHA DESC");

	echo '
		<div class="w3-row">
			<div class="w3-half w3-col s12 m12 l6">
				<div class="w3-container w3-white w3-border w3-padding">
					<h4>
						<span class="material-symbols-outlined w3-medium">chat</span> Mensajes de Incidencias
						<div class="w3-right w3-button w3-light-gray w3-round-large w3-small" onclick="cargar_datos(\'id=post&op=new\', \'new_post\', \'GET\', \'0\');">Crear Incidencia</div>
					</h4>
					<div class="w3-container">
					<div id="new_post"></div>
					';

				if( !mysqli_num_rows($cons) ) {
					echo ' no hay reporte de los usuarios..';
				}
				else {	
					while( $buf=mysqli_fetch_array($cons) ) {
						if( !$buf["DATA_JSON"] ) {
							$tag1= 'none';
							$tag2= 'none';
						}
						else {
							$jsonData= json_decode(desproteger_cadena($buf["DATA_JSON"]));
							$tag1= $jsonData->server;
							$tag2= $jsonData->subserver;
						}

						echo '
						<div class="w3-row w3-margin-bottom w3-col w12 l12 m12">
							<div class="w3-row w3-small w3-padding-small w3-col w12 l12 m12">
								'. desproteger_cadena($buf["MENSAJE"]). '
							</div>
							<div class="w3-row w3-right-align w3-light-gray w3-small w3-padding-small w3-col w12 l12 m12">
								<div class="w3-half w3-left-align">
									<div class="w3-blue w3-left w3-tiny w3-round-large" style="padding:5px;">'. desproteger_cadena($tag1). '</div>
									<div class="w3-purple w3-left w3-tiny w3-round-large mar5l" style="padding:5px;">'. desproteger_cadena($tag2). '</div>
								</div>
								<div class="w3-half w3-right-align">
									Publicado el '. date("Y-m-d, H:i", $buf["FECHA"]). ' por <b>'. desproteger_cadena($buf["NOMBRE"]). '</b>
								</div>
							</div>
						</div>
						';
					}
				}

				echo '
					</div>
				</div>
			</div>		
			<div class="w3-half w3-col s12 m12 l6">
				<div class="w3-container w3-white w3-border w3-padding">
					<h4>
						<span class="material-symbols-outlined w3-medium">smart_toy</span> Incidencias del Bot
					</h4>
					<div class="w3-container">';

				unset($buf);
				if( !mysqli_num_rows($consBot) ) {
					echo ' no hay reportes del bot..';
				}
				else {	
					while( $buf=mysqli_fetch_array($consBot) ) {
						if( !$buf["TAG_SERVERNAME"] ) {
							$tag1= ($mhServer2[$buf["ID_SERVER"]]["sandbox"] ? "sandbox":"prod");
							$tag2= $mhServer2[$buf["ID_SERVER"]]["tagname"];
						}
						else {
							$tag1= (count($mhServer["prod"][$buf["TAG_SERVERNAME"]]) ? "prod":"sandbox");
							$tag2= $buf["TAG_SERVERNAME"];
						}

						$colorAmb= (!strcmp($tag1, "prod") ? 'w3-blue':'w3-orange');
						$bordeEvento= ($buf["STATUS"] ? 'w3-border-blue':'w3-border-red w3-pale-red');
						$dtaJson= json_decode(desproteger_cadena($buf["DATA_JSON"]));

						echo '
						<div class="w3-row w3-border-bottom w3-leftbar w3-padding-small '. $bordeEvento. ' w3-col w12 l12 m12">
							<div class="w3-row w3-small w3-col w12 l12 m12">
								'. date("Y-m-d, H:i", $buf["FECHA"]). ' - '. desproteger_cadena($dtaJson->estado). '
								<div class="'. $colorAmb. ' w3-right w3-tiny w3-round-large mar5l" style="padding:5px;">'. desproteger_cadena($tag1). '</div>
								<div class="w3-teal w3-right w3-tiny w3-round-large mar5l" style="padding:5px;">'. desproteger_cadena($tag2). '</div> 
							</div>
						</div>
						';
					}
				}

				echo '
					</div>
				</div>
			</div>
		</div>
		';
	}

echo '
		<div>
		<footer class="w3-container w3-center w3-padding-32">
			Todos los Derechos reservados &copy; '. date("Y", time()). ' | Un sitio web de Moneybox El Salvador SA de CV | <a href="https://www.facturaelectronica.sv" target="_blank" class="nd">https://www.facturaelectronica.sv</a>
		</footer>
	</body>
</html>
';
ob_end_flush(); # fin objeto
?>