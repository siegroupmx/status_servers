#!/usr/bin/php
<?php
date_default_timezone_set("America/El_Salvador");
define('CWD', '/app'); // definir directorio de la APP
include(CWD."/libs/config.php");
include(CWD."/libs/base.php");
include(CWD."/libs/wssMH.php");

echo "\n\nIniciando monitor de servidores MH-ElSalvador...";

// el salvador
$mhServer= array(
	"mh_send"=>array("sandbox"=>false, "service"=>"send"), 
	"mh_cancel"=>array("sandbox"=>false, "service"=>"cancel"), 
	"mh_contingencia"=>array("sandbox"=>false, "service"=>"contingencia"), 
	"mh_consulta"=>array("sandbox"=>false, "service"=>"consulta"), 
	"mh_send_sandbox"=>array("sandbox"=>true, "service"=>"send"), 
	"mh_cancel_sandbox"=>array("sandbox"=>true, "service"=>"cancel"), 
	"mh_contingencia_sandbox"=>array("sandbox"=>true, "service"=>"contingencia"), 
	"mh_consulta_sandbox"=>array("sandbox"=>true, "service"=>"consulta")
);

foreach( $mhServer as $k=>$v ) {
	$idMonitor= consultar_datos_general("SERVER_TARGETS", "TAGNAME='". $k. "' && BLOCK='0'", "ID" );

	if( $idMonitor ) {
		$sandbox= $v["sandbox"];
		$s= new wssMH($sandbox);
		$s->getStatusServer($v["service"]);
		$transfer= $s->getFullHeaderRequest();
		$fecha=time();
		$statusNow= ($s->getError() ? 0:1);
		$lastEvent= consultar_datos_general("SERVER_LOG", "ID_SERVER='". $idMonitor. "' ORDER BY FECHA DESC", "STATUS"); // ultimo status
		$registIncident= (!strcmp($statusNow, $lastEvent) ? false:true); // distinto, registrar suceso

		$trama= array(
			"id_server"=>"'". $idMonitor. "'", 
			"time_response"=>"'". ($transfer["total_time"] ? $transfer["total_time"]:0). "'", 
			"status"=>"'". $statusNow. "'", 
			"fecha"=>"'". $fecha. "'"
		);
		insertar_bdd("SERVER_LOG", $trama);
		unset($trama);

		echo "\n\n[". date("Y-m-d, H:i", $fecha). "] Id Monitor: ". $idMonitor. " - ". $k;

		if( $s->getError() ) {
			echo "\nStatus: ERROR";
			$latDown= ($s->getError() ? $fecha:0);
			$trUpdate= array(
				"id"=>"'". $idMonitor. "'", 
				"status"=>"'". $statusNow. "'", 
				"last_down"=>"'". $latDown. "'"
			);
			actualizar_bdd("SERVER_TARGETS", $trUpdate);
			unset($latDown);
		}
		else { // exito
			// echo "\nStatus: EXITO";
			// echo "\n";
			// print_r($transfer);
			// echo "\n";
			$latUp= ($s->getError() ? 0:$fecha);
			$trUpdate= array(
				"id"=>"'". $idMonitor. "'", 
				"status"=>"'". $statusNow. "'", 
				"last_up"=>"'". $latUp. "'"
			);
			actualizar_bdd("SERVER_TARGETS", $trUpdate);
			unset($trUpdate);
		}

		if( $registIncident ) { // registrar suceso
			$dataJson=json_encode(
				array(
					"estado"=>(!$statusNow ? 'se detecto caida':'se detecto vuelta en funcionamiento')
				)
			);
			$trama= array(
				"id_server"=>"'". $idMonitor. "'", 
				"tag_servername"=>"'". $k. "'", 
				"ambiente"=>"'". ($mhServer[$k]["sandbox"] ? 0:1). "'", 
				"time_response"=>"'". ($transfer["total_time"] ? $transfer["total_time"]:0). "'", 
				"status"=>"'". $statusNow. "'", 
				"data_json"=>"'". proteger_cadena($dataJson). "'", 
				"fecha"=>"'". $fecha. "'"
			);
			insertar_bdd("BOT_INCIDENT", $trama);
			unset($trama);
		}
		else {
			echo "\nNo se registro suceso de cambio de estado [Ultimo: ". $lastEvent. "] [Ahora: ". $statusNow. "]...";
		}
		unset($sandbox, $s, $trama, $trUpdate, $transfer, $fecha, $registIncident);
	}
	unset($idMonitor);
}

echo "\n\nFin del monitor...\n";
?>