<?php
/**
* author		Angel Cantu Jauregui
* mail			angel.cantu@moneybox.business
* date 			01/01/2024, 12:00:00
* descrpition	libreria simple para conectar a los servicios regulares del MH
* licence		
*/
class wssMH {
	const API_AUTH_SANDBOX= 'https://apitest.dtes.mh.gob.sv/seguridad/auth';
	const API_AUTH= 'https://api.dtes.mh.gob.sv/seguridad/auth';
	const API_SANDBOX= 'https://apitest.dtes.mh.gob.sv/fesv/recepciondte';
	const API= 'https://api.dtes.mh.gob.sv/fesv/recepciondte';
	const API_QUERY_SANDBOX= 'https://apitest.dtes.mh.gob.sv/fesv/recepcion/consultadte/';
	const API_QUERY= 'https://api.dtes.mh.gob.sv/fesv/recepcion/consultadte/';
	const API_CONTINGENCIA_SANDBOX= 'https://apitest.dtes.mh.gob.sv/fesv/contingencia';
	const API_CONTINGENCIA= 'https://api.dtes.mh.gob.sv/fesv/contingencia';
	const API_CANCEL_SANDBOX= 'https://apitest.dtes.mh.gob.sv/fesv/anulardte';
	const API_CANCEL= 'https://api.dtes.mh.gob.sv/fesv/anulardte';

	private $service=NULL;
	private $auth=NULL;
	private $query=NULL;
	private $send=NULL;
	private $firma=NULL;
	private $contingencia=NULL;
	private $cancel=NULL;
	private $checkStatus=NULL;
	private $apiUrl=NULL;
	private $headers=NULL;
	private $metodo=NULL;
	private $data=NULL;
	private $sandbox=NULL;
	private $user=NULL;
	private $pass=NULL;
	private $headerRequest=NULL;
	private $headerResponse=NULL;
	private $fullHeaderRequest=NULL;
	private $error=NULL;
	private $error_code=NULL;
	private $success=NULL;
	private $success_code=NULL;
	private $token=NULL;
	private $errorArr=NULL;

	/**
	* establecemos el metodo de conexion: GET o PSOT
	* 
	* @param string $a metodo de conexion
	*/
	public function setMetodo($a=NULL) {
		$this->metodo= $a;
	}

	/**
	* devuelve el metodo de conexion: GET o PSOT
	* 
	* @return string el metodo
	*/
	public function getMetodo() {
		return $this->metodo;
	}

	/**
	* establecemos el servicio a consumir
	* 
	* @param string $a el servicio
	*/
	public function setService($a=NULL) {
		$this->service= $a;
		$this->auth= (!strcmp($a, "auth") ? true:false);
		$this->query= (!strcmp($a, "query") ? true:false);
		$this->send= (!strcmp($a, "send") ? true:false);
		$this->firma= (!strcmp($a, "firma") ? true:false);
		$this->contingencia= (!strcmp($a, "contingencia") ? true:false);
		$this->cancel= (!strcmp($a, "cancel") ? true:false);
		$this->checkStatus= (!strcmp($a, "status") ? true:false);
	}

	/**
	* valida si el servicio de verificacion de estado esta activo
	* 
	* @return boolean verdadero o falso
	*/
	public function isCheckStatusEnable() {
		return $this->checkStatus;
	}

	/**
	* valida si el servicio de autenticacion esta activo
	* 
	* @return boolean verdadero o falso
	*/
	public function isAuthEnable() {
		return $this->auth;
	}

	/**
	* valida si el servicio de firmado esta activo
	* 
	* @return boolean verdadero o falso
	*/
	public function isFirmaEnable() {
		return $this->firma;
	}

	/**
	* valida si el servicio de consulta esta activo
	* 
	* @return boolean verdadero o falso
	*/
	public function isQueryEnable() {
		return $this->query;
	}

	/**
	* valida si el servicio de envio de documentos esta activo
	* 
	* @return boolean verdadero o falso
	*/
	public function isSendEnable() {
		return $this->send;
	}

	/**
	* valida si el servicio cancelacion esta activo
	* 
	* @return boolean verdadero o falso
	*/
	public function isCancelEnable() {
		return $this->cancel;
	}

	/**
	* valida si el servicio de contingencia
	* 
	* @return boolean verdadero o falso
	*/
	public function isContingenciaEnable() {
		return $this->contingencia;
	}

	/**
	* devuelve el servicio consumido
	* 
	* @return string el servicio
	*/
	public function getService() {
		return $this->service;
	}

	/**
	* establecemos los datos a enviar
	* 
	* @param string $a los datos
	*/
	public function setData($a=NULL) {
		$this->data= $a;
	}

	/**
	* devuelve los datos
	* 
	* @return string los datos
	*/
	public function getData() {
		return $this->data;
	}

	/**
	* establece las cabeceras
	* 
	* @param array $a datos de cabeceras
	*/
	public function setCabeceras($a=NULL) {
		$this->headers= $a;
	}

	/**
	* devuelve las cabeceras
	* 
	* @return array cabeceras
	*/
	public function getCabeceras() {
		return $this->headers;
	}

	/**
	* establece la ruta API
	* 
	* @param string ruta api
	*/
	public function setApiUrl($a=NULL) {
		$this->apiUrl= $a;
	}

	/**
	* devuelve la ruta API
	* 
	* @return string cabeceras
	*/
	public function getApiUrl() {
		return $this->apiUrl;
	}

	/**
	* establece el estado de Sandbox
	* 
	* @param boolean $a verdadero o falso
	*/
	public function setSandbox($a=false) {
		$this->sandbox= $a;
	}

	/**
	* devuelve el estado de Sandbox
	* 
	* @return boolean verdadero o falso
	*/
	public function isSandboxActive() {
		return $this->sandbox;
	}

	/**
	* establecemos el usuario
	* 
	* @param string $a usuario
	*/
	public function setUser($a=NULL) {
		$this->user= $a;
	}

	/**
	* devuelve el usuario
	* 
	* @return string usuario
	*/
	public function getUser() {
		return $this->user;
	}

	/**
	* establecemos el password
	* 
	* @param string $a password
	*/
	public function setPass($a=NULL) {
		$this->pass= $a;
	}

	/**
	* devuelve el password
	* 
	* @return string password
	*/
	public function getPass() {
		return $this->pass;
	}

	/**
	* Retorna contenido de la variable $headerRequest
	*
	* @return string contenido de variable
	*/
	public function getHeaderRequest() {
		return $this->headerRequest;
	}

	/**
	* Establece el contenido del Request
	*
	* @param string contenido de variable
	*/
	public function setHeaderRequest($a=NULL) {
		$this->headerRequest= $a;
	}

	/**
	* Establece el contenido del Request
	*
	* @param array contenido de variable
	*/
	public function setFullHeaderRequest($a=NULL) {
		$this->fullHeaderRequest= $a;
	}

	/**
	* retorna las cabeceras completas del envio
	*
	* @return array informacion de cabeceras
	*/
	public function getFullHeaderRequest() {
		return $this->fullHeaderRequest;
	}

	/**
	* Retorna contenido de la variable $headerResponse
	*
	* @return string contenido de variable
	*/
	public function getHeaderResponse() {
		return $this->headerResponse;
	}

	/**
	* Establece el contenido del Response
	*
	* @param string contenido de variable
	*/
	public function setHeaderResponse($a=NULL) {
		$this->headerResponse= $a;
	}

	/**
	* indica el ultimo error obtenido en caso que exista
	*
	* @return string informacion del estado
	*/
	public function getError() {
		return $this->error;
	}

	/**
	* indica el ultimo error obtenido en caso que exista
	*
	* @param string informacion del estado
	* @param string codigo de error, opcional..
	*/
	public function setError($a=NULL, $code=NULL) {
		$this->error= $a;
		$this->setErrorCode($code);
	}

	/**
	* indica el ultimo error obtenido en caso que exista
	*
	* @param string informacion del estado
	*/
	public function setErrorData($a=NULL) {
		$this->errorArr= $a;
	}

	/**
	* retorna los detalles del error
	*
	* @return array datos detallados del error
	*/
	public function getErrorData($a=NULL) {
		return $this->errorArr;
	}

	/**
	* indica el codigo error obtenido en caso que exista
	*
	* @param string informacion del estado
	*/
	public function setErrorCode($a=NULL) {
		$this->error_code= $a;
	}

	/**
	* indica el codigo error obtenido en caso que exista
	*
	* @return string informacion del estado
	*/
	public function getErrorCode() {
		return $this->error_code;
	}

	/**
	* retorn el mensaje de exito
	*
	* @return string informacion del estado
	*/
	public function getSuccess() {
		return $this->success;
	}

	/**
	* establece el mensaje de exito
	*
	* @param string informacion del estado
	*/
	public function setSuccess($a) {
		$this->success= $a;
	}

	/**
	* devuelve la respuesta
	*
	* @param string informacion del estado
	* @param string codigo de error, opcional..
	*/
	public function getRespuesta() {
		return $this->getSuccess();
	}

	/**
	* convierte datos a post
	*
	* @param mixed datos
	* @return string post data
	*/
	public function arr2post($a=NULL) {
		if( !is_array($a) ) // no es array
			return $a; // devolvemos string
		else {
			$r='';
			foreach( $a as $k=>$v ) {
				$r .= ($r ? '&':'').$k.'='.$v;
			}

			return $r;
		}
	}

	/**
	* establece la token de autenticacion
	*
	* @param string $a token
	*/
	public function setToken($a=NULL) {
		$this->token= $a;
	}

	/**
	* devuelve la token de autenticacion
	*
	* @return string token
	*/
	public function getToken() {
		return $this->token;
	}

	/**
	* envia la token a un archivo
	*
	* @param string $a ruta a la token
	*/
	public function saveTokentoFile($a=NULL) {
		if( $this->getToken() ) {
			$fp= fopen($a, "w");
			fwrite($fp, $this->getToken());
			fclose($fp);
			return true;
		}
	}

	/**
	* carga token desde archivo o string
	*
	* @param string $a ruta a la token
	* @param string $m modo de funcionamiento
	*/
	public function loadToken($a=NULL, $m="file") {
		if( !strcmp($m, "file") ) { // desde archivo
			$this->setToken(trim(file_get_contents($a)));
		}
		else if( !strcmp($m, "string") ) { // desde el stream
			$this->setToken($a);
		}
	}

	/**
	* envio al ministerior de haciendo
	*/
	public function sendToMh() {
		$s= curl_init();
		curl_setopt($s, CURLOPT_URL, $this->getApiUrl());
		curl_setopt($s, CURLOPT_HTTPHEADER, $this->getCabeceras() );
		curl_setopt($s, CURLOPT_HEADER, 1 );
		curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($s, CURLOPT_VERBOSE, true);
		curl_setopt($s, CURLINFO_HEADER_OUT, true);
		curl_setopt($s, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

		if( !strcmp($this->getMetodo(), "POST") ) { # es distinto de GET, usamos POST
			curl_setopt($s, CURLOPT_POST, 1);
			curl_setopt($s, CURLOPT_POSTFIELDS, $this->arr2post($this->getData()));
		}
		if( !strcmp($this->getMetodo(), "JSON") ) { # es distinto de JSON
			curl_setopt($s, CURLOPT_POST, 1);
			curl_setopt($s, CURLOPT_POSTFIELDS, $this->getData());
		}
		if( !strcmp($this->getMetodo(), "GET") ) { # es GET
			// curl_setopt($s, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
			curl_setopt($s, CURLOPT_POST, 0);
		}
		$resp= curl_exec($s);
		$rq= curl_getinfo($s);

		$this->setFullHeaderRequest($rq);
		$this->setHeaderRequest($rq["request_header"]); // request
		$this->setHeaderResponse($resp); // response
		$this->debugResponse();

		curl_close($s);
		unset($rq, $resp, $s);
	}

	/**
	* Verifica cabeceras de una respuesta para determinar si fue error o exito, setea variable $success y $error
	*/
	public function debugResponse() {
		// echo "\n\n--- Request:\n\n";
		// print_r($this->getHeaderRequest());
		// echo "\n";
		// print_r($this->getData());
		// echo "\n\n--- Response:\n\n";
		// print_r($this->getHeaderResponse());

		if( $this->isAuthEnable() ) { // servicio de autenticacion
			$r= explode("\r\n", $this->getHeaderResponse());
			$json= json_decode($r[(count($r)-1)]);

			if( strcmp($json->status, "OK") ) { // error
				$this->setError("problemas para realizar autenticacion: ".$json->body->codigoMsg." - ".$json->body->descripcionMsg, "001");
				$this->setSuccess(NULL);
			}
			else { // exito
				$this->setToken($json->body->token);
				$this->setSuccess("token: ".$this->getToken());
			}
		}
		else if( $this->isFirmaEnable() ) { // firmado electronico
			$r= explode("\r\n", $this->getHeaderResponse());
			$json= json_decode($r[(count($r)-1)]);

			if( strcmp($json->status, "OK") ) { // error
				$this->setError("problemas para firma: ".$json->status." - ".$json->body->mensaje, "001");
				$this->setSuccess(NULL);
			}
			else {
				$this->setSuccess($json->body);
			}
		}
		else if( $this->isContingenciaEnable() ) {
			$r= explode("\r\n", $this->getHeaderResponse());
			$json= json_decode($r[(count($r)-1)]);

			if( strcmp($json->estado, "RECIBIDO") ) { // error
				$this->setError("problemas para realizar contingencia: ".$json->mensaje, "001");
				$this->setErrorData($json->observaciones);
				$this->setSuccess(NULL);
			}
			else { // exito
				$this->setSuccess($json->selloRecibido);
			}
		}
		else if( $this->isQueryEnable() ) { // servicio de consulta de estatus
		}
		else if( $this->isSendEnable() ) { // servicio de envio de documentos
			$r= explode("\r\n", $this->getHeaderResponse());
			$json= json_decode($r[(count($r)-1)]);

			if( $json->status ) {
				$this->setError("problemas para realizar timbrado: ".$json->error, "001");
				$this->setSuccess(NULL);
			}
			else if( strcmp($json->estado, "PROCESADO") ) { // error
				$this->setError("problemas para realizar timbrado: ".$json->codigoMsg." - ".$json->descripcionMsg, "001");
				$this->setErrorData($json->observaciones);
				$this->setSuccess(NULL);
			}
			else { // exito
				$this->setSuccess($json->selloRecibido);
			}
		}
		else if( $this->isCancelEnable() ) {
			$r= explode("\r\n", $this->getHeaderResponse());
			$json= json_decode($r[(count($r)-1)]);

			// print_r($json);

			if( !strcmp($json->status, "400") ) {
				$this->setError("problemas para realizar transacci&oacute;n de cancelaci&oacute;n: ".$json->error. " ". $json->path, "001");
				$this->setErrorData(NULL);
				$this->setSuccess(NULL);
			}
			else if( strcmp($json->estado, "PROCESADO") ) { // error
				$this->setError("problemas para realizar cancelaci&oacute;n: ".$json->codigoMsg." - ".$json->descripcionMsg, "001");
				$this->setErrorData($json->observaciones);
				$this->setSuccess(NULL);
			}
			else { // exito
				$this->setSuccess($json->selloRecibido);
			}
		}
		else if( $this->isCheckStatusEnable() ) {
			$r= explode("\r\n", $this->getHeaderResponse());
			$trasnfer= $this->getFullHeaderRequest();

			if( strcmp($trasnfer["http_code"], "401") ) { // error
				$this->setSuccess(NULL);
				$this->setError( "el estado del servidor es inactivo", "005");
			}
			else { // exito
				$this->setError(NULL);
				$this->setSuccess(1);
			}
		}
	}

	/**
	* servicio de autenticacion
	*/
	public function auth() {
		if( $this->isSandboxActive() )
			$this->setApiUrl(self::API_AUTH_SANDBOX);
		else
			$this->setApiUrl(self::API_AUTH);

		$this->setService("auth"); // informamos servicio de autenticacion
		$this->setCabeceras(array(
			"Content-Type: application/x-www-form-urlencoded; charset=UTF-8", 
			"User-Agent: moneyBox"
		));

		$this->setMetodo("POST"); // establecemos metodo POST
		$this->setData(array( // agregamos datos de conexion
			"user"=>$this->getUser(), 
			"pwd"=>$this->getPass()
		));

		$this->sendToMh();
	}

	/**
	* servicio de firmar documento
	*/
	public function firma($a=NULL) {
		$firmaXml= simplexml_load_string(file_get_contents($a["cert"]));
		$stringPrivateKey= "-----BEGIN PRIVATE KEY-----\n";
		$stringPrivateKey .= sprintf($firmaXml->privateKey->encodied[0]);
		$stringPrivateKey .= "\n-----END PRIVATE KEY-----";
		$stringPublicKey= "-----BEGIN PUBLIC KEY-----\n";
		$stringPublicKey .= sprintf($firmaXml->publicKey->encodied[0]);
		$stringPublicKey .= "\n-----END PUBLIC KEY-----";
		$pubKey= openssl_pkey_get_public($stringPublicKey); // obtenemos clave publica
		$privKey= openssl_pkey_get_private($stringPrivateKey); // obtenemos clave privada

		$algo= str_replace("=", "", base64_encode("{\"alg\":\"RS512\"}"));
		$jsonB64= str_replace("=", "", base64_encode(json_encode($a["dteJson"])));
		$toSign= $algo.".".$jsonB64;
		$rs= (openssl_sign($toSign, $dataOut, $privKey, OPENSSL_ALGO_SHA512) ? str_replace("=", "", base64_encode($dataOut)):NULL);
		$dataSigned= $algo.".";
		$dataSigned .= $jsonB64. ".";
		$dataSigned .= $rs;

		$verify= openssl_verify($toSign, base64_decode($rs), $pubKey, OPENSSL_ALGO_SHA512);

		if( $verify ) {
			$this->setError(NULL);
			$this->setSuccess($dataSigned);
		}
		else {
			// $errorArr= "[DS: ".$dataSigned."]";
			// $this->setErrorData($errorArr);
			$this->setError("problemas verificar firma electronica", "001");
			$this->setSuccess(NULL);
		}
	}

	/**
	* servicio de timbrado de documentos
	*/
	public function send($a=NULL) {
		if( !$this->getToken() )
			$this->setError("no hay token generada para conectar al MH", "002");
		else if( !$a )
			$this->setError("no hay datos para el envio de timbrado", "003");
		else {
			if( $this->isSandboxActive() )
				$this->setApiUrl(self::API_SANDBOX);
			else
				$this->setApiUrl(self::API);

			$this->setService("send"); // informamos servicio de autenticacion
			$this->setCabeceras(array(
				"Authorization: ".$this->getToken(),
				"Content-Type: application/json; charset=UTF-8", 
				"User-Agent: moneyBox"
			));
			$this->setData($a);
			$this->setMetodo("JSON"); // establecemos metodo POST

			$this->sendToMh();
		}
	}

	/**
	* servicio de timbrado de documentos
	*/
	public function contingencia($a=NULL) {
		if( !$this->getToken() )
			$this->setError("no hay token generada para conectar al MH", "002");
		else if( !$a )
			$this->setError("no hay datos para el envio de timbrado", "003");
		else {
			if( $this->isSandboxActive() )
				$this->setApiUrl(self::API_CONTINGENCIA_SANDBOX);
			else
				$this->setApiUrl(self::API_CONTINGENCIA);

			$this->setService("contingencia"); // informamos servicio de autenticacion
			$this->setCabeceras(array(
				"Authorization: ".$this->getToken(),
				"Content-Type: application/json; charset=UTF-8", 
				"User-Agent: moneyBox"
			));
			$this->setData($a);
			$this->setMetodo("JSON"); // establecemos metodo POST

			$this->sendToMh();
		}
	}

	/**
	* servicio de cancelacion
	*/
	public function cancel($a=NULL) {
		if( !$this->getToken() )
			$this->setError("no hay token generada para conectar al MH", "002");
		else if( !$a )
			$this->setError("no hay datos para el envio de timbrado", "003");
		else {
			if( $this->isSandboxActive() )
				$this->setApiUrl(self::API_CANCEL_SANDBOX);
			else
				$this->setApiUrl(self::API_CANCEL);

			$this->setService("cancel"); // informamos servicio de autenticacion
			$this->setCabeceras(array(
				"Authorization: ".$this->getToken(),
				"Content-Type: application/json; charset=UTF-8", 
				"User-Agent: moneyBox"
			));
			$this->setData($a);
			$this->setMetodo("JSON"); // establecemos metodo POST

			$this->sendToMh();
		}
	}

	/**
	* devuelve el estado del servidor
	* 
	* @param string $servername nombre del servidor: send, cancel, contingencia, consulta
	*/
	public function getStatusServer($servername=NULL) {
		if( !$servername )
			$this->setError("no indico el servidor a verificar", "004");
		else {
			if( $this->isSandboxActive() ) {
				if( !strcmp($servername, "send") )
					$this->setApiUrl(self::API_SANDBOX);
				else if( !strcmp($servername, "cancel") )
					$this->setApiUrl(self::API_CANCEL_SANDBOX);
				else if( !strcmp($servername, "contingencia") )
					$this->setApiUrl(self::API_CONTINGENCIA_SANDBOX);
				else if( !strcmp($servername, "consulta") )
					$this->setApiUrl(self::API_QUERY_SANDBOX);
			}
			else {
				if( !strcmp($servername, "send") )
					$this->setApiUrl(self::API);
				else if( !strcmp($servername, "cancel") )
					$this->setApiUrl(self::API_CANCEL);
				else if( !strcmp($servername, "contingencia") )
					$this->setApiUrl(self::API_CONTINGENCIA);
				else if( !strcmp($servername, "consulta") )
					$this->setApiUrl(self::API_QUERY);
			}

			$this->setService("status"); // informamos servicio de autenticacion
			$this->setCabeceras(array(
				"Content-Type: text/html; charset=utf-8", 
				"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7", 
				"Accept-Encoding: gzip, deflate, br", 
				"User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
			));
			$this->setData(NULL);
			$this->setMetodo("GET"); // establecemos metodo POST
			$this->sendToMh();
		}
	}

	/**
	* clase principal
	*/
	public function __construct($sandbox=false, $user=NULL, $pass=NULL) {
		$this->setSandbox($sandbox);
		$this->setUser($user ? $user:NULL);
		$this->setPass($pass ? $pass:NULL);
	}
}
?>