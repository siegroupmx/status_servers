<?php
// cerrar conexion
function cerrar_db() {
	if( isset($_SESSION["mysql_r"]) || isset($_SESSION["mysql_w"]) ) {
		@mysqli_close($_SESSION["mysql_r"]);
		@mysqli_close($_SESSION["mysql_w"]);
		unset($_SESSION["mysql_r"], $_SESSION["mysql_w"]);
	}
}

//realiza conexion a la Base de Datos
function conectar($modo=NULL) {
	if( !($link= mysqli_connect( ($modo==1 ? SERVER_RD : SERVER_WR), BASE_USR, BASE_PASS, BASE )) ) {
		echo 'Error 01: Error para Conectarse a MySQL.'. mysqli_error($link);
		$link= 'INSTALL';
	}
	return $link;
}

//consulta multiples valores a una Base de Datos. Donde los valores van delimitados
//por un ":" xD
function consultar( $base_t, $valores ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));

	if( strstr( $valores, ":" ) )  {
		$valores= str_replace( ":", ",", $valores ); //cambiamos el :  por  ,
		$r= mysqli_query( $link, "select ". $valores. " from ". $base_t. ";" );
	}
	else if( $valores ) {
		$r= mysqli_query( $link, "select ". $valores. " from ". $base_t. ";");
	}
	else
		$r= mysqli_query( $link, "select * from ". $base_t. ";");
	
	# @mysqli_close($link);
	return $r;
}


//realizar consulta donde escojeremos el o las celdas de una tabla que
//coincidan con (where) los valores pasados :D. Donde los valores estan
//delimitados por un ":".
function consultar_con( $base_t, $valores, $thisSel=NULL ) {
	$link= (isset($_SESSION["mysql_r"]) ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");

	if( strchr( $valores, ":" ) ) {
		$valores= str_replace( ":", " && ", $valores );
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valores. ";");
	}
	else if( $valores ) {
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valores. ";");
	}
	# @mysqli_close($link);
	return $r;
}

//consultamos en ordenacion mediante '$regla'
function consultar_enorden( $base_t, $regla, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");

	$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " ORDER BY ". $regla. ";");
	# @mysqli_close($link);
	return $r;
}

//consultamos valores delimitados por un ":", en ordenacion mediante '$regla'
function consultar_enorden_con( $base_t, $valores, $regla, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");

	if( strchr( $valores, ":" ) ) {
		$valores= str_replace( ":", " && ", $valores );
		$r= mysqli_query( $link, "select ".$sel. " from ". $base_t. " where ". $valores. " ORDER BY ". $regla. ";");
	}
	else if( $valores ) {
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valores. " ORDER BY ". $regla. ";");
	}
	
	# @mysqli_close($link);
	return $r;
}

//consultamos valores delimitados por un ":", en ordenacion mediante '$regla'
function consultar_cont_enorden_con( $base_t, $valores, $regla, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");
	
	if( strchr( $valores, ":" ) ) //si se encuentra   :   entonces ahi mas de 1 valor
		$valores= str_replace( ":", " && ", $valores );

	$r= mysqli_query( $link, "select COUNT(". $sel. ") as TOTAL from ". $base_t. " where ". $valores. " ORDER BY ". $regla. ";");
	# @mysqli_close($link);
	return $r;
}

//consultamos valores delimitados por un ":", en ordenacion mediante '$regla'
function consultar_cont_con( $base_t, $valores, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");
	
	if( strchr( $valores, ":" ) ) //si se encuentra   :   entonces ahi mas de 1 valor
		$valores= str_replace( ":", " && ", $valores );

	$r= mysqli_query($link, "select COUNT(". $sel. ") as TOTAL from ". $base_t. " where ". $valores. ";");
	# @mysqli_close($link);
	return $r;
}

function consultar_rango( $base_t, $valores, $fechainicio, $fechafin, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");
	
	if( strchr( $valores, ":" ) ) //si se encuentra   :   entonces ahi mas de 1 valor
		$valores= str_replace( ":", " && ", $valores );

	$r= mysqli_query($link, "select ". $sel. " from ". $base_t. " where ". $valores. " BETWEEN '". $fechainicio. "' AND '". $fechafin. "';");
	# @mysqli_close($link);
	return $r;
}

function consultar_rango_enorden( $base_t, $valores, $fechainicio, $fechafin, $regla, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");

	if( strchr( $valores, ":" ) ) {
		$valores= str_replace( ":", " && ", $valores );
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valores. " BETWEEN '". $fechainicio. "' AND '". $fechafin. "' ORDER BY ". $regla. ";");
	}
	else if( $valores ) {
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valores. " BETWEEN '". $fechainicio. "' AND '". $fechafin. "' ORDER BY ". $regla. ";");
	}
	
	# @mysqli_close($link);
	return $r;
}

//consultando limitada, ordenada, donde coinsidiran las variables
function consultar_rango_limite_enorden_con( $base_t, $vars, $limite, $fechainicio, $fechafin, $regla, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");
	
	if( strchr( $limite, "," ) ) {
		$vars= str_replace( ":", " && ", $vars ); //cambiamos el :  por  &&
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $vars. " BETWEEN '". $fechainicio. "' AND '". $fechafin. "' ORDER BY ". $regla. " LIMIT ". $limite. ";");
	}
	
	# @mysqli_close($link);
	return $r;
}

function consultar_rango_enorden_con( $base_t, $args, $valores, $fechainicio, $fechafin, $regla, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");

	if( strchr( $valores, ":" ) ) {
		$valores= str_replace( ":", " && ", $valores );
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valores. " BETWEEN '". $fechainicio. "' AND '". $fechafin. "' AND ". $args. " ORDER BY ". $regla. ";");
	}
	else if( $valores ) {
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valores. " BETWEEN '". $fechainicio. "' AND '". $fechafin. "' AND ". $args. " ORDER BY ". $regla. ";");
	}
	
	# @mysqli_close($link);
	return $r;
}

function contar_indexados( $base_t, $valor, $muestra, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");
	
	if( $valor ) {
		if( !($r= mysqli_query( $link, "select COUNT(". $sel. ") as TOTAL from ". $base_t. " where ". $valor. " LIKE '%". $muestra. "%';")) )
			return 0;
		else {
			$aux= mysqli_fetch_array($r);
			limpiar($r);
			unset($r);
			# @mysqli_close($link);
			return $aux["TOTAL"];
		}
	}
	
	# @mysqli_close($link);
	return 0;
}

function consultar_indexados( $base_t, $valor, $muestra, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");

	if( $valor ) {
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valor. " LIKE '%". $muestra. "%';");
	}
	
	# @mysqli_close($link);
	return $r;
}

function consultar_indexados_con( $base_t, $args, $valor, $muestra, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");
	
	if( strchr( $valor, ":" ) ) //si se encuentra   :   entonces ahi mas de 1 valor
		$r=0;
	else
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valor. " LIKE '%". $muestra. "%' && ". $args. ";");
	# @mysqli_close($link);
	return $r;
}

function consultar_indexados_enorden_con( $base_t, $args, $valor, $muestra, $orden, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");
	
	if( strchr( $valor, ":" ) ) //si se encuentra   :   entonces ahi mas de 1 valor
		$r=0;
	else
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valor. " LIKE '%". $muestra. "%' && ". $args. " ORDER BY ". $orden. ";");
	# @mysqli_close($link);
	return $r;
}

function consultar_indexados_enorden( $base_t, $valor, $muestra, $orden, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");

	if( $valor ) {
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valor. " LIKE '%". $muestra. "%' ORDER BY ". $orden. ";");
	}
	
	# @mysqli_close($link);
	return $r;
}

function consultar_indexados_limite_enorden_con( $base_t, $args, $valor, $muestra, $limite, $orden, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");
	
	if( strchr( $valor, ":" ) ) //si se encuentra   :   entonces ahi mas de 1 valor
		$r=0;
	else
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $valor. " LIKE '%". $muestra. "%' && ". $args. " ORDER BY ". $orden. " LIMIT ". $limite. ";" );
	# @mysqli_close($link);
	return $r;
}


//consultamos un rango de celdas mediante la instruccion LIMIT y los valores de "Inicio", "Fin"
function consultar_limite( $base_t, $valores, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");

	if( strchr( $valores, ":" ) ) {
		$valores= str_replace( ":", ",", $valores ); //cambiamos el :  por  ,
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " LIMIT ". $valores. ";");
	}
	else
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " LIMIT ". $valores. ";");
	
	# @mysqli_close($link);
	return $r;
}

//consultamos un rango de celdas mediante la instruccion LIMIT y los valores de "Inicio", "Fin"
function consultar_limite_con( $base_t, $vars, $valores, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");

	if( strchr( $valores, "," ) ) {
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $vars. " LIMIT ". $valores. ";");
	}
	
	# @mysqli_close($link);
	return $r;
}

function consultar_limite_enorden( $base_t, $limite, $regla, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");

	if( strchr( $limite, "," ) ) {
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " ORDER BY ". $regla. " LIMIT ". $limite. ";");
	}
	
	# @mysqli_close($link);
	return $r;
}

//consultando limitada, ordenada, donde coinsidiran las variables
function consultar_limite_enorden_con( $base_t, $vars, $limite, $regla, $thisSel=NULL ) {
	$link= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$sel= ($thisSel ? $thisSel:"*");

	if( strchr( $limite, "," ) ) {
		$vars= str_replace( ":", " && ", $vars ); //cambiamos el :  por  &&
		$r= mysqli_query( $link, "select ". $sel. " from ". $base_t. " where ". $vars. " ORDER BY ". $regla. " LIMIT ". $limite. ";");
	}
	
	# @mysqli_close($link);
	return $r;
}

//insercion de valores a una tabla
function insertar_bdd( $base_t, $valores ) {
	$link= (isset($_SESSION["mysql_w"]) ? $_SESSION["mysql_w"]:conectar(2));

	if( count($valores)>0 ) {
		$vars="";
		$datos="";

		foreach( $valores as $key=>$val ) {
			$vars .= ($vars ? ', ':'').$key;
			$datos .= ($datos ? ', ':'').$val;
		}

		if( !($r= mysqli_query( $link, "insert into ". $base_t. " ( ". $vars. " ) values( ". $datos. " );")) )
			$r= false;
		else
			$r= true;
	}
	else
		$r= false;
	
	unset($valores, $var, $datos);
	# @mysqli_close($link);
	return $r;
}

//actualizar valores existentes de un campos de la tabla
function actualizar_bdd( $base_t, $valores ) {
	$link= (isset($_SESSION["mysql_w"]) ? $_SESSION["mysql_w"]:conectar(2));

	if( count($valores)>0 ) {
		$condicion= "";
		$datos="";
		$fl=true;

		foreach( $valores as $key=>$val ) {
			if( $fl ) {
				$condicion= $key. '='.$val;
				$fl= false;
			}
			else {
				$datos .= ($datos ? ', ':'').$key. '='.$val;
			}
		}

		if( !($r= mysqli_query( $link, "update ". $base_t. " set ". $datos. " where ". $condicion. ";")) )
			$r= false;
		else
			$r= true;
	}
	else
		$r= false;

	# @mysqli_close($link);
	return $r;
}

//elimina una celda de la tabla
function eliminar_bdd( $base_t, $valores ) {
	$linkRead= ($_SESSION["mysql_r"] ? $_SESSION["mysql_r"]:conectar(1));
	$linkWrite= (isset($_SESSION["mysql_w"]) ? $_SESSION["mysql_w"]:conectar(2));

	if( strchr( $valores, "=" ) ) {
		$r= mysqli_query( $linkRead, "select * from ". $base_t. " where ". $valores. ";");

		if( !mysqli_num_rows($r) )
			return 0;
		else if( !(mysqli_query( $linkWrite, "delete from ". $base_t. " where ". $valores. ";")) )
			$r= false;
		else
			$r= true;
	}
	else
		$r= false;
	
	# @mysqli_close($link);
	return $r;
}

# limpia consulta
function limpiar( $a ) {
	mysqli_free_result($a);
}

# contador de datos de una DB
function contador_db( $db, $arg ) {
	$c= consultar_cont_con($db, $arg, "ID");
	$aux= mysqli_fetch_array($c);
	$r= $aux["TOTAL"];
	unset($aux);
	return $r;
}

//contabilisa el numero de celdas existentes en la tabla '$cad_tabla'
function contador_celdas( $cad_tabla ) {
	$r= consultar( $cad_tabla, "*" );
	return ($r ? mysqli_num_rows($r):0);
}

//contabilizada el numero de celdas de una tabla '$cad_tabla' que contienen algun valor en la variable/celda/miembro '$var'
function contador_datos_tabla( $cad_tabla, $var ) {
	$total=0; //inicializamos
	$cont=0;
	if( ($total=contador_celdas( $cad_tabla ))>0 ) {
		$cons= consultar_con( $cad_tabla, $var );
		if( mysqli_num_rows($cons)<$total ) {
			unset($cons); //borramos variable
			$cons= consultar( $cad_tabla, "*" ); //consultamos todos los registros

			while( $filtro=mysqli_fetch_array($cons) ) {
				if( strcmp( $filtro["IMAGENES_NOMBRE"], "" ) ) {
					if( strchr( $filtro["IMAGENES_NOMBRE"], ":" ) ) {
						$x= explode( ":", $filtro["IMAGENES_NOMBRE"] );
						$cont += sizeof($x); //contamos los nuevos registros
					}
					else //entonces solo existe una sola imagen
						$cont++; //incrementamos en 1
				}
			}

			limpiar($cons);
			unset($filtro, $x, $cons);
			return ($cont+1);
		}
		else {
			unset($cons);
			return ($cons+1);
		}
	}
	unset($total);
	return ($total+1);
}

//realiza concatenacion de variables, llama a la funcion para comprobar existencia
//de un usuarios y retorna un valor 0 o 1.
function login( $user, $pass ) {
	//concatenamos
	$var= "USUARIO='". $user. "' && CLAVE='". $pass. "'";
	$cons= consultar_con( "USUARIOS", $var, "USUARIO,CLAVE,ID" ); //realizamos consulta
	$r=false;
	
	if( !mysqli_num_rows($cons) ) //no existe usuario
		$r=false;
	else
		$r=true;

	limpiar($cons); # limpiamos
	unset($var, $cons, $arr);
	return $r; //entonces la consulta retorno 0
}

//funcion para consultar datos especificos en la BDD de USUARIOS
//estos datos pueden ser: email, nick, nombre, tipo_usuario
function consultar_datos_usuario( $usr, $var ) {
	$cons= consultar_con( "USUARIOS", "USUARIO='". $usr. "'" );
	
	if( !mysqli_num_rows($cons) )
		return false;
	else {
		$tmp= mysqli_fetch_array($cons);
		return $tmp[strtoupper($var)];
	}
}

//funcion para consultar datos especificos en la BDD de CUALQUIERA, es necesario especificar ID
function consultar_datos_base( $bdt, $bdt_id, $var ) {
	$cons= consultar_con( $bdt, "ID='". $bdt_id. "'" );
	
	if( !mysqli_num_rows($cons) )
		return false;
	else {
		$tmp= mysqli_fetch_array($cons);		
		return $tmp[strtoupper($var)];
	}
}

//funcion para consultar datos especificos en la BDD de CUALQUIERA
function consultar_datos_general( $bdt, $bdt_where, $var ) {
	$cons= consultar_con( $bdt, $bdt_where, $var );
	$r=0;
	
	if( !mysqli_num_rows($cons) )
		$r=0;
	else {
		$tmp= mysqli_fetch_array($cons);		
		$r= $tmp[strtoupper($var)];
	}
	limpiar($cons);
	unset($cons);
	return $r;
}
	
function contador_concatenados( $valores, $delimitador ) {
	if( strchr($valores, $delimitador) ) {
		$x= explode($delimitador, $valores);
		
		return sizeof($x);
	}
	else	return 1;
}
	
function usuario_legitimo() {
	//concatenamos
	$var= "usuario=";
	$var .= "'". $_SESSION["log_usr"]. "'";	
	$var .= ":clave=";
	$var .= "'". $_SESSION["log_pwd"]. "'";

	$cons= consultar_con( "USUARIOS", $var, "USUARIO,CLAVE,ID" ); //consultamos los datos de la cookie

	if( !mysqli_num_rows($cons) ) //entonces la SESSION es FALSA
		return false;

	else //la SESSION es correcta y mostramos PANEL y mas Cosas del Usuario
		return true;
}

function inicializar_espacio_personal( $nick ) {
	$dir_src="usuarios/";
	
	if( !is_dir($dir_src) ) {
		if( mkdir( $dir_src, 0755 )==FALSE ) {
			echo "<i>Error al crear directorio del usuario</i>";
			return 0;
		}
	}
		
	if( !is_dir($dir_src.$nick) ) {
		if( mkdir( $dir_src.proteger_cadena($nick), 0755 ) == FALSE ) {
			echo "<i>Error al crear directorio del usuario</i>"; //creamos carpeta con nombre del usuarui
			return 0;
		}
		else if( mkdir( $dir_src.htmlentities(proteger_cadena($nick)."/archivos", ENT_QUOTES), 0755 )==FALSE ) { //creamos carpeta de 'archivos'
			echo "<i>Error al crear directorio de <b>archivos</b> del usuario</i>";
			return 0;
		}
		else if( mkdir( $dir_src.htmlentities(proteger_cadena($nick)."/imagenes", ENT_QUOTES), 0755 )==FALSE ) { //creamos carpeta de 'imagenes'
			echo "<i>Error al crear directorio de <b>imagenes</b> del usuario</i>";
			return 0;
		}
		else if( mkdir( $dir_src.htmlentities(proteger_cadena($nick)."/uploads", ENT_QUOTES), 0755 )==FALSE ) { //creamos carpeta de 'uploads'
			echo "<i>Error al crear directorio de <b>uploads</b> del usuario</i>";
			return 0;
		}
		else if( mkdir( $dir_src.htmlentities(proteger_cadena($nick)."/buzon", ENT_QUOTES), 0755 )==FALSE ) { //creamos carpeta de 'uploads'
			echo "<i>Error al crear directorio de <b>uploads</b> del usuario</i>";
			return 0;
		}
		else if( copy( "admin/imagenes/default.png", $dir_src.htmlentities(proteger_cadena($nick), ENT_QUOTES)."/imagenes/default.png" )==FALSE ) { //copiamos la imagen por default de su perfil
			echo "<i>Error al crear directorio del usuario</i>";
			return 0;
		}
		return 1;
	}
	
	return 0; //el usuario ya existe
}

function navegador_lenguaje( $lenguaje ) {
	if( !strcmp( $lenguaje, "es-mx" ) )
		return "Espanol-Mexico";
	else if( !strcmp( $lenguaje, "es-ar" ) )
		return "Espanol-Argentina";
	else if( !strcmp( $lenguaje, "es-cl" ) )
		return "Espanol-Chile";
	else if( !strcmp( $lenguaje, "es-ve" ) )
		return "Espanol-Venezuela";
	else if( !strcmp( $lenguaje, "es-br" ) )
		return "Espanol-Brasil";
	else if( !strcmp( $lenguaje, "es-uy" ) )
		return "Espanol-Uruguay";
	else if( !strcmp( $lenguaje, "es-sp" ) )
		return "Espanol-Espana";
	else if( !strcmp( $lenguaje, "en-us" ) )
		return "Ingles-USA";
	else
		return $lenguaje;
}

function deamon_logd() {
	$log_cons= consultar_con( "LOG", "SESION='". session_id(). "'", "ID,NICK" );
	$ip= $_SERVER['REMOTE_ADDR'];
	$nav= get_browser();
	
	if( mysqli_num_rows($log_cons)==0 )	{ //no existe la SESION, esta entrando nuevo usuario
		//recolectamos informacion
		foreach( $nav as $key=>$val ) {
			if( !strcmp($key, "parent" ) )
				$navegador= $val;
			else if( !strcmp($key, "platform" ) )
				$so= $val;
		}
		
		//Geo Localizacion por IP
		require( "admin/geoip.inc" ); //incluimos cabecera
		$geoip_bd= geoip_open( "admin/geoip/GeoIP.dat", GEOIP_STANDARD ); //abrimos archivos dat
		
		if( strcmp( $_SERVER['HTTP_REFERER'], "" ) )
			$ref= strtolower($_SERVER['HTTP_REFERER']);
		else
			$ref= strtolower($_SERVER['HTTP_HOST']);
			
		//condicion especial para identificar Windows Vista
		if( !strcmp( $so, "unknown" ) )
			$so= "Desconocido";
		
		$trama= array(
			"nick"=>"'Visitante'",
			"ip"=>"'". $ip. "'",
			"so"=>"'". $so. "'",
			"navegador"=>"'". $navegador. "'",
			"navegador_lenguaje"=>"'". navegador_lenguaje( substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 5) ). "'", 
			"sesion"=>"'". session_id(). "'",
			"ubicacion"=>"'". geoip_country_name_by_addr( $geoip_bd, $ip). "'", 
			"referencia"=>"'". $ref. "'",
			"nombre_host"=>"'". gethostbyaddr($ip). "'", 
			"fecha_login"=>"'". time(). "'" 
		);
		
		insertar_bdd( "LOG", $trama );
		geoip_close($geoip_bd);
		unset($trama, $ref);
	}
	else {
		$buf= mysqli_fetch_array($log_cons);

		//si el usuario esta logeado y su NICK es de visitante, actualizamos datos
		if( usuario_legitimo() && !strcmp($buf["NICK"], "Visitante") ) {
			$trama= array(
				"sesion"=>"'". session_id(). "'",
				"nick"=>"'". $_SESSION["log_usr"]. "'"
			);
			
			actualizar_bdd( "LOG", $trama );
			unset($trama);
		}

		unset($buf);
	}
	
	unset($log_cons);
	
	//establece los cierres de sesion
	temporizador_de_sesiones();
}
	
function temporizador_de_sesiones() {
	$log_cons= consultar_con( "LOG", "FECHA_LOGOUT=''", "ID,SESION,FECHA_LOGIN" ); //consultamos sesiones abiertas
	$flag=0; //bandera para saber si debemos cerrar la sesion o dejarla abierta
	
	while( $buf= mysqli_fetch_array($log_cons) ) {
		//si la SESION es igual, aun esta activo en la web
		if( !strcmp( session_id(), $buf["SESION"] ) ) 
			$flag=1; //bandera activada
		
		//si la sesion no esta activada, entonces posiblemente no este conectado
		if( $flag==0 ) {
			if( ($buf["FECHA_LOGIN"]+(10*60))<time() ) { //si el tiempo se vencio se cierra la sesion
				$trama= array(
					"ID"=>"'". $buf["ID"]. "'", 
					"FECHA_LOGOUT"=>"'". ($buf["FECHA_LOGIN"]+(10*60)). "'"
				);
				
				actualizar_bdd( "LOG", $trama );
			}
		}
		else { //entonces esta conectado, se actualiza el tiempo de sesion
			$trama= array(
				"ID"=>"'". $buf["ID"]. "'", 
				"FECHA_LOGIN"=>"'". time(). "'" 
			);
				
			actualizar_bdd( "LOG", $trama );
		}
				
		unset($trama);
		$flag=0; //desactivamos bandera
	}
	
	unset($buf, $log_cons);
}

?>
