<?php
/*
OneFloor-PHP v1.0 :: Sistema Base para iniciar tus proyectos en PHP

Sistema base para iniciar tus proyectos en PHP, con la finalidad de facilitar la creacion de aplicaciones apartir de
esta plantilla que pretende ser el primer esclaron para la creacion de proyectos, basandose en la lectura de modulos
pre-programados por ti mismo y carga automatica de estos modulos que tu mismo proporciones.
Este codigo fuente queda reservado a los Derechos de Autor Copyright para SIE-Group con su respectiva Licencia. De modo 
que cualquier alteracion, uso ilegal o publicacion debe ser tal cual esta, conservando los derechos del mismo.

Autor: Angel Cantu Jauregui
Nick: Diabliyo
Web: http://www.sie-group.net/
Blog: http://elite-mexicana.blogspot.com/
Foro: http://foro.sie-group.net/
E-mail: darkdiabliyo@gmail.com

Licencia CreativeCommons
Tipo: Attribution-Noncommercial 2.5 Mexico (http://creativecommons.org/licenses/by-nc/2.5/mx/)
URL de la Licencia: http://creativecommons.org/licenses/by-nc/2.5/mx/
*/

function inicializar_espacio( $id, $op ) {
	$r=0;
	$path= 'clientes/';
	$obj= array( "compras", "facturas", "firma_cert", "notacredito", "uploads", "radian" );

	if( !strcmp($op, "crear") ) { # crear cuenta
		if( !mkdir($path.$id) ) { # carpeta personal
			echo '<br>Error "mkdir" ['. getcwd(). '] ['. $path.$id. ']';
			return 0;
		}
		else {
			foreach( $obj as $key ) {
				if( !mkdir($path.$id.'/'.$key) ) # carpeta
					return 0;
			}
		}
		$r=1;
	}
	else if( !strcmp($op, "del") ) { # eliminar ecuenta
		foreach( $obj as $key ) {
			if( file_exists($path.$id.'/'.$key) ) {
				$fp= opendir($path.$id.'/'.$key); # abrir doc
				while( ($b=readdir($fp))!==FALSE )
					unlink($path.$id.'/'.$key.'/'.$b); # eliminar archivo
				rmdir($path.$id.'/'.$key); # eliminar directorio
				closedir($fp); # cerrar strea,
				unset($b); # limpiar buffer
			}
		}
		rmdir($path.$id); # eliminar directorio principal
		$r=1;
	}

	return $r;
}

# urls amigabeles 
function url_amigable( $id, $titulo, $tipo, $arg )
	{
	$t= url_cleaner(desproteger_cadena($titulo));
	$url= HTTP_SERVER. '/';
	
	if( !HAPPY_URL ) # si NO estan activadad
		return $id;
	
	if( !strcmp($tipo, "blog") )
		{
		$x= explode( "?", $id ); # explotamos URL
		$y= explode( "&", $x[1] ); # explotamos URL 
		$arr= array(); # arreglo de variables 
		for( $i=0; $i<count($y); $i++ ) # recorremosv
			{
			$d= explode("=", $y[$i]);
			$arr[]= $d[1]; # reordenamos
			unset($d);
			}
		$url= HTTP_SERVER. '/blog/'. $arr[0]. '-'. $t. '.html';
		unset($x, $y, $arr);
		return $url;
		}
	else if( !strcmp($tipo, "contenido") )
		{
		if( $t ) # si existe titulo
			$url .= $t. '/';
		
		if( !strstr($arg, ",") ) #si no existe delimitador
			$url .= $arg;
		else
			{
			$x= explode( ",", $arg ); # explotamos
			for( $i=0; $i<count($x); $i++ )
				{
				if( $i )
					$url .= '/';
				$url .= $x[$i]; # concatenamos
				}
			$url .= '.html';
			unset($x, $y );
			}
		return $url;
		}
	else if( !strcmp($tipo, "menu") )
		{
		if( $arg )
			return HTTP_SERVER.'/'. $t. '/'. $arg. '/';
		else
			return HTTP_SERVER.'/'. $t. '/';
		}
	else if( !strcmp($tipo, "users") ) # calcula la profundidad con las variables
		{
		$url .= 'user/'. $t. '/';
		
		$x= explode( "?", $id ); # explotamos
		$y= explode( "=", $x[1] ); # explotamos 
		$url .= $y[1]; # concatenamos
		unset($x, $y );
		return $url;
		}
	else if( !strcmp($tipo, "radio") ) # calcula la profundidad con las variables
		{
		$radio= consultar_datos_general( "ESTACION", "ID_USUARIO='". proteger_cadena($t). "'", "NOMBRE_URL" );
		if( !$radio ) $radio='unknow';
		$url .= 'station/'. $radio;
		if( $arg )
			{
			$b= '';
			if( strstr($arg, ",") ) # si hay delimitador
				{
				$x= explode( ",", $x);
				foreach( $x as $key )
					$b .= '/'. $key;
				unset($x);
				}
			else		$b .= '/'. $arg;
			
			$url .= $b. '.html';
			}
		unset($radio, $b );
		return $url;
		}
	else if( !strcmp($tipo, "radio_sec") ) # calcula la profundidad con las variables
		{
		$radio= consultar_datos_general( "ESTACION", "ID_USUARIO='". proteger_cadena($t). "'", "NOMBRE_URL" );
		if( !$radio ) $radio='unknow';
		$url .= 'station/'. $radio;
		if( $arg )
			{
			$b= '/';
			if( strstr($arg, ",") ) # si hay delimitador
				{
				$x= explode( ",", $x);
				foreach( $x as $key )
					{
					if( strcmp($b, "/") )
						$b .= '/';
					$b .= $key;
					}
				unset($x);
				}
			else		$b .= $arg;
			
			$url .= $b;
			}
		unset($radio, $b );
		return $url;
		}
	else if( !strcmp($tipo, "stream") ) # calcula la profundidad con las variables
		{
		$radio= consultar_datos_general( "ESTACION", "ID_USUARIO='". proteger_cadena($t). "'", "TITULO_URL" );
		if( !$radio ) $radio='unknow';
		$url .= 'stream/'. $radio;
		unset($radio );
		return $url;
		}
	else if( !strcmp($tipo, "perfil") ) # perfiles
		return $url .= 'perfil/'. $t;
	else if( !strcmp($tipo, "login") ) # calcula la profundidad con las variables
		{
		$url .= $t;
		if( !strstr($arg, ",") ) # si no tiene delimitadores
			$url .= '/'. $arg;
		else # la sincronizacion sera por red social
			{
			$x= explode( ",", $arg ); # explotamos
			foreach( $x as $key )
				$url .= '/'. $key;
			unset($x);
			}
		return $url;
		}
	else if( !strcmp($tipo, "script") ) # pagina o script
		{
		$x= explode( ":", $arg );
		if( !strcmp($titulo, "share") ) # si son compartidos, cambiamos titulo
			$t= 'compartir';
		else if( !strcmp($titulo, "votos") ) # si son votos, cambiamos titulo
			$t= 'votar';
		else if( !strcmp($titulo, "add") ) # si son addspot, cambiamos titulo
			$t= 'add';
		
		$url .= $t;
		
		foreach($x as $key ) # ponemos argumentos
			$url .= '/'. $key;
		return $url;
		}
	else if( !strcmp($tipo, "auto") ) # calcula la profundidad con las variables
		{
		if( $t ) # si existe titulo
			$url .= $t. '/';
		
		if( !strstr($arg, ",") ) #si no existe delimitador
			$url .= $arg;
		else
			{
			$x= explode( ",", $arg ); # explotamos
			foreach( $x as $val )
				$url .= $val. '/'; # concatenamos
			unset($x, $y );
			}

		return $url;
		}
	}
	
# forza utilizar www 
function httpwwwforce()
	{
	 # si no contiene www, redirigimos
	 if( !urlparser($_SERVER['HTTP_HOST'], array("www", "0")) )
	 	{
	 	$parsehttp='';

		if( !strcmp($_SERVER['HTTPS'], "on") )
			$parsehttp= 'https://';
		else		$parsehttp= 'http://';

		$parsehttp .= 'www.'. $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'];

		header( "Location: ". $parsehttp );
	 	}
	}

# comprubea url
# arg debe ser un arreglo, el primer valor es la opcion y el segundo, si retornamos una cadena o bit de error o exito
function urlparser( $url, $arg )
	{
	/*
	Ejm: http://username:password@hostname/path?arg=value#anchor
	Array
		(
    	[scheme] => http
    	[host] => hostname
    	[user] => username
    	[pass] => password
    	[path] => /path
    	[query] => arg=value
    	[fragment] => anchor
		)
	*/
	if( !is_array($arg) )		return 0;
	if( count($arg)!=2 )		return 0;
	
	$data= parse_url($url); # parseamos
	
	if( !strcmp($arg[0], "http") ) # forzar usar http
		{
		if( !strcmp($arg[1], "1") ) # retornar cadena
			{
			if( $data['scheme'] ) # si tiene scheme
				$r= $url;
			else		$r= 'http://'. $url;
			}
		else # retorna bit
			{
			if( $data['scheme'] ) # si tiene scheme
				$r=1;
			else		$r=0;
			}
		}

	else if( !strcmp($arg[0], "https") ) # forzar usar https
		{
		if( !strcmp($arg[1], "1") ) # retornar cadena
			{
			if( $data['scheme'] ) # si tiene scheme
				$r= $url;
			else		$r= 'https://'. $url;
			}
		else # retorna bit
			{
			if( $data['scheme'] ) # si tiene scheme
				$r=1;
			else		$r=0;
			}
		}

	else if( !strcmp($arg[0], "www") ) # forzar usar www
		{
		if( !strcmp($arg[1], "1") ) # retornar cadena
			{
			if( strstr($data['path'], "www") ) # si tiene www
				$r= $url;
			else		$r= 'www.'. $url;
			}
		else # retorna bit
			{
			if( strstr($data['path'], "www") ) # si tiene www
				$r=1;
			else		$r=0;
			}
		}
	unset($data);
	return $r; # retornamos respuesta 
	}

function url_cleaner( $data )
	{
	$a= strtolower($data); # pasamos a minusculas 
	$s_letras= array( 'á', 'é', 'í', 'ó', 'ú', 'ñ' ); # buscar letras
	$r_letras= array( 'a', 'e', 'i', 'o', 'u', 'n' ); # sustituir letras 
	$s_signos= array( '/[^a-z0-9_]/' ); # buscar simbolos
	$r_signo= array( '_' );
	
	$a= str_replace( $s_letras, $r_letras, $a ); # re-emplazamos letras
	$a= preg_replace( $s_signos, $r_signo, $a ); # re-emplazamos signos
	
	return $a; 
	}

# limpia la cadena para usarla como 'keyword' content en meta tags, sustituye espacios por ','
# y elimina caracteres raros	
function url_cleaner_meta( $data )
	{
	$a= strtolower($data); # pasamos a minusculas 
	$s_letras= array( 'á', 'é', 'í', 'ó', 'ú', 'ñ', ',' ); # buscar letras
	$r_letras= array( 'a', 'e', 'i', 'o', 'u', 'n', '' ); # sustituir letras 
	$s_signos= array( '/[^a-z0-9_]/' ); # buscar simbolos
	$r_signo= array( ',' );
	
	$a= str_replace( $s_letras, $r_letras, $a ); # re-emplazamos letras
	$a= preg_replace( $s_signos, $r_signo, $a ); # re-emplazamos signos
	
	return $a; 
	}

# limpia la cadena para usarla como 'description' content en meta tags, sustituye saltos por ,	
function url_cleaner_description( $data )
	{
	$a= strtolower($data); # pasamos a minusculas 
	$s_letras= array( '\n', '<br>' ); # buscar letras
	$r_letras= array( ',', ',' ); # sustituir letras 
	$s_signos= array( '/[^a-z0-9_]/' ); # buscar simbolos
	$r_signo= array( ',' );
	
	$a= str_replace( $s_letras, $r_letras, $a ); # re-emplazamos letras
	# $a= preg_replace( $s_signos, $r_signo, $a ); # re-emplazamos signos
	
	return $a; 
	}

# acomoda el mensaje para usarse en sociales
function url_cleaner_tinymsg( $data )
	{
	$a= strtolower($data); # pasamos a minusculas 
	$s_letras= array( '-', ',', '_', '~', ':' ); # buscar letras
	$r_letras= array( '', '', '', '', '' ); # sustituir letras
	
	$a= str_replace( $s_letras, $r_letras, $a ); # re-emplazamos letras
	return $a;
	}

# obtiene la IP del usuario actual y si esta por proxy tambien la IP de este 
function get_ip()
	{
	$ip= $_SERVER['REMOTE_ADDR']; //obtenemos IP
	
	if( isset($_SERVER['HTTP_X_FORWARDED_FOR']) )
		$proxy_ip= $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if( isset($_SERVER['HTTP_VIA']) )
		$proxy_ip= $_SERVER['HTTP_VIA'];
	else 	 $proxy_ip=0;
	
	if( $proxy_ip ) # si existe IP de proxy
		return $proxy_ip. ','. $ip; # retornamos ambas IPs: IP Real, IP Proxy 
	else	return $ip; # solo IP, ya que es su IP real
	}

function validar_tipo_de_formato( $tipo, $formato )
	{
	if( !strcmp( $formato, "mixto") )
		return 1;
	else if( !strcmp( $formato, "imagen") )
		$arr= array( "jpg", "jpeg", "gif" ); # formatos validos
	else if( !strcmp( $formato, "video") )
		$arr= array( "flv", "ogg" ); # formatos validos
	else if( !strcmp( $formato, "archivo") )
		$arr= array( "doc", "odt", "xls", "pdf", "txt" ); # formatos validos
	else if( !strcmp( $formato, "audio") )
		$arr= array( "mp3" ); # formatos validos
	else return 0;
	
	for( $i=0; $i<count($arr); $i++ )
		{
		#if( strstr($tipo, $arr[$i]) )
		if( !strcmp($tipo, $arr[$i]) )
			return 1;
		}
	return 0;
	}

# valida si un enlace web es valido 
function validar_enlaceweb( $url )
	{
	if( !strcmp($url, "") ) return 0; # si esta vacio o sin contenido 
	else if( gethostbyname($url) )	return 1; # enlace valido 
	return 0; # no fue valido 
	}

function leer_formato( $tipo )
	{
	$arr= array( "jpg"=>"imagen", "jpeg"=>"imagen", "gif"=>"imagen", "flv"=>"video",  "ogg"=>"video", "doc"=>"archivo", 
			"odt"=>"archivo", "xls"=>"archivo", "pdf"=>"archivo", "txt"=>"archivo", "mp3"=>"audio" ); # formatos valido
	
	foreach( $arr as $key=>$val )
		{
		if( !strcmp($tipo, $key) )
			return $val;
		}
	return 0;
	}
	
function get_mime_format( $archivo )
	{
	$arr= array( "jpg"=>"image/jpg", "jpeg"=>"image/jpg", "png"=>"image/png", "gif"=>"image/gif", "swf"=>"application/x-shockwave-flash", 
	"flv"=>"video/flv", "doc"=>"application/doc", "odt"=>"application/odt", "xls"=>"application/xls", "pdf"=>"application/pdf", "txt"=>"text/plain", 
	"mp3"=>"audio/mp3", "html"=>"application/html", "tar.gz"=>"application/gzip", "rar"=>"application/rar", "zip"=>"application/zip"); # formatos validos
	
	foreach($arr as $key=>$val )
		{
		#if( strstr($tipo, $arr[$i]) )
		if( !strstr($archivo, $key) )
			return $val; # retornamos mime 
		}
	return 'application/octet-stream'; # desconocido mime
	}

function acento( $var )
	{
	return '&'. $var. 'acute;';
	}

# genera semilla conforme al reloj
function make_seed() {
  	list($usec, $sec) = explode(' ', microtime());
  	return (float) $sec + ((float) $usec * 100000);
}

function generar_idtrack()
	{
	$min= 4;
	$limite= 10;
	srand(make_seed());
	$dimension = rand($min, $limite);

	$arr_abc123= array( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 
							'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'y', 'y', 'z' );
						
	$randval="";
	for( $i=0; $i<$dimension; $i++ )
		$randval .= $arr_abc123[rand(1, count($arr_abc123) )];

	return $randval;
	}

function generar_idnumber( $dato ) {
	if( !$dato["min"] || !$dato["max"] )
		return 0;

	srand(make_seed());
	$dim= rand($dato["min"], $dato["max"]);
	$arr_123= array( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' );
	$r='';
	for($i=0; $i<$dim; $i++ )
		$r .= $arr_123[rand(1,count($arr_123))];
	unset($min, $max, $dim);

	return $r;
}
	
//generar log del ultimo envio de mail
function last_sendmail_log( $email )
	{
	$file="last_sendmail_log.txt"; //archivo log
	$modo="w"; //abrimos en modo creacion nueva
	
	$fp= fopen( $file, $modo ); //abrimos stream
	fwrite( $fp, $email );
	fclose($fp);
	}

//genera, crea e incrementa log del smtp relay
function generar_log_relay_smtp( $valor )
	{
	$file="log_smtp.txt"; //archivo log

	if( !file_exists($file) ) //no existe archivo
		$modo="w"; //abrimos en modo creacion nueva
	else	$modo="r"; //abrimos en modo lectura

	$fp= fopen( $file, $modo ); //abrimos stream
	if( !strcmp($modo, "r") ) //si es en modo lectura
		{
		$buf= fgets( $fp, 1024 ); //leemos linea del archivo
		$x= explode( ":", $buf ); //explotamos 
		$tiempo= $x[0]; //tomamos tiempo 
		$cont= $x[1]; //tomamos valor contador 
		unset($x);
		unset($buf);
		fclose($fp); //cerramos stream
		unset($fp); //limpiamos
		
		$fp= fopen( $file, "w" ); //abrimos stream en modo escritura 
		}
	else //es modo escritura, nuevo log
		$tiempo= time(); //establecemos tiempo 

	//escribimos valores
	if( !strcmp( date("d/m/y", time()), date("d/m/y", $tiempo) ) ) //si la fecha d/m/y son iguales 
		$cont= ($cont+$valor); //incrementamos contador
	else
		{
		$tiempo= time();
		$cont= $valor;
		}
		
	$buf= $tiempo. ':'. $cont; //creamos buffer
	fputs( $fp, $buf ); //escribimos buffer
	
	fclose($fp);
	unset($modo);
	unset($buf);
	unset($file);
	
	return $cont; //retornamos el contador
	}

# navegador movil
function get_navegador($t)
	{
	$r=0;
	$nav= $_SERVER['HTTP_USER_AGENT'];

	if( strcmp( $_SERVER['HTTP_REFERER'], "" ) )
		$ref= strtolower($_SERVER['HTTP_REFERER']);
	else
		$ref= strtolower($_SERVER['HTTP_HOST']);

	$navegador=0;
	$so=0;
	if( is_array($nav) ) {
		foreach( $nav as $key=>$val )
			{
			if( !strcmp($key, "parent" ) )
				$navegador= $val;
			else if( !strcmp($key, "platform" ) )
				$so= $val;
			}
	}

	if( !strcmp( $t, "all" ) ) # todo el array
		$r= $nav;
	else if( !strcmp($t, "name") ) # nombre del navegador
		$r= $navegador;
	else if( !strcmp($t, "version") ) # version del navegador
		$r= $navegador;
	else if( !strcmp($t, "os") ) # sistema operativo
		$r= $so;
	else if( !strcmp($t, "tipo") ) # tipo de navegador: movil o desktop
		{
		if( strstr(strtolower($so), "and") )					$r= 'movil';
		else if( strstr(strtolower($so), "ipho") )			$r= 'movil';
		else if( strstr(strtolower($so), "symb") )			$r= 'movil';
		else if( strstr(strtolower($so), "blackberry") )	$r= 'movil';
		else if( strstr(strtolower($so), "nok") )			$r= 'movil';
		else if( strstr(strtolower($so), "sams") )			$r= 'movil';
		else		$r= 'desktop';
		}

	unset($nav, $so, $navegador);
	return $r;
	}

//Funcion para comprobar que es un usuario logeado legitimo 
function is_login() {
	$data= consultar_datos_general( "USUARIOS", "USUARIO='". proteger_cadena($_SESSION["log_usr"]). "':CLAVE='". proteger_cadena($_SESSION["log_pwd"]). "'", "ID" );
	if( !strcmp( $data, $_SESSION["log_id"] ) ) {
		unset($data);
		return 1;
	}
	else {
		$path= 'cuenta/info';
		$data= array(); # vacio
		// $mbox= new moneyBox($_SESSION["log_usr"], $_SESSION["log_pwd"], $path, $data);

		// if( $mbox->getError() )
		// 	return 0;
		// else
		// 	return 1;
	}
	unset($data);
	return 0;
}
	
// root de la plataforma
function is_admin() {
	$data= consultar_datos_general( "USUARIOS", "USUARIO='". proteger_cadena($_SESSION["log_usr"]). "':CLAVE='". proteger_cadena($_SESSION["log_pwd"]). "'", "PRIVILEGIO" );
	if( !strcmp( $data, "1" ) ) {
		unset($data);
		return 1;
	}
	unset($data);
	return 0;
}

// empresa
function is_empresa() {
	$data= consultar_datos_general( "USUARIOS", "USUARIO='". proteger_cadena($_SESSION["log_usr"]). "':CLAVE='". proteger_cadena($_SESSION["log_pwd"]). "'", "PRIVILEGIO" );
	if( !strcmp( $data, "2" ) ) {
		unset($data);
		return 1;
	}
	unset($data);
	return 0;
}

// usuario del sistema
function is_usuario() {
	$data= consultar_datos_general( "USUARIOS", "USUARIO='". proteger_cadena($_SESSION["log_usr"]). "':CLAVE='". proteger_cadena($_SESSION["log_pwd"]). "'", "PRIVILEGIO" );
	if( !strcmp( $data, "3" ) ) {
		unset($data);
		return 1;
	}
	unset($data);
	return 0;
}

// obtiene el nombre del usuario maestro
function get_userMaster($idUser=NULL) {
	return (!$idUser ? 0:consultar_datos_general("USUARIOS", "ID='". proteger_cadena($idUser). "'", "USUARIO"));
}

// obtiene el nombre del usuario maestro
function get_userNamePrint($idUser=NULL) {
	$uName= consultar_datos_general("USUARIOS", "ID='". proteger_cadena($idUser). "'", "USUARIO");
	$x= (strstr($uName, "_") ? explode("_", $uName):$uName);
	return (strstr($uName, "_") ? $x[1]:$uName);
}


# Funcion para comprobar si es un robot 
function is_a_robot( $nav, $host )
	{
	# lista de robots 
	$robots= array( 
			"google"=>"Google", 
			"slurp"=>"Yahoo",
			"yahoo"=>"Yahoo", 
			"scooter"=>"Altavista", 
			"lnktomi"=>"Altavista", 
			"baidu"=>"Baidu/China", 
			"yandex"=>"Yandex/Rusia", 
			"msn"=>"MSN Microsoft", 
			"rippers"=>"Robot Rippers", 
			"feeds_blogs"=>"Feed", 
			"sogou"=>"Sogou/China", 
			"entireweb"=>"Entireweb", 
			"general_crawler"=>"General Crawler", 
			"facebook"=>"Facebook", 
			"spiderduck"=>"Twitter", 
			"amazon"=>"Amazon Web Servers", 
			"search_engines"=>"Topsy" );
	
	$m= url_cleaner(strtolower($nav)); # convertimos a minusculas
	$n= strtolower($host); # convertimos a minusculas 
	foreach( $robots as $key=>$val )
		{
		if( strstr($m, $key) || strstr($n, $key) ) # si existe valor, es robot 
			return 'Indexado por '. $val; # retornamos nombre del indexador 
		}
	return 0;
	}

# Funcion para obtener el nombre real del robot 
function get_name_robot( $nav, $host )
	{
	# lista de robots 
	$robots= array( 
			"google"=>"Google", 
			"slurp"=>"Yahoo",
			"yahoo"=>"Yahoo", 
			"scooter"=>"Altavista", 
			"lnktomi"=>"Altavista", 
			"baidu"=>"Baidu/China", 
			"yandex"=>"Yandex/Rusia", 
			"msn"=>"MSN Microsoft", 
			"rippers"=>"Robot Rippers", 
			"feeds_blogs"=>"Feed", 
			"sogou"=>"Sogou/China", 
			"entireweb"=>"Entireweb", 
			"general_crawler"=>"General Crawler", 
			"facebook"=>"Facebook", 
			"spiderduck"=>"Twitter", 
			"amazon"=>"Amazon Web Servers", 
			"search_engines"=>"Topsy" );
	
	$m= url_cleaner(strtolower($nav)); # convertimos a minusculas
	$n= strtolower($host); # convertimos a minusculas 
	foreach( $robots as $key=>$val )
		{
		if( strstr($m, $key) || strstr($n, $key) ) # si existe valor, es robot 
			return $key; # retorno el nombre del robot  
		}
	return 0;
	}

//Funcion para conectarse a la pagina
function autenticacion( $log ) {
	if( $log ) {
		$ref= $_SERVER['HTTP_REFERER'];
		if( strstr($ref, "/error") || strstr($ref, "/ban") ) {
			$ref= str_replace(array('/error', '/ban'), array('', ''), $ref);
		}

		if( !strcmp($log, "token") ) {
			if( !consultar_datos_general("USUARIOS", "TOKEN='". proteger_cadena($_GET["token"]). "'", "ID") ) {
				echo '<div class="w3-panel w3-padding w3-leftbar w3-border-red w3-pale-red w3-left-align">El codigo de autenticaci&oacute;n no es valido..</div>';
			}
			else {
				$_SESSION["log_usr"]= consultar_datos_general("USUARIOS", "TOKEN='". proteger_cadena($_GET["token"]). "'", "USUARIO");
				$_SESSION["log_pwd"]= consultar_datos_general("USUARIOS", "TOKEN='". proteger_cadena($_GET["token"]). "'", "CLAVE");
				$_SESSION["log_id"]= consultar_datos_general("USUARIOS", "TOKEN='". proteger_cadena($_GET["token"]). "'", "ID");
				$_SESSION["SUPERID"]= (is_empresa() ? $_SESSION["log_id"]:consultar_datos_general( "USUARIOS", "USUARIO='". proteger_cadena($_SESSION["log_usr"]). "' && CLAVE='". proteger_cadena($_SESSION["log_pwd"]). "'", "ID_EMPRESA" ));

				if( $_SESSION["ref"] ) # si existe referencia
					$ref= $_SESSION["ref"];
				else		$ref= '/';

				echo '<div class="w3-panel w3-padding w3-leftbar w3-border-green w3-pale-green w3-left-align">Autenticaci&oacute;n exitosa, redireccionando..</div>';
				header( "Location: ". $ref );
			}
		}
		else if( !strcmp($log, "entrar") ) {
			if( !strcmp($_POST["log_usr"], "admin") && $_POST["portal"] ) {
				$subFix= $_POST["portal"];
				$_POST["log_usr"]='';
				$user_site= proteger_cadena($subFix);
			}
			else  {
				$subFix= ($_POST["portal"] ? $_POST["portal"].'_':'');
				$user_site= proteger_cadena($subFix.$_POST["log_usr"]);
			}

			$pass_site= md5($_POST["log_pass"]);

			$gl= new googleService($_POST["g-recaptcha-response"]);
			$gl->setServiceType("recaptcha");
			$gl->sendRecaptcha();

			if( $gl->getError() ) {
				header( "Location: ". $ref."/recaptcha" ); 
			}
			else if( bruteforcing_detect($user_site, substr($subFix, 0, -1)) ) # si se detecta bruteforcing
				header( "Location: ". $ref."/ban" ); 
			else if( login( $user_site, $pass_site ) ) { # login en la DB
				$_SESSION["log_usr"]= proteger_cadena($subFix.$_POST["log_usr"]);
				$_SESSION["log_pwd"]= md5($_POST["log_pass"]);
				$_SESSION["log_id"]= consultar_datos_general( "USUARIOS", "USUARIO='". proteger_cadena($_SESSION["log_usr"]). "' && CLAVE='". proteger_cadena($_SESSION["log_pwd"]). "'", "ID" );
				$_SESSION["pais"]= consultar_datos_general("USUARIOS", "ID='".$_SESSION["log_id"]."'", "PAIS");
				$_SESSION["BLOCK_DOC_RECIBIDO"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["log_id"]."'", "BLOCK_DOC_EMITIDO")==1 ? true:false);
				$_SESSION["BLOCK_DOC_EMITIDO"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["log_id"]."'", "BLOCK_DOC_RECIBIDO")==1 ? true:false);
				$_SESSION["VIEW_DOC_RECIBIDO"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["log_id"]."'", "VIEW_DOC_EMITIDO")==1 ? true:false);
				$_SESSION["VIEW_DOC_EMITIDO"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["log_id"]."'", "VIEW_DOC_RECIBIDO")==1 ? true:false);
				$_SESSION["SUPERID"]= (is_empresa() ? $_SESSION["log_id"]:consultar_datos_general( "USUARIOS", "USUARIO='". proteger_cadena($_SESSION["log_usr"]). "' && CLAVE='". proteger_cadena($_SESSION["log_pwd"]). "'", "ID_EMPRESA" ));
				$_SESSION["EXPIRACION_SESION"]= consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "PASS_SES_TIME");
				$_SESSION["PAGINACION_EMITIDOS"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "PAGINACION_EMITIDOS") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "PAGINACION_EMITIDOS"):0);
				$_SESSION["PAGINACION_RECIBIDOS"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "PAGINACION_RECIBIDOS") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "PAGINACION_RECIBIDOS"):0);

				$_SESSION["ICON_030_VIEW"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_VIEW") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_VIEW"):0);
				$_SESSION["ICON_030_TAB"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_TAB") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_TAB"):0);
				$_SESSION["ICON_030_SENDMAIL"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_SENDMAIL") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_SENDMAIL"):0);
				$_SESSION["ICON_030_TEXTVIEW"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_TEXTVIEW") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_TEXTVIEW"):0);

				$_SESSION["ICON_030_TEXTICON"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_TEXTICON") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_TEXTICON"):0);
				$_SESSION["ICON_030_TEXTICON_NO"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_TEXTICON_NO") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_030_TEXTICON_NO"):0);
				$_SESSION["ICON_031_TEXTICON"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_031_TEXTICON") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_031_TEXTICON"):0);
				$_SESSION["ICON_031_TEXTICON_NO"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_031_TEXTICON_NO") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_031_TEXTICON_NO"):0);
				$_SESSION["ICON_032_TEXTICON"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_032_TEXTICON") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_032_TEXTICON"):0);
				$_SESSION["ICON_032_TEXTICON_NO"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_032_TEXTICON_NO") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_032_TEXTICON_NO"):0);
				$_SESSION["ICON_033_TEXTICON"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_033_TEXTICON") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_033_TEXTICON"):0);
				$_SESSION["ICON_033_TEXTICON_NO"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_033_TEXTICON_NO") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_033_TEXTICON_NO"):0);
				$_SESSION["ICON_034_TEXTICON"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_034_TEXTICON") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_034_TEXTICON"):0);
				$_SESSION["ICON_034_TEXTICON_NO"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_034_TEXTICON_NO") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_034_TEXTICON_NO"):0);

				$_SESSION["ICON_031_VIEW"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_031_VIEW") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_031_VIEW"):0);
				$_SESSION["ICON_031_SENDMAIL"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_031_SENDMAIL") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_031_SENDMAIL"):0);
				$_SESSION["ICON_031_TEXTVIEW"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_031_TEXTVIEW") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_031_TEXTVIEW"):0);
				$_SESSION["ICON_032_VIEW"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_032_VIEW") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_032_VIEW"):0);
				$_SESSION["ICON_032_SENDMAIL"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_032_SENDMAIL") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_032_SENDMAIL"):0);
				$_SESSION["ICON_032_TEXTVIEW"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_032_TEXTVIEW") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_032_TEXTVIEW"):0);
				$_SESSION["ICON_033_VIEW"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_033_VIEW") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_033_VIEW"):0);
				$_SESSION["ICON_033_SENDMAIL"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_033_SENDMAIL") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_033_SENDMAIL"):0);
				$_SESSION["ICON_033_TEXTVIEW"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_033_TEXTVIEW") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_033_TEXTVIEW"):0);
				$_SESSION["ICON_BLOCK_VIEW"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_BLOCK_VIEW") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_BLOCK_VIEW"):0);
				$_SESSION["ICON_BLOCK_SENDMAIL"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_BLOCK_SENDMAIL") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_BLOCK_SENDMAIL"):0);
				$_SESSION["ICON_BLOCK_TEXTVIEW"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_BLOCK_TEXTVIEW") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_BLOCK_TEXTVIEW"):0);
				$_SESSION["FROM_MAIL_EMIS_NOTIF"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "FROM_MAIL_EMIS_NOTIF") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "FROM_MAIL_EMIS_NOTIF"):0);
				$_SESSION["FROM_MAIL_RECP_NOTIF"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "FROM_MAIL_EMIS_NOTIF") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "FROM_MAIL_EMIS_NOTIF"):0);
				$_SESSION["MAIN_COMPANY"]= consultar_datos_general("NIT_SUBEMPRESAS", "ID_EMPRESA='".$_SESSION["SUPERID"]."' && SET_DEFAULT='1'", "ID");
				$_SESSION["ICON_MAIL"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_MAIL") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "ICON_MAIL"):0);
				$_SESSION["SENDMAIL_AUTO"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "SENDMAIL_AUTO") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "SENDMAIL_AUTO"):0);
				$_SESSION["SENDMAIL_TESTMODE"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "SENDMAIL_TESTMODE") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "SENDMAIL_TESTMODE"):0);
				$_SESSION["REPORTE_EMIT_MAX"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "REPORTE_EMIT_MAX") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "REPORTE_EMIT_MAX"):500);
				$_SESSION["REPORTE_RECIB_MAX"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "REPORTE_RECIB_MAX") ? consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "REPORTE_RECIB_MAX"):500);
				$_SESSION["STATUS_SERVER"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "STATUS_SERVER_ENTIDAD") ? 1:0);
				$_SESSION["STATUS_SERVER_SANDBOX"]= (consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "STATUS_SERVER_SANDBOX_ENTIDAD") ? 1:0);
				$_SESSION["VIEW_LOG"]= consultar_datos_general("USUARIOS", "ID='".$_SESSION["log_id"]."'", "VIEW_LOG");
				$_SESSION["OUTFILEMAME_DOC_EMIT"]= consultar_datos_general("USUARIOS", "ID='".$_SESSION["SUPERID"]."'", "FILENAME_EMIT");
				$_SESSION["RECOVERY_EMISION"]= consultar_datos_general("USUARIOS", "ID='".$_SESSION["log_id"]."'", "RECOVERY_EMISION");
				$_SESSION["OTP_SECRET"]= consultar_datos_general("USUARIOS", "ID='".$_SESSION["log_id"]."'", "OTP_SECRET");
				$_SESSION["OTP_ACCESS"]= ($_SESSION["OTP_SECRET"]==1 ? 1:0);
				
				bruteforcing_del($_SESSION["log_usr"]); # eliminamos la IP del Bruteforcing (si es que existe)

				if( !$_SESSION["OTP_SECRET"] ) {
					$ref .= "/2factor";
				}
				else if( $_SESSION["OTP_SECRET"]!=1 && isset($_SESSION["OTP_SECRET"]) && !is_numeric($_SESSION["OTP_SECRET"]) ) {
					$ref .= "/2factor";
				}
				// else if( $_SESSION["OTP_SECRET"]!=1 && isset($_SESSION["OTP_SECRET"]) && !is_numeric($_SESSION["OTP_SECRET"]) && $_SESSION["otp_access"]!=1) {
				// 	// echo '<br>Redireccion a: '. $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]."/2factor";
				// 	$ref .= "/2factor";
				// }
				else {
					if( $_SESSION["ref"] ) # si existe referencia
						$ref= $_SESSION["ref"];
					else
						$ref= '/';
				}
				header( "Location: ". $ref );
			}
			else {
				// $path= 'cuenta/info';
				// $data= array(); # vacio
				// $mbox= new moneyBox($user_site, $pass_site, $path, $data);

				// if( $mbox->getError() ) {
				// 	$ref= url_amigable( "?log=error", "log", "login", "error");
				// }
				// else {
				// 	$r= $mbox->getRespuesta();

				// 	$_SESSION["log_usr"]= proteger_cadena($_POST["log_usr"]);
				// 	$_SESSION["log_pwd"]= proteger_cadena($_POST["log_pass"]);
				// 	$_SESSION["log_id"]= $r->result->id;
				// 	$_SESSION["log_src"]= $r->result->id_cuentaorig;
				// 	$_SESSION["descuentos"]= $r->result->descuentos;
				// 	$_SESSION["modulos"]= $r->result->modulos;
				// 	$_SESSION["empresa"]= $r->result->empresa;
				// 	$_SESSION["sucursales"]= $r->result->sucursales;
				// 	$_SESSION["cuenta"]= $r->result->tipo_cuenta; # tipo de cuenta: 1=> sistema, 2=> distribuidor
				// 	$_SESSION["cuenta_cl"]= (!strcmp($r->result->tipo_cuenta, "2") ? $r->result->tipo_cuenta_idcl:0);
				// 	$_SESSION["usuarios"]= $r->result->usuarios;
				// 	bruteforcing_del(); # eliminamos la IP del Bruteforcing (si es que existe)
				// 	if( $_SESSION["ref"] ) # si existe referencia
				// 		$ref= $_SESSION["ref"];
				// 	else		$ref= '/';
				// 	header( "Location: ". $ref );
				// }

				if( consultar_datos_general("USUARIOS", "USUARIO='". proteger_cadena($user_site). "'", "ID") ) { # existe el usuario, activamos baneo despues de X intentos
					bruteforcing_add($user_site, substr($subFix, 0, -1));
				}

				$_SESSION["log_error"]=1;
				header( "Location: ". $ref."/error" );
			}
		}
		else if( !strcmp($log, "salir") ) {
			//$trama= array(
			//		"SESION"=>"'". session_id(). "'",
			//		"FECHA_LOGOUT"=>"'". time(). "'"  
			//		);
			//actualizar_bdd( "LOG", $trama );
			//unset($trama);
			if( is_usuario() || is_empresa() )
				$url= HTTP_SERVER.'/portal/'. get_userMaster($_SESSION["SUPERID"]);
			else
				$url= HTTP_SERVER;

			session_destroy();
			header( "Location: ". $url);
			// header( "Location: /");
		}
	}
}

# detecta si una IP esta haciendo bruteforcing
function bruteforcing_detect($idU=NULL, $idM=NULL) {
	$idUser= consultar_datos_general("USUARIOS", "USUARIO='". proteger_cadena($idU). "'", "ID");
	$idMaster= consultar_datos_general("USUARIOS", "USUARIO='". proteger_cadena($idM). "'", "ID");
	$maxTime= consultar_datos_general("USUARIOS", "ID='". proteger_cadena($idMaster). "'", "PASS_USE_BANTIME");

	if( consultar_datos_general("BAN_LIST", "ID_USUARIO='". proteger_cadena($idUser). "' && ACTIVO='1' && BAN='1'", "ID") ) { // esta banneado
		$fecha= consultar_datos_general("BAN_LIST", "ID_USUARIO='". proteger_cadena($idUser). "' && ACTIVO='1' && BAN='1'", "FECHA_START_BAN");
		$pasTime= $fecha+($maxTime*60);

		//       85no    |   nuaoif3i  |     1      | 1681235043 |1681236239|     2023-04-11,12:44          |     2023-04-11,13:03
		// $debug= $idUser."|".$idMaster."|".$maxTime."|".$pasTime."|".time()."|".date("Y-m-d,H:i", $pasTime)."|".date("Y-m-d,H:i", time());
		// actualizar_bdd("BAN_LIST", array("id"=>"'". consultar_datos_general("BAN_LIST", "ID_USUARIO='". proteger_cadena($idUser). "' && ACTIVO='1' && BAN='1'", "ID"). "'", "debug"=>"'". $debug. "'"));

		if( $pasTime<time() ) { // ya paso el tiempo necesario, quitamos baneo
			bruteforcing_del($idU); // quitamos bloqueo

			return false;
		}
		else // sigue bloqueado
			return true; // si bloqueado
	}
	else
		return false; // no
}

# agregue la IP a la BDD de posibles bruteforcing
function bruteforcing_add($idU=NULL, $idM=NULL) {
	$idUser= consultar_datos_general("USUARIOS", "USUARIO='". proteger_cadena($idU). "'", "ID");
	$idMaster= consultar_datos_general("USUARIOS", "USUARIO='". proteger_cadena($idM). "'", "ID");
	$ip_src= get_ip(); # obtenemos IP
	$ip_proxy=0; # variable para IP del proxy 
	if( strstr($ip_src, ",") ) { # si existe delimitador, existe Ip y proxy 
		$x= explode( ",", $ip_src ); # explotamos 
		$ip= $x[0];
		$ip_proxy= $x[1];
	}
	else # solo es IP
		$ip= $ip_src; # metemos IP real/normal 

	$mxIntentos= consultar_datos_general("USUARIOS", "ID='". proteger_cadena($idMaster). "'", "PASS_USE_MAXFAIL");
	
	if( consultar_datos_general("BAN_LIST", "ID_USUARIO='". proteger_cadena($idUser). "' && ACTIVO='1' && BAN='0'", "ID") ) { // si existe activo
		$idBan= consultar_datos_general("BAN_LIST", "ID_USUARIO='". proteger_cadena($idUser). "' && ACTIVO='1' && BAN='0'", "ID");
		$try= ((consultar_datos_general("BAN_LIST", "ID_USUARIO='". proteger_cadena($idUser). "' && ACTIVO='1' && BAN='0'", "TRY"))+1);

		if( $try>=$mxIntentos ) { // supero los intentos
			$ban=1; // se bannea
			$start= time();
		}
		else {
			$ban=0;
			$start=0;
		}

		$trama= array(
			"id"=>"'". $idBan. "'", 
			"id_usuario"=>"'". proteger_cadena($idUser). "'",
			"try"=>"'". $try. "'", 
			"ban"=>"'". $ban. "'", 
			"fecha_start_ban"=>"'". $start. "'"
		);
	}
	else {
		$try= 1;
		$trama= array(
			"id_usuario"=>"'". proteger_cadena($idUser). "'",
			"ip"=>"'". proteger_cadena($ip). "'", 
			"ip_proxy"=>"'". proteger_cadena($ip_proxy). "'", 
			"fecha"=>"'". time(). "'", 
			"try"=>"'". $try. "'", 
			"activo"=>"'1'", 
			"ban"=>"'0'"
		);
	}
	
	if( $try==1 )	
		insertar_bdd( "BAN_LIST", $trama ); # insertamos nueva IP target que hace bruteforcing 
	else 
		actualizar_bdd( "BAN_LIST", $trama ); # insertamos nueva IP target que hace bruteforcing 

	unset($trama);
}

# elimina la IP a la BDD de posibles bruteforcing
function bruteforcing_del($idU=NULL) {
	$idUser= consultar_datos_general("USUARIOS", "USUARIO='". proteger_cadena($idU). "'", "ID");
	// $idMaster= consultar_datos_general("USUARIOS", "ID='". proteger_cadena($idM). "'", "ID");

	$trama= array(
		"id_usuario"=>"'". proteger_cadena($idUser). "'", 
		"activo"=>"'0'", // desactivar todo
		"ban"=>"'0'"
	);
	actualizar_bdd("BAN_LIST", $trama);
}
	
// Funcion que comprueba si una seccion es libre
function seccion_libre( $seccion_id )
	{
	$cons= consultar( "PRIVILEGIOS", "*" ); # consultamos tabla
	$buf= mysqli_fetch_array($cons);
	
	if( mysqli_num_rows($cons) ) # si existen datos
		{
		while( $buf= mysqli_fetch_array($cons) ) # ciclo
			{
			if( strcmp( $buf["SECCIONES"], "0") && strcmp( $buf["SECCIONES"], "vacio") && strcmp( $buf["SECCIONES"], "") ) # si el privilegio tiene contenido
				if( strchr( $buf["SECCIONES"], $seccion_id) ) # verificamos existencia del ID
					{
					limpiar($cons);
					return 0; # si esta el ID, seccion privada :D
					}
			}
		}
		
	unset($buf);
	limpiar($cons);
	return 1; # no existio ID de la seccion, es una seccion libre
	}

//Convierte de BBCode a HTML Tags
function grupos_comprueba_miembro( $grupos, $seccion )
	{
	if( !strcmp($grupos, "") || !strcmp($grupos, "vacio") )	return 1;	
	else if( strchr( $grupos, ":" ) ) //si existe delimitador, entonces existe mas de un valor
		{
		$x= explode( ":", $grupos ); //cortamos
		
		for( $i=0; $i<sizeof($x); $i++ )
			if( !strcmp($x[$i], $seccion) ) //si la seccion existe
				return 0;	//regresamos 0, ya existe
				
		return 1; //en este punto, la seccion no se encontro y es posible insertarla
		
		unset($x);
		}
	else //solo existe un valor
		{
		if( !strcmp($grupos, $seccion) )	return 0; //entonces ya existe esta seccion en los grupos
		return 1; //no existe la seccion en los grupos
		}
	}
	
function msg2msgtags( $mensaje )
	{
	/*
	strchr( lugar, palabraclave )  busca la "palabraclave" en "lugar"
	str_replace( palabraclave, sustituto, lugar )  busca en "lugar" la "palabraclave" y la sustituye por "sustituto"
	*/
   $cad_buscar= array(
   				'/\[h1\](.*?)\[\/h1\]/is', 
   				'/\[amp\]/is',
   				'/\[capa\=(.*?)\](.*?)\[\/capa\]/is',
   				'/\[span\=(.*?)\](.*?)\[\/span\]/is',
   				'/\[center\](.*?)\[\/center\]/is',
   				'/\[table\](.*?)\[\/table\]/is',
   				'/\[th\](.*?)\[\/th\]/is',
   				'/\[td\](.*?)\[\/td\]/is',
   				'/\[tr\]/is',
   				'/\[list\](.*?)\[\/list\]/is',
   				'/\[li\](.*?)\[\/li\]/is',
   				'/\[b\](.*?)\[\/b\]/is',
   				'/\[i\](.*?)\[\/i\]/is',
   				'/\[u\](.*?)\[\/u\]/is',
   				'/\[s\](.*?)\[\/s\]/is',
   				'/\[url\=(.*?)\](.*?)\[\/url\]/is',                         
   				'/\[url\](.*?)\[\/url\]/is',                             
   				'/\[align\=(left|center|right|justify)\](.*?)\[\/align\=(left|center|right|justify)\]/is',
   				'/\[img\](.*?)\[\/img\]/is',                            
   				'/\[font\=(.*?)\](.*?)\[\/font\]/is',                    
   				'/\[size\=(.*?)\](.*?)\[\/size\]/is',                    
   				'/\[color\=(.*?)\](.*?)\[\/color\]/is',
   				'/\[code\=php\](.*?)\[\/code\]/is', 
   				'/\[code\](.*?)\[\/code\]/is',
   				'/\[quote\](.*?)\[\/quote\]/is',
   				'/\[youtube](.*?)\[\/youtube\]/is',
   				'/\[swf\](.*?)\[\/swf\]/is',
   				'/\[swf (.*?) (.*?)\](.*?)\[\/swf\]/is',
   				'/\[tab\]/is', 
   				'/\[play_mp3\](.*?)\[\/play_mp3\]/is', 
   				'/\[video (.*?) (.*?)\](.*?)\[\/video\]/is',
   				'/\[video\](.*?)\[\/video\]/is', 
   				'/\[video_flash\](.*?)\[\/video_flash\]/is', 
   				'/\[video_flash (.*?) (.*?)\](.*?)\[\/video_flash\]/is', 
   				'/\[vimeo](.*?)\[\/vimeo\]/is', 
   				'/\[twitcam\](.*?)\[\/twitcam\]/is'
   				);
/*
<iframe src="http://player.vimeo.com/video/18073096" width="400" height="225" frameborder="0"></iframe>
*/
	$cad_remplazo= array(
					'<h1>$1</h1>', 
					'&',
					'<div id="$1">$2</div>', 
					'<div style="$1">$2</div>',
					'<center>$1</center>',
					'<table id="bbcode_tabla">$1</table>',
					'<th>$1</th>',
					'<td>$1</td>',
					'<tr>',
					'<ul>$1</ul>',
					'<li>$1</li>',
   				'<strong>$1</strong>',
   				'<em>$1</em>',
   				'<u>$1</u>',
   				'<strike>$1</strike>',
   				'<a href="$1">$2</a>',
   				'<a href="$1">$1</a>',
   				'<div style="text-align:$1;">$2</div>',
   				'<img src="$1" border="0" />',
   				'<span style="font-family:$1;">$2</span>',
   				'<span style="font-size:$1;">$2</span>',
   				'<span style="color: $1;">$2</span>',
   				'Codigo PHP<br><div id="etiqueta_code">'. highlight_string('$1', 'true'). '</div>',
   				'Codigo:<br><div id="etiqueta_code">$1</div>',
   				'Cita:<br><div id="etiqueta_code">$1</div>',
   				'<span style="display:block;"><object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/$1"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/$1" type="application/x-shockwave-flash" wmode="transparent" width="425" height="344"></embed></object></span>',
					'<span style="display:block;"><embed src="$1" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" wmode="transparent"></embed></span>',
					'<span style="display:block;"><embed src="$3" $1 $2 type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" wmode="transparent"></embed></span>',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', 
					'<object type="application/x-shockwave-flash" data="admin/addons/dewplayer-mini.swf" width="160" height="20">
					<param name="wmode" value="transparent" />
					<param name="movie" value="admin/addons/dewplayer-mini.swf" />
					<param name="flashvars" value="mp3=uploads/noticias/'. mp3_file('$1'). '" />
					</object>', 
					'<video src="$3" $1 $2 controls onerror="stream_video_error(event)" type="video/ogg">Su Navegador no soporta reproduccion de video HTML5</video>', 
					'<video src="$1" controls onerror="stream_video_error(event)" type="video/ogg">Su Navegador no soporta reproduccion de video HTML5</video>',
					'<embed type="application/x-shockwave-flash" src="admin/addons/player.swf" style="" id="mpl" name="mpl" quality="high" allowfullscreen="true" allowscriptaccess="always" 
					flashvars="file=$1">',
					'<embed type="application/x-shockwave-flash" src="admin/addons/player.swf" style="" id="mpl" name="mpl" quality="high" allowfullscreen="true" allowscriptaccess="always" 
					flashvars="file=$3" height="$2" width="$1">', 
					'<iframe src="http://player.vimeo.com/video/$1" width="400" height="225" frameborder="0"></iframe>', 
					'<object id="twitcamPlayer" width="320" height="265" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"><param name="movie" value="http://static.livestream.com/grid/LSPlayer.swf?hash=$1"/><param name="allowScriptAccess" value="always"/><param name="allowFullScreen" value="true"/><param name="wmode" value="window"/> <embed name="twitcamPlayer" src="http://static.livestream.com/grid/LSPlayer.swf?hash=$1" allowFullScreen="true" allowScriptAccess="always" type="application/x-shockwave-flash" bgcolor="#ffffff" width="320" height="265" wmode="window" ></embed></object>'
                );
                #http://www.podtrac.com/pts/redirect.mp3/

	$mensaje= htmlentities($mensaje, ENT_QUOTES);
	$mensaje= preg_replace( $cad_buscar, $cad_remplazo, $mensaje );
	$mensaje= html_entity_decode($mensaje, ENT_QUOTES);

	return $mensaje;
	}

function r_msg2msgtags( $mensaje )
	{
	/*
	strchr( lugar, palabraclave )  busca la "palabraclave" en "lugar"
	str_replace( palabraclave, sustituto, lugar )  busca en "lugar" la "palabraclave" y la sustituye por "sustituto"
	*/
   $cad_remplazo= array(
   				'[amp]',
   				'[capa=$1]$2[/capa]',
   				'[span=$1]$2[/span]',
   				'[center]$1[/center]',
   				'[table]$1[/table]',
   				'[th]$1[/th]',
   				'[td]$1[/td]',
   				'[tr]',
   				'[list]$1[/list]',
   				'[li]$1[/li]',
   				'[b]$1[/b]',
   				'[i]$1[/i]',
   				'[u]$1[/u]',
   				'[s]$1[/s]',
   				'[url=$1]$2[/url]',                         
   				'[url]$1[/url]',                             
   				'[align=(left|center|right|justify)]$1[/align]',
   				'[img]$1[/img]', 
   				'[font=$1]$2[/font]', 
   				'[size=$1]$2[/size]',                    
   				'[color=$1]$2[/colo]',
   				'[code=php]$1[/code]', 
   				'[code]$1[/code]',
   				'[quote]$1[/quote]',
   				'[youtube]$1[/youtube]',
   				'[swf]$1[/swf]',
   				'[swf $1 $2]$3[/swf]',
   				'[tab\]', 
   				'[play_mp3]$1[/play_mp3]', 
   				'[video $1 $2]$3[/video]',
   				'[video]$1[/video]', 
   				'[video_flash]$1[/video_flash]', 
   				'[video_flash $1 $2]$3[/video_flash]'
   				);

	$cad_buscar= array(
					'/&/is',
					'/\<div id\=\"(.*?)\"\>(.*?)\<\/div\>/is', 
					'/\<div style\=\"(.*?)\"\>(.*?)\<\/div\>/is',
					'/\<center\>(.*?)\<\/center\>/is',
					'/\<table id\=\"bbcode_tabla\"\>(.*?)\<\/table\>/is',
					'/\<th\>(.*?)\<\/th\>/is',
					'/\<td\>(.*?)\<\/td\>/is',
					'/\<tr\>/is',
					'/\<ul\>(.*?)\<\/ul\>/is',
					'/\<li\>(.*?)\<\/li\>/is',
   				'/\<strong\>(.*?)\<\/strong\>/is',
   				'/\<em\>(.*?)\<\/em\>/is',
   				'/\<u\>(.*?)\<\/u\>/is',
   				'/\<strike\>(.*?)\<\/strike\>/is',
   				'/\<a href\=\"(.*?)\"\>(.*?)\<\/a\>/is',
   				'/\<a href\=\"(.*?)\"\>(.*?)\<\/a\>/is',
   				'/\<div style\=\"text\-align\:(.*?)\;\"\>(.*?)\<\/div\>/is',
   				'/\<img src\=\"(.*?)\" border\=\"0\" \/\>/is',
   				'/\<span style\=\"font\-family\:(.*?)\;\"\>(.*?)\<\/span\>/is',
   				'/\<span style\=\"font\-size\:(.*?)\;\"\>(.*?)\<\/span\>/is',
   				'/\<span style\=\"color\: (.*?)\;\"\>(.*?)\<\/span\>/is',
   				'/Codigo PHP\<br\>\<div id\=\"etiqueta_code\"\>'. highlight_string('(.*?)', 'true'). '\<\/div\>/is',
   				'/Codigo\:\<br\>\<div id\=\"etiqueta_code\"\>(.*?)\<\/div\>/is',
   				'/Cita\:\<br\>\<div id\=\"etiqueta_code\"\>(.*?)\<\/div\>/is',
   				'/\<span style\=\"display\:block\;\"\>\<object width\=\"425\" height\=\"344\"\>\<param name\=\"movie\" value\=\"http\:\/\/www\.youtube\.com\/v\/(.*?)\"\>\<\/param\>\<param name\=\"wmode\" value\=\"transparent\"\>\<\/param\>\<embed src\=\"http\:\/\/www\.youtube\.com\/v\/(.*?)\" type\=\"application\/x\-shockwave\-flash\" wmode\=\"transparent\" width\=\"425\" height\=\"344\"\>\<\/embed\>\<\/object\>\<\/span\>/is',
					'/\<span style\=\"display\:block\;\"\>\<embed src\=\"(.*?)\" type\=\"application\/x\-shockwave\-flash\" pluginspage\=\"http\:\/\/www\.macromedia\.com\/shockwave\/download\/index\.cgi\?P1_Prod_Version\=ShockwaveFlash\" wmode\=\"transparent\"\>\<\/embed\>\<\/span\>/is',
					'/\<span style\=\"display\:block\;\"\>\<embed src\=\"(.*?)\" (.*?) (.*?) type\=\"application\/x\-shockwave\-flash\" pluginspage\=\"http\:\/\/www\.macromedia\.com\/shockwave\/download\/index\.cgi\?P1_Prod_Version\=ShockwaveFlash\" wmode\=\"transparent\"\>\<\/embed\>\<\/span\>/is',
					'/&nbsp\;&nbsp\;&nbsp\;&nbsp\;&nbsp\;&nbsp\;/is', 
					'/\<object type\=\"application\/x\-shockwave\-flash\" data\=\"admin\/addons\/dewplayer\-mini\.swf\" width\=\"160\" height\=\"20\"\>
					\<param name\=\"wmode\" value\=\"transparent\" \/\>
					\<param name\=\"movie\" value\=\"admin\/addons\/dewplayer\-mini\.swf\" \/\>
					\<param name\=\"flashvars\" value\=\"mp3\=uploads\/noticias\/'. mp3_file('(.*?)'). '\" \/\>
					\<\/object\>/is', 
					'/\<video src\=\"(.*?)\" (.*?) (.*?) controls onerror\=\"stream_video_error\(event\)\" type\=\"video\/ogg\"\>Su Navegador no soporta reproduccion de video HTML5\<\/video\>/is', 
					'/\<video src\=\"(.*?)\" controls onerror\=\"stream_video_error\(event\)\" type\=\"video\/ogg\"\>Su Navegador no soporta reproduccion de video HTML5\<\/video\>/is',
					'/\<embed type\=\"application\/x\-shockwave\-flash\" src\=\"admin\/addons\/player\.swf\" style\=\"\" id\=\"mpl\" name\=\"mpl\" quality\=\"high\" allowfullscreen\=\"true\" allowscriptaccess\=\"always\" 
					flashvars\=\"file\=(.*?)\"\>/is',
					'/\<embed type\=\"application\/x\-shockwave\-flash\" src\=\"admin\/addons\/player\.swf\" style\=\"\" id\=\"mpl\" name\=\"mpl\" quality\=\"high\" allowfullscreen\=\"true\" allowscriptaccess\=\"always\" 
					flashvars\=\"file\=(.*?)\" height\=\"(.*?)\" width\=\"$1\"\>/is'
                );
                
	$mensaje= preg_replace( $cad_buscar, $cad_remplazo, $mensaje );

	return $mensaje;
	}

//elimina algunos BBCodes
function eliminar_bbcode( $mensaje )
	{
   $cad_buscar= array(
   				'/\[center\](.*?)\[\/center\]/is',
   				'/\[b\](.*?)\[\/b\]/is',
   				'/\[i\](.*?)\[\/i\]/is',
   				'/\[u\](.*?)\[\/u\]/is',
   				'/\[s\](.*?)\[\/s\]/is',
   				'/\[align\=(left|center|right|justify)\](.*?)\[\/align\=(left|center|right|justify)\]/is',
   				'/\[img\](.*?)\[\/img\]/is',                            
   				'/\[youtube](.*?)\[\/youtube\]/is',
   				'/\[swf\](.*?)\[\/swf\]/is',
   				'/\[swf (.*?) (.*?)\](.*?)\[\/swf\]/is',
   				'/\[url\=(.*?)\](.*?)\[\/url\]/is',                         
   				'/\[url\](.*?)\[\/url\]/is'
   				);

	$cad_remplazo= array(
					'<center>$1</center>',
   				'<strong>$1</strong>',
   				'<em>$1</em>',
   				'<u>$1</u>',
   				'<strike>$1</strike>',
   				'$2',
   				'$1',
   				'$1',
					'$1',
					'$3',
					'$1',
					'$1',
                );

	$mensaje= htmlentities($mensaje, ENT_QUOTES);
	$mensaje= preg_replace( $cad_buscar, $cad_remplazo, $mensaje );
	$mensaje= html_entity_decode($mensaje, ENT_QUOTES);

	return $mensaje;
	}

//busca archivo MP3 a reproducir
function mp3_file( $id )
	{
	return $id. '.mp3';
	}


//Convierte de CODIGO a Caritas
function msg2caritas( $mensaje )
	{
	/*
	strchr( lugar, palabraclave )  busca la "palabraclave" en "lugar"
	str_replace( palabraclave, sustituto, lugar )  busca en "lugar" la "palabraclave" y la sustituye por "sustituto"
	*/
	if( strchr( $mensaje, ":D" ) )
		$mensaje= str_replace( ":D", "<img src=\"". CARITAS_URL. "/001.gif\" border=\"0\" alt=\":D\" title=\":D\">", $mensaje );

	if( strchr( $mensaje, ":(" )  )
		$mensaje= str_replace( ":(", "<img src=\"". CARITAS_URL. "/002.gif\" border=\"0\" alt=\":(\" title=\":(\">", $mensaje );

	if( strchr( $mensaje, ";)" ) )
		$mensaje= str_replace( ";)", "<img src=\"". CARITAS_URL. "/009.gif\" border=\"0\" alt=\";)\" title=\";)\">", $mensaje );

	if( strchr( $mensaje, "^_^" ) )
		$mensaje= str_replace( "^_^", "<img src=\"". CARITAS_URL. "/003.gif\" border=\"0\" alt=\"^_^\" title=\"^_^\">", $mensaje );

	if( strchr( $mensaje, "o_O" ) )
		$mensaje= str_replace( "o_O", "<img src=\"". CARITAS_URL. "/004.gif\" border=\"0\" alt=\"o_O\" title=\"o_O\">", $mensaje );

	if( strchr( $mensaje, ":S" ) )
		$mensaje= str_replace( ":S", "<img src=\"". CARITAS_URL. "/005.gif\" border=\"0\" alt=\":S\" title=\":S\">", $mensaje );

	if( strchr( $mensaje, "-.-" ) )
		$mensaje= str_replace( "-.-", "<img src=\"". CARITAS_URL. "/008.gif\" border=\"0\" alt=\"-.-\" title=\"-.-\">", $mensaje );	

	if( strchr( $mensaje, "(H)" ) )
		$mensaje= str_replace( "(H)", "<img src=\"". CARITAS_URL. "/006.gif\" border=\"0\" alt=\"(H)\" title=\"(H)\">", $mensaje );

	if( strchr( $mensaje, ":O" ) )
		$mensaje= str_replace( ":O", "<img src=\"". CARITAS_URL. "/011.gif\" border=\"0\" alt=\":O\" title=\":O\">", $mensaje );
		 
	if( strchr( $mensaje, ":|" ) )
		$mensaje= str_replace( ":|", "<img src=\"". CARITAS_URL. "/010.gif\" border=\"0\" alt=\":|\" title=\":|\">", $mensaje );
	/*if( strchr( $mensaje, "" ) &&  strchr( $mensaje, "" ) )
		{
		$mensaje= str_replace();
		$mensaje= str_replace();
		}*/
	return $mensaje;
	}
	
function msg2caritas_extra( $mensaje )
	{
	/*
	strchr( lugar, palabraclave )  busca la "palabraclave" en "lugar"
	str_replace( palabraclave, sustituto, lugar )  busca en "lugar" la "palabraclave" y la sustituye por "sustituto"
	*/
	if( strchr( $mensaje, ":ass_suck:" ) )
		$mensaje= str_replace( ":ass_suck:", "<img src=\"". CARITAS_URL. "/012.gif\" border=\"0\" alt=\":ass_suck:\" title=\":ass_suck:\">", $mensaje );
	if( strchr( $mensaje, ":laser:" ) )
		$mensaje= str_replace( ":laser:", "<img src=\"". CARITAS_URL. "/013.gif\" border=\"0\" alt=\":laser:\" title=\":laser:\">", $mensaje );
	if( strchr( $mensaje, ":vomito:" ) )
		$mensaje= str_replace( ":vomito:", "<img src=\"". CARITAS_URL. "/014.gif\" border=\"0\" alt=\":vomito:\" title=\":vomito:\">", $mensaje );
	if( strchr( $mensaje, ":nerdo:" ) )
		$mensaje= str_replace( ":nerdo:", "<img src=\"". CARITAS_URL. "/015.gif\" border=\"0\" alt=\":nerdo:\" title=\":nerdo:\">", $mensaje );
	if( strchr( $mensaje, ":juez:" ) )
		$mensaje= str_replace( ":juez:", "<img src=\"". CARITAS_URL. "/016.gif\" border=\"0\" alt=\":juez:\" title=\":juez:\">", $mensaje );
	if( strchr( $mensaje, ":vomitando:" ) )
		$mensaje= str_replace( ":vomitando:", "<img src=\"". CARITAS_URL. "/017.gif\" border=\"0\" alt=\":vomitando:\" title=\":vomitando:\">", $mensaje );
	if( strchr( $mensaje, ":metralleta:" ) )
		$mensaje= str_replace( ":metralleta:", "<img src=\"". CARITAS_URL. "/018.gif\" border=\"0\" alt=\":metralleta:\" title=\":metralleta:\">", $mensaje );
	if( strchr( $mensaje, ":asesino:" ) )
		$mensaje= str_replace( ":asesino:", "<img src=\"". CARITAS_URL. "/019.gif\" border=\"0\" alt=\":asesino:\" title=\":asesino:\">", $mensaje );
	if( strchr( $mensaje, ":ak_rojo:" ) )
		$mensaje= str_replace( ":ak_rojo:", "<img src=\"". CARITAS_URL. "/020.gif\" border=\"0\" alt=\":ak_rojo:\" title=\":ak_rojo:\">", $mensaje );
	if( strchr( $mensaje, ":ak_asalto:" ) )
		$mensaje= str_replace( ":ak_asalto:", "<img src=\"". CARITAS_URL. "/021.gif\" border=\"0\" alt=\":ak_asalto:\" title=\":ak_asalto:\">", $mensaje );
	if( strchr( $mensaje, ":sniper:" ) )
		$mensaje= str_replace( ":sniper:", "<img src=\"". CARITAS_URL. "/022.gif\" border=\"0\" alt=\":sniper:\" title=\":sniper:\">", $mensaje );
	if( strchr( $mensaje, ":rambo:" ) )
		$mensaje= str_replace( ":rambo:", "<img src=\"". CARITAS_URL. "/023.gif\" border=\"0\" alt=\":rambo:\" title=\":rambo:\">", $mensaje );
	if( strchr( $mensaje, ":metralleta2:" ) )
		$mensaje= str_replace( ":metralleta2:", "<img src=\"". CARITAS_URL. "/024.gif\" border=\"0\" alt=\":metralleta2:\" title=\":metralleta2:\">", $mensaje );
	if( strchr( $mensaje, ":tanque:" ) )
		$mensaje= str_replace( ":tanque:", "<img src=\"". CARITAS_URL. "/025.gif\" border=\"0\" alt=\":tanque:\" title=\":tanque:\">", $mensaje );
	if( strchr( $mensaje, ":sangrado:" ) )
		$mensaje= str_replace( ":sangrado:", "<img src=\"". CARITAS_URL. "/026.gif\" border=\"0\" alt=\":sangrado:\" title=\":sangrado:\">", $mensaje );
	if( strchr( $mensaje, ":num1:" ) )
		$mensaje= str_replace( ":num1:", "<img src=\"". CARITAS_URL. "/027.gif\" border=\"0\" alt=\":num1:\" title=\":num1:\">", $mensaje );
	if( strchr( $mensaje, ":ok:" ) )
		$mensaje= str_replace( ":ok:", "<img src=\"". CARITAS_URL. "/028.gif\" border=\"0\" alt=\":ok:\" title=\":ok:\">", $mensaje );
	if( strchr( $mensaje, ":platanin:" ) )
		$mensaje= str_replace( ":platanin:", "<img src=\"". CARITAS_URL. "/029.gif\" border=\"0\" alt=\":platanin:\" title=\":platanin:\">", $mensaje );
	if( strchr( $mensaje, ":fuck1:" ) )
		$mensaje= str_replace( ":fuck1:", "<img src=\"". CARITAS_URL. "/030.gif\" border=\"0\" alt=\":fuck1:\" title=\":fuck1:\">", $mensaje );
	if( strchr( $mensaje, ":fuck2:" ) )
		$mensaje= str_replace( ":fuck2:", "<img src=\"". CARITAS_URL. "/031.gif\" border=\"0\" alt=\":fuck2:\" title=\":fuck2:\">", $mensaje );
	if( strchr( $mensaje, ":varazo:" ) )
		$mensaje= str_replace( ":varazo:", "<img src=\"". CARITAS_URL. "/032.gif\" border=\"0\" alt=\":varazo:\" title=\":varazo:\">", $mensaje );
	if( strchr( $mensaje, ":golpe:" ) )
		$mensaje= str_replace( ":golpe:", "<img src=\"". CARITAS_URL. "/033.gif\" border=\"0\" alt=\":golpe:\" title=\":golpe:\">", $mensaje );
	if( strchr( $mensaje, ":ban:" ) )
		$mensaje= str_replace( ":ban:", "<img src=\"". CARITAS_URL. "/034.gif\" border=\"0\" alt=\":ban:\" title=\":ban:\">", $mensaje );
	if( strchr( $mensaje, ":dedo:" ) )
		$mensaje= str_replace( ":dedo:", "<img src=\"". CARITAS_URL. "/035.gif\" border=\"0\" alt=\":dedo:\" title=\":dedo:\">", $mensaje );
	if( strchr( $mensaje, ":pared:" ) )
		$mensaje= str_replace( ":pared:", "<img src=\"". CARITAS_URL. "/036.gif\" border=\"0\" alt=\":pared:\" title=\":pared:\">", $mensaje );
	if( strchr( $mensaje, ":fuck3:" ) )
		$mensaje= str_replace( ":fuck3:", "<img src=\"". CARITAS_URL. "/037.gif\" border=\"0\" alt=\":fuck3:\" title=\":fuck3:\">", $mensaje );
	if( strchr( $mensaje, ":besamela:" ) )
		$mensaje= str_replace( ":besamela:", "<img src=\"". CARITAS_URL. "/038.gif\" border=\"0\" alt=\":besamela:\" title=\":besamela:\">", $mensaje );
	if( strchr( $mensaje, ":borracho:" ) )
		$mensaje= str_replace( ":borracho:", "<img src=\"". CARITAS_URL. "/039.gif\" border=\"0\" alt=\":borracho:\" title=\":borracho:\">", $mensaje );
	if( strchr( $mensaje, ":borracho2:" ) )
		$mensaje= str_replace( ":borracho2:", "<img src=\"". CARITAS_URL. "/040.gif\" border=\"0\" alt=\":borracho2:\" title=\":borracho2:\">", $mensaje );
	if( strchr( $mensaje, ":agente_007:" ) )
		$mensaje= str_replace( ":agente_007:", "<img src=\"". CARITAS_URL. "/041.gif\" border=\"0\" alt=\":agente_007:\" title=\":agente_007:\">", $mensaje );
	if( strchr( $mensaje, ":boxeador:" ) )
		$mensaje= str_replace( ":boxeador:", "<img src=\"". CARITAS_URL. "/042.gif\" border=\"0\" alt=\":boxeador:\" title=\":boxeador:\">", $mensaje );
	if( strchr( $mensaje, ":chacos:" ) )
		$mensaje= str_replace( ":chacos:", "<img src=\"". CARITAS_URL. "/043.gif\" border=\"0\" alt=\":chacos:\" title=\":chacos:\">", $mensaje );
	if( strchr( $mensaje, ":muletas:" ) )
		$mensaje= str_replace( ":muletas:", "<img src=\"". CARITAS_URL. "/044.gif\" border=\"0\" alt=\":muletas:\" title=\":muletas:\">", $mensaje );
	if( strchr( $mensaje, ":granada_estupida:" ) )
		$mensaje= str_replace( ":granada_estupida:", "<img src=\"". CARITAS_URL. "/045.gif\" border=\"0\" alt=\":granada_estupida:\" title=\":granada_estupida:\">", $mensaje );
	//if( strchr( $mensaje, ":camilla:" ) )
		//$mensaje= str_replace( ":camilla:", "<img src=\"". CARITAS_URL. "/046.gif\" border=\"0\" alt=\":camilla:\" title=\":camilla:\">", $mensaje );
		
	return $mensaje;
	}
	
function lista_caritas()
	{
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':D', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/001.gif\" border=\"0\" alt=\":D\" title=\":D\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':(', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/002.gif\" border=\"0\" alt=\":(\" title=\":(\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ';)', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/009.gif\" border=\"0\" alt=\";)\" title=\";)\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( '^_^', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/003.gif\" border=\"0\" alt=\"^_^\" title=\"^_^\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( 'o_O', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/004.gif\" border=\"0\" alt=\"o_O\" title=\"o_O\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':S', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/005.gif\" border=\"0\" alt=\":S\" title=\":S\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( '-.-', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/008.gif\" border=\"0\" alt=\"-.-\" title=\"-.-\"></a> ";	
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( '(H)', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/006.gif\" border=\"0\" alt=\"(H)\" title=\"(H)\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':O', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/011.gif\" border=\"0\" alt=\":O\" title=\":O\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':|', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/010.gif\" border=\"0\" alt=\":|\" title=\":|\"></a> ";
	//echo "[><a onclick=\"caritas_extra('caritas_extra')\">Ver Caritas Extra</a><] ";
	//echo "[><a onclick=\"caritas_extra('caritas_tuki')\">Ver Caritas Tuki</a><] ";
	}
	
function lista_cartas_extra()
	{
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':ass_suck:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/012.gif\" border=\"0\" alt=\":ass_suck:\" title=\":ass_suck:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':laser:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/013.gif\" border=\"0\" alt=\":laser:\" title=\":laser:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':vomito:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/014.gif\" border=\"0\" alt=\":vomito:\" title=\":vomito:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':nerdo:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/015.gif\" border=\"0\" alt=\":nerdo:\" title=\":nerdo:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':juez:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/016.gif\" border=\"0\" alt=\":juez:\" title=\":juez:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':vomitando:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/017.gif\" border=\"0\" alt=\":vomitando:\" title=\":vomitando:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':metralleta:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/018.gif\" border=\"0\" alt=\":metralleta:\" title=\":metralleta:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':asesino:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/019.gif\" border=\"0\" alt=\":asesino:\" title=\":asesino:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':ak_rojo:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/020.gif\" border=\"0\" alt=\":ak_rojo:\" title=\":ak_rojo:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':ak_asalto:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/021.gif\" border=\"0\" alt=\":ak_asalto:\" title=\":ak_asalto:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':sniper:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/022.gif\" border=\"0\" alt=\":sniper:\" title=\":sniper:\"> </a>";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':rambo:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/023.gif\" border=\"0\" alt=\":rambo:\" title=\":rambo:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':metralleta2:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/024.gif\" border=\"0\" alt=\":metralleta2:\" title=\":metralleta2:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':tanque:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/025.gif\" border=\"0\" alt=\":tanque:\" title=\":tanque:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':sangrado:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/026.gif\" border=\"0\" alt=\":sangrado:\" title=\":sangrado:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':num1:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/027.gif\" border=\"0\" alt=\":num1:\" title=\":num1:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':ok:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/028.gif\" border=\"0\" alt=\":ok:\" title=\":ok:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':platanin:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/029.gif\" border=\"0\" alt=\":platanin:\" title=\":platanin:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':fuck1:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/030.gif\" border=\"0\" alt=\":fuck1:\" title=\":fuck1:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':fuck2:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/031.gif\" border=\"0\" alt=\":fuck2:\" title=\":fuck2:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':varazo:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/032.gif\" border=\"0\" alt=\":varazo:\" title=\":varazo:\"></a> "; 
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':golpe:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/033.gif\" border=\"0\" alt=\":golpe:\" title=\":golpe:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':ban:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/034.gif\" border=\"0\" alt=\":ban:\" title=\":ban:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':dedo:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/035.gif\" border=\"0\" alt=\":dedo:\" title=\":dedo:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':pared:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/036.gif\" border=\"0\" alt=\":pared:\" title=\":pared:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':fuck3:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/037.gif\" border=\"0\" alt=\":fuck3:\" title=\":fuck3:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':besamela:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/038.gif\" border=\"0\" alt=\":besamela:\" title=\":besamela:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':borracho:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/039.gif\" border=\"0\" alt=\":borracho:\" title=\":borracho:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':borracho2:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/040.gif\" border=\"0\" alt=\":borracho2:\" title=\":borracho2:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':agente_007:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/041.gif\" border=\"0\" alt=\":agente_007:\" title=\":agente_007:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':boxeador:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/042.gif\" border=\"0\" alt=\":boxeador:\" title=\":boxeador:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':chacos:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/043.gif\" border=\"0\" alt=\":chacos:\" title=\":chacos:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':muletas:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/044.gif\" border=\"0\" alt=\":muletas:\" title=\":muletas:\"></a> ";
	echo "<a href=\"javascript:void(0);\" onclick=\"reemplazar_carita( ':granada_estupida:', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"". CARITAS_URL. "/045.gif\" border=\"0\" alt=\":granada_estupida:\" title=\":granada_estupida:\"></a> ";
	}

function lista_controles()
	{
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[img]', '[/img]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"admin/imagenes/imagen.gif\" border=\"0\" title=\"Imagen\" alt=\"Imagen\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[b]', '[/b]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"admin/imagenes/bold.gif\" border=\"0\" title=\"Negrita\" alt=\"Negrita\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[i]', '[/i]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"admin/imagenes/italic.gif\" border=\"0\" title=\"Italica\" alt=\"Italica\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[url]', '[/url]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"admin/imagenes/url.gif\" border=\"0\" title=\"Link\" alt=\"Link\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[youtube]', '[/youtube]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"admin/imagenes/youtube.gif\" border=\"0\" title=\"Youtube Video\" alt=\"Youtube Video\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[swf]', '[/swf]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"admin/imagenes/swf.png\" border=\"0\" title=\"Archivo SWF\" alt=\"Archivo SWF\"></a>";
	echo "<a href=\"javascript:void(0)\" onclick=\"surroundText('[swf width= height=]', '[/swf]', document.forms.formulario_comentarios.comentario_mensaje); return false;\"><img src=\"admin/imagenes/swf-ajustable.png\" border=\"0\" title=\"SWF Ajustable\" alt=\"SWF Ajustable\"></a>";
	}
	
//obtener tipo de archivo apartir del nombre y extencion
function obtener_extencion_stream_archivo( $archivo )
	{
	$arr= array( "jpg"=>"image/jpg", "jpeg"=>"image/jpg", "png"=>"image/png", "gif"=>"image/gif", "swf"=>"application/x-shockwave-flash", 
	"flv"=>"video/flv", "doc"=>"application/doc", "odt"=>"application/odt", "xls"=>"application/xls", "pdf"=>"application/pdf", "txt"=>"text/plain", 
	"mp3"=>"audio/mp3", "html"=>"application/html", "tar.gz"=>"application/gzip", "rar"=>"application/rar", "zip"=>"application/zip"); # formatos validos
	
	foreach($arr as $key=>$val )
		{
		#if( strstr($tipo, $arr[$i]) )
		if( !strtr($archivo, $key) )
			return $val; # retornamos mime 
		}
	return 'application/octet-stream'; # desconocido mime 
	}


//comprueba que la extencion del archivo a subir coincida con los establecidos
function comprobar_extencion_imagen( $archivo )
	{
	/*
	image/jpeg	Imagen JPEG, JPG
	image/png	Imagen PNG
	image/gif	Imagen GIF
	*/
	while( list($g, $h)=each($archivo) ) //recorremos todos los FILES
		{
		$i=0; //inicializamos foco "apagado"=0
		while( list($w, $z)=each($h) )
			{
			if( !strcmp($w, "name") && strcmp( $z, "" ) ) //si existe nombre de archivo
				$i=1; //encendemos foco
			if( !strcmp($w, "type") && strcmp( $z, "" ) ) //si existe tipo
				{
				if( !strpos( $z, "jpg" ) && !strpos( $z, "jpeg" ) && !strpos( $z, "png" ) && !strpos( $z, "gif" ) )
					{
					echo "Tipo de extencion/archivo inaceptable.<br>";
					return 0;
					}
				$i=0; //apagamos foco
				}
			}
		}

	unset($i);
	return 1;
	}

//comprueba que la extencion del archivo a subir coincida con los establecidos
function comprobar_extencion_archivo( $archivo )
	{
	/*
	application/zip			Archivo ZIP
	application/octet-stream	Archivos TAR.BZ2, RAR, ODT, DOC
	application/gzip		Archivo TAR.GZ
	application/pdf			Archivo PDF
	text/plain			Archivo TXT
	text/html			Archivo HTML
	application/x-shockwave-flash	Archivo SWF
	*/

	while( list($g, $h)=each($archivo) ) //recorremos todos los FILES
		{
		$i=0; //inicializamos foco "apagado"=0
		while( list($w, $z)=each($h) )
			{
			if( !strcmp($w, "name") && strcmp( $z, "" ) ) //si existe nombre
				$i=1; //encendemos foco
			if( !strcmp($w, "type") && strcmp( $z, "" ) ) //sie xiste tipo
				{
				if( !strpos( $z, "mp3" )  && !strpos( $z, "zip" ) && !strpos( $z, "gzip" ) && !strpos( $z, "pdf" ) && !strpos( $z, "plain" ) && !strpos( $z, "html" ) && !strpos( $z, "flash" ) && !strpos( $z, "octet-stream" ) && !strpos( $z, "opendocument" ) )
					{
					echo "Tipo de extencion/archivo inaceptable.<br>";
					return 0;
					}
				$i=0; //apagamos foco
				}
			}
		}
	unset($i);
	return 1;
	}

//comprueba que la extencion del archivo a subir coincida con los establecidos
function comprobar_extencion_archivo_scriptin( $archivo )
	{
	/*
	application/zip			Archivo ZIP
	application/octet-stream	Archivos TAR.BZ2, RAR, ODT, DOC
	application/gzip		Archivo TAR.GZ
	application/pdf			Archivo PDF
	text/plain			Archivo TXT
	text/html			Archivo HTML
	application/x-shockwave-flash	Archivo SWF
	*/

	while( list($g, $h)=each($archivo) ) //recorremos todos los FILES
		{
		$i=0; //inicializamos foco "apagado"=0
		while( list($w, $z)=each($h) )
			{
			if( !strcmp($w, "name") && strcmp( $z, "" ) ) //si existe nombre
				$i=1; //encendemos foco
			if( !strcmp($w, "type") && strcmp( $z, "" ) ) //si existe tipo
				{
				if( !strpos( $z, "php" ) && strcmp($z, "application/octet-stream") )
					{
					echo "Tipo de extencion/archivo inaceptable.<br>";
					return 0;
					}
				$i=0; //apagamos foco
				}
			}
		}
	unset($i);
	return 1;
	}

function lista_grupos( $modo )
	{
	$cons= consultar_enorden( "PRIVILEGIOS", "NOMBRE" );
	switch( $modo )
		{
		case 'select':
			if( mysqli_num_rows($cons)==0 )
				echo "<option>No existen grupos aun...</option>";
			else
				{
				while( $buf= mysqli_fetch_array($cons) )
					{
					echo "<option value=\"". $buf["NOMBRE"]. "\">";
					echo $buf["NOMBRE"];
					echo "</option>";
					}
					}
			unset($buf);
			break;
		}
	unset($cons);
	}

function lista_paises( $modo )
	{
	$cons= r_consultar_enorden( "MUNDO", "PAIS" );
	switch( $modo )
		{
		case 'select':
			while( $buf= mysqli_fetch_array($cons) )
				{
				echo "<option value=\"". $buf["PAIS"]. "\">";
				echo $buf["PAIS"];
				echo "</option>";
				}
			unset($buf);
			break;
		}
	unset($cons);
	}

function num2month($num)
	{
	$mes= array( "1"=>"Enero", "2"=>"Febrero", "3"=>"Marzo", "4"=>"Abril", "5"=>"Mayo", 
					"6"=>"Junio", "7"=>"Julio", "8"=>"Agosto", "9"=>"Septiembre", "10"=>"Octubre", "11"=>"Noviembre", "12"=>"Diciembre" );
					
	foreach( $mes as $key=>$val )
		{
		if( !strcmp($key, $num) ) # si es el mismo numero
			return $val;
		}
	return 0;
	}
	
function mes_esp( $dato )
	{
	if( !strcmp( $dato, "01" ) )
		return "Enero";
	else if( !strcmp( $dato, "02" ) )
		return "Febrero";
	else if( !strcmp( $dato, "03" ) )
		return "Marzo";
	else if( !strcmp( $dato, "04" ) )
		return "Abril";
	else if( !strcmp( $dato, "05" ) )
		return "Mayo";
	else if( !strcmp( $dato, "06" ) )
		return "Junio";
	else if( !strcmp( $dato, "07" ) )
		return "Julio";
	else if( !strcmp( $dato, "08" ) )
		return "Agosto";
	else if( !strcmp( $dato, "09" ) )
		return "Septiembre";
	else if( !strcmp( $dato, "10" ) )
		return "Octubre";
	else if( !strcmp( $dato, "11" ) )
		return "Noviembre";
	else
		return "Diciembre";
	}
	
function proteger_cadena( $cadena )
	{
	return htmlentities($cadena, ENT_QUOTES, "UTF-8");
	}

function desproteger_cadena_src( $cadena )
	{
	$out=$cadena;
	$out= html_entity_decode( $out, ENT_QUOTES, "UTF-8" );

	if( strchr( $out, "<" ) )
		$out= str_replace( "<", htmlentities("<", ENT_QUOTES, "UTF-8"), $out );
	if( strchr( $out, ">" ) )
		$out= str_replace( ">", htmlentities(">", ENT_QUOTES, "UTF-8"), $out );

	if( strchr( $out, "\n" ) )
		$out= str_replace( "\n", "<br>", $out );
	if( strchr( $out, "\t" ) )
		$out= str_replace( "\t", "&nbsp;&nbsp;&nbsp;", $out );
		
	return $out;
	}
	
function cleanhtml( $cad )
	{
	$out= $cad;
	if( strstr($out,  "<br>") )
		$out= str_replace( "<br>", "\n", $out );
	if( strstr($out,  proteger_cadena("<br>") ) )
		$out= str_replace( proteger_cadena("<br>"), "\n", $out );
	return $out;
	}
	
function desproteger_cadena( $cadena )
	{
	$out=$cadena;
	$out= html_entity_decode( $out, ENT_QUOTES, "UTF-8" );

	if( strchr( $out, "<" ) )
		$out= str_replace( "<", htmlentities("<", ENT_QUOTES, "UTF-8"), $out );
	if( strchr( $out, ">" ) )
		$out= str_replace( ">", htmlentities(">", ENT_QUOTES, "UTF-8"), $out );

	if( strchr( $out, "\n" ) )
		$out= str_replace( "\n", "<br>", $out );
	if( strchr( $out, "\t" ) )
		$out= str_replace( "\t", "&nbsp;&nbsp;&nbsp;", $out );

	$out= msg2msgtags($out);
	# $out= msg2caritas($out);
	# $out= msg2caritas_extra($out);
	
	return $out;
	}

function desproteger_cadena_xml( $cadena )
	{
	$out=$cadena;
	$out= html_entity_decode( $out, ENT_QUOTES, "UTF-8" );

	if( strchr( $out, "<" ) )
		$out= str_replace( "<", htmlentities("<", ENT_QUOTES, "UTF-8"), $out );
	if( strchr( $out, ">" ) )
		$out= str_replace( ">", htmlentities(">", ENT_QUOTES, "UTF-8"), $out );

	if( strchr( $out, "\n" ) )
		$out= str_replace( "\n", "<br>", $out );
	if( strchr( $out, "\t" ) )
		$out= str_replace( "\t", "&nbsp;&nbsp;&nbsp;", $out );
	
	//$out= utf8_encode( str_replace("|","/",$out) );
	$out= msg2msgtags($out);
	$out= msg2caritas($out);
	$out= msg2caritas_extra($out);
	
	$out= utf8_encode( $out ); //UTF
	return $out;
	}
	
function eliminar_acentos( $cadena )
	{
	if( strchr( $out, "á" ) )
		$out= str_replace( "á", "a", $out );
	if( strchr( $out, "é" ) )
		$out= str_replace( "é", "e", $out );
	if( strchr( $out, "í" ) )
		$out= str_replace( "í", "i", $out );
	if( strchr( $out, "ó" ) )
		$out= str_replace( "ó", "o", $out );
	if( strchr( $out, "ú" ) )
		$out= str_replace( "ú", "u", $out );
	}

function comprobar_variables( $base_dd, $base_dd2, $valor )
	{
	if( strchr($valor, ",") )
		{
		$x= explode( ",", $valor );
		
		$a= consultar_con( $base_dd, "ID='". $x[0]. "'" );
		$b= consultar_con( $base_dd2, "ID='". $x[1]. "':RELACION='". $x[0]. "'" );
		
		if( mysqli_num_rows($a)==0 || mysqli_num_rows($b)==0 )
			{
			unset($a);
			unset($b);
			unset($x);
			return 0;
			}
			
		$buf= mysqli_fetch_array($b);

		unset($a);
		unset($b);
		unset($x);

		/*##################################
		#########		NOTICIAS		#########
		####################################*/
		if( !strcmp($buf["TIPO"], "noticia") )
			{
			unset($buf);

			if( strcmp($_POST["titulo_noticia"], "" ) && strcmp($_POST["mensaje_noticia"], "" ) )
				return 1;
			}
		else if( !strcmp($buf["TIPO"], "noticia_limpia") )
			{
			unset($buf);

			if( strcmp( $_POST["titulo_noticia"], "" ) && strcmp($_POST["mensaje_noticia"], "") )
				return 1;
			}
		else if( !strcmp($buf["TIPO"], "galeria") )
			{
			unset($buf);

			if( strcmp($_POST["titulo_noticia"], "") && strcmp($_POST["mensaje_noticia"], "") && (strcmp($_FILES["imagen_noticia_01"]["name"], "") || strcmp($_FILES["imagen_noticia_02"]["name"], "") || strcmp($_FILES["imagen_noticia_03"]["name"], "") || strcmp($_FILES["imagen_noticia_04"]["name"], "")) )
				return 1;
			}
		else if( !strcmp($buf["TIPO"], "descriptiva") )
			{
			unset($buf);

			if( strcmp($_POST["titulo_noticia"], "") && strcmp($_POST["mensaje_noticia"], "") && ( strcmp($_FILES["archivo_noticia_01"]["name"], "" ) || $_POST["manual_file"] ==1) )
				return 1;
			}
		else if( !strcmp($buf["TIPO"], "scriptin") )
			{
			unset($buf);

			if( strcmp($_POST["titulo_noticia"], "") && strcmp($_FILES["archivo_noticia_01"]["name"], "" ) )
				return 1;
			}
		}

	/*########################################
	#########	USUARIOS MENSAJERIA	#########
	##########################################*/
	if( !strcmp($valor, "enviar_mensaje") )
		{
		if( strcmp($_POST["mensajeria_usuario"], "") && strcmp($_POST["mensajeria_asunto"], "") && strcmp($_POST["mensajeria_msg"], "" ) )
			return 1;
		}
		
	return 0;
	}

function evitar_repeticion( $tipo, $dato, $bdd )
	{
	//comprobar registros ID
	if( !strcmp($tipo, "registro") )
		{
		$cons= consultar_con( $bdd, "ID='". $dato. "'" );
		
		if( mysqli_num_rows($cons)>0 )
			echo "<td>Error: El Identificador (ID) que intentas insertar ya existe...<br>";
		}
		
	//comprobar imagenes
	else if( !strcmp($tipo, "imagenes") )
		{
		$cons= consultar( $bdd, "*" );
		while( $buf= mysqli_fetch_array($cons) ) //recorriendo registros
			{
			if( strcmp( $buf["IMAGENES_URL"], "") ) //si no esta vacio, accedemos
				{
				$tmp="";
				
				//Obtenemos nombres de imagenes existentes
				if( strchr( $buf["IMAGENES_NOMBRE"], ":" ) ) //si existe concatenador, son varias imagenes
					$tmp= explode( ":", $buf["IMAGENES_NOMBRE"] ); //dividimos
				else //solo es una imagen
					$tmp= $buf["IMAGENES_NOMBRE"]; //copiamos nombre
					
				//Comparacion de nombres para evitar imagenes duplicadas
				if( strchr($buf["IMAGENES_NOMBRE"], ":") ) //si existe concatenador, son varias imagenes
					{
					for( $i=0; $i<count($tmp); $i++ )
						{
						if( !strcmp($buf["IMAGENES_NOMBRE"], $dato) ) //si tienen el mismo nombre
							{
							echo "<td>La imagen <b>". $tmp[$i]. "</b> ya existe, asi que no sera subida al servidor.<br>";
							return 1; //error
							}
						}
					}
				else //solo es una imagen
					{
					if( !strcmp($buf["IMAGENES_NOMBRE"], $dato) ) //si tienen el mismo nombre
						{
						echo "<td>La imagen <b>". $dato. "</b> ya existe, asi que no sera subida al servidor.<br>";
						return 1; //error
						}
					}
				}
			} 
		}

	//comprobar archivos
	else if( !strcmp($tipo, "archivos") )
		{
		$cons= consultar( $bdd, "*" );
		while( $buf= mysqli_fetch_array($cons) ) //recorriendo registros
			{
			if( strcmp( $buf["ARCHIVOS_URL"], "") ) //si no esta vacio, accedemos
				{
				$tmp="";
				
				//Obtenemos nombres de imagenes existentes
				if( strchr( $buf["ARCHIVOS_NOMBRE"], ":" ) ) //si existe concatenador, son varias imagenes
					$tmp= explode( ":", $buf["ARCHIVOS_NOMBRE"] ); //dividimos
				else //solo es una imagen
					$tmp= $buf["ARCHIVOS_NOMBRE"]; //copiamos nombre
					
				//Comparacion de nombres para evitar imagenes duplicadas
				if( strchr($buf["ARCHIVOS_NOMBRE"], ":") ) //si existe concatenador, son varias imagenes
					{
					for( $i=0; $i<count($tmp); $i++ )
						{
						if( !strcmp($buf["ARCHIVOS_NOMBRE"], $dato) ) //si tienen el mismo nombre
							{
							echo "<td>El archivo</td> <b>". $tmp[$i]. "</b> ya existe, asi que no sera subida al servidor.<br>";
							return 1; //error
							}
						}
					}
				else //solo es una imagen
					{
					if( !strcmp($buf["ARCHIVOS_NOMBRE"], $dato) ) //si tienen el mismo nombre
						{
						echo "<td>El archivo <b>". $dato. "</b> ya existe, asi que no sera subida al servidor.<br>";
						return 1; //error
						}
					}
				}
			} 
		}
		
	//comprobar imagenes del buzon
	else if( !strcmp($tipo, "buzon") )
		{
		$cons= consultar( $bdd, "*" );
		while( $buf= mysqli_fetch_array($cons) ) //recorriendo registros
			{
			if( strcmp( $buf["IMAGENES_URL"], "") ) //si no esta vacio, accedemos
				{
				$tmp="";
				
				//Obtenemos nombres de imagenes existentes
				if( strchr( $buf["ADJUNTOS"], ":" ) ) //si existe concatenador, son varias imagenes
					$tmp= explode( ":", $buf["ADJUNTOS"] ); //dividimos
				else //solo es una imagen
					$tmp= $buf["ADJUNTOS"]; //copiamos nombre
					
				//Comparacion de nombres para evitar imagenes duplicadas
				if( strchr($buf["ADJUNTOS"], ":") ) //si existe concatenador, son varias imagenes
					{
					for( $i=0; $i<count($tmp); $i++ )
						{
						if( !strcmp($buf["ADJUNTOS"], $dato) ) //si tienen el mismo nombre
							{
							echo "<td>La imagen <b>". $tmp[$i]. "</b> ya existe, asi que no sera subida al servidor.<br>";
							return 1; //error
							}
						}
					}
				else //solo es una imagen
					{
					if( !strcmp($buf["ADJUNTOS"], $dato) ) //si tienen el mismo nombre
						{
						echo "<td>La imagen <b>". $dato. "</b> ya existe, asi que no sera subida al servidor.<br>";
						return 1; //error
						}
					}
				}
			} 
		}
	
	unset($cons);
	return 0; //exito
	}

	
function rating_download( $muestra )
	{
	$cons= consultar_con( "DOWNLOAD_RATING", "ID='". $muestra. "'" );
	
	if( mysqli_num_rows($cons)==0 )
		{
		unset($cons);
		return 0;
		}
	else
		{
		$buf= mysqli_fetch_array($cons);
		return $buf["RATING"];
		unset($buf);
		unset($cons);
		}
	}

//limpia la cadena dependiendo lo que se desee eliminar
function limpiar_cadena( $cadena, $modo )
	{
	if( !strcmp($modo, "espacios") ) //limpia espacios
		{
		$tmp="";
		for( $i=0; $i<strlen(proteger_cadena($cadena)); $i++ )
			{
			if( strcmp($cadena[$i], " ") ) //si no es un espacio
				$tmp .= $cadena[$i]; //concatenamos
			}
		return $tmp;
		}
	else if( !strcmp($modo, "comas") ) //limpia comas
		{
		$tmp="";
		for( $i=0; $i<strlen(proteger_cadena($cadena)); $i++ )
			{
			if( strcmp($cadena[$i], ",") ) //si no es un espacio
				$tmp .= $cadena[$i]; //concatenamos
			}
		return $tmp;
		}
	}
	
function verificar_correos_grupos( $mail_dest )
	{
	$mails="";
	
	if( strchr( $mail_dest, " ") ) //si existen espacios
		$mail_dest= limpiar_cadena($mail_dest, "espacios" ); //limpiamos espacios

	if( strchr( $mail_dest, ",") ) //si existen comas
		{
		$xmails= explode( ",", $mail_dest ); //dividimos variable
			
		for( $i=0; $i<sizeof($xmails); $i++ )
			{
			if( strcmp( $mails, "" ) )
				$mails .= ",";
					
			if( strchr( $xmails[$i], "@" ) ) //si existe un arroba
				{
				if( !validar_email( $xmails[$i] ) )
					{
					echo 'E-Mail que se intento enviar es invalido.<br>';
					return 0;
					}
				else	$mails .= $xmails[$i];
				}
			else //entonces es un grupo de usuarios
				{
				if( mysqli_num_rows(consultar_con("PRIVILEGIOS", "NOMBRE='". $xmails[$i]. "'"))==0 )
					return 0;
				else
					$mails .= $xmails[$i];
				}
			}
		}
	else //es un unico valor
		{
		if( strchr( $mail_dest, "@" ) ) //si existe un arroba
			{
			if( !validar_email( $mail_dest ) )
				{
				echo 'E-Mail que se intento enviar es invalido.<br>';
				return 0;
				}
			else	$mails .= $mail_dest;
			}
		else //entonces es un grupo de usuarios
			{
			if( mysqli_num_rows(consultar_con("PRIVILEGIOS", "NOMBRE='". $mail_dest. "'"))==0 )
				{
				echo 'No se encuentra el grupo ['. $mail_dest. ']<br>';
				return 0;
				}
			else
				$mails .= $mail_dest;
			}
		}
	return $mails;
	}

function email_tipo_marketing($email_tipo)
	{
	if( !strcmp($email_tipo, "E") ) return 'Empresa';
	else if( !strcmp($email_tipo, "P") ) return 'Persona';
	else if( !strcmp($email_tipo, "P") ) return 'Persona';
	else return '';
	}
	
# funcion para evitar repeticion de envio, retorn un 1 si NO se repite, y un 0 si se repite
function evitar_repeticion_mails( $mails_db, $email )
	{
	if( strstr($mails_db, $email) )
		return 0; //el mail se ha repetido
	return 1; //el mail NO se esta repitiendo
	}

function enviar_correo( $to, $asunto, $modo, $enlace, $adjunto, $from, $log, $link_custom, $replayTo=NULL, $bccTo=NULL, $ccTo=NULL, $onShell=false ) {
	include( "config.json.php" );
	$website= new Config($configJson);

	$subject= desproteger_cadena_src($asunto); //titulo del correo	
	$fl= false;

	if( consultar_datos_general("SMTP_SERVER", "ID_USUARIO='". $_SESSION["SUPERID"]. "'", "ID") ) { # si tiene SMTP personalizado activo
		$mail = new PHPMailer;
		$mail->CharSet = 'utf-8';
	
		$fl= true;
		$cons= consultar_con("SMTP_SERVER", "ID_USUARIO='". $_SESSION["SUPERID"]. "'");
		$buf_smtp= mysqli_fetch_array($cons);
		limpiar($cons);
		unset($cons);
	
		$mail->isSMTP();
		$mail->Subject = $subject;
		$mail->SMTPDebug = 2;
		$mail->Debugoutput = true;
		$mail->Host = $buf_smtp["SMTP_HOST"];
		$mail->Port = $buf_smtp["SMTP_PORT"];
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = (!strcmp($buf_smtp["SMTP_SECURITY"], "1") ? false:get_clienteprofile($buf_smtp["SMTP_SECURITY"], "smtp_security", 0));
		$mail->Username = $buf_smtp["SMTP_USER"];
		$mail->Password = $buf_smtp["SMTP_PASS"];
	
		# copia carbon
		if( $buf_smtp["SMTP_CC"] ) {
			$x= explode(",", $buf_smtp["SMTP_CC"]);
			for( $i=0; $i<count($x); $i++ )
				$mail->addCC(addslashes($x[$i]));
			unset($x, $i);
		}
	
		# copia carbon oculta
		#if( $buf_smtp["SMTP_BCC"] )
		#	{
		#	$x= explode(",", $buf_smtp["SMTP_CC"]);
		#	for( $i=0; $i<count($x); $i++ )
		#		$mail->addBCC(addslashes($x[$i]));
		#	unset($x, $i);
		#	}

		if( $fl ) { # personalizado
			$emisor= $buf_smtp["SMTP_FROM_TEXT"];
			$from_mail= $buf_smtp["SMTP_FROM"];
			$mail->setFrom($from_mail, desproteger_cadena_src($emisor));
		}
		else  {
			$emisor= consultar_datos_general( "USUARIOS", "ID='". $_SESSION["SUPERID"]. "'", "EMPRESA");
			$from_mail= $website->getWebLicenciaMail();
			if( strstr($from, "factura") || strstr($from, "cotizacion") || strstr($from, "oc") || strstr($from, "nomina") || strstr($from, "recibo") || strstr($from, "pagos") )
				$mail->setFrom($from_mail, desproteger_cadena_src($emisor));
			else 	$mail->setFrom($from_mail, $website->getWebTitulo());
		}

		# replay_to - responder a/hacia
		if( $replayTo ) {
			$mail->addReplyTo($replayTo, $emisor);
		}
		else {
			$mail->addReplyTo($from_mail, $emisor);
		}

		# to - para
		if( strstr($to, ",") ) { # si son varios
			$x= explode(",", $to);
			foreach( $x as $key ) {
				if( strstr($key, "<") && strstr($key, ">") ) {
					$patron= '/([a-zA-Z0-9._:\-\s\w]{1,})<([a-zA-Z0-9._@\-\w]{1,})\>/i';
					preg_match_all($patron, $key, $buf, PREG_OFFSET_CAPTURE );
					$mail->addAddress($buf[2][0][0], desproteger_cadena_src($buf[1][0][0]));
					unset($patron, $buf);
				}
				else  {
					$key= str_replace(array(" "), "", $key);
					$mail->addAddress($key);
				}
			}
			unset($x);
		}
		else { # solo uno
			if( strstr($to, "<") && strstr($to, ">") ) {
				$patron= '/([a-zA-Z0-9._:\-\s\w]{1,})<([a-zA-Z0-9._@\-\w]{1,})\>/i';
				preg_match_all($patron, $to, $buf, PREG_OFFSET_CAPTURE );
				$mail->addAddress($buf[2][0][0], desproteger_cadena_src($buf[1][0][0]));
				unset($patron, $buf);
			}
			else 	$mail->addAddress($to);
		}
	}
	else  {
		$mail= new sendinBlue();
		$mail->setSubject($subject);

		# from - de
		if( $fl ) { # personalizado
			$emisor= $buf_smtp["SMTP_FROM_TEXT"];
			$from_mail= $buf_smtp["SMTP_FROM"];
			$mail->setMailFrom($from_mail, desproteger_cadena_src($emisor));
		}
		else  {
			$emisor= consultar_datos_general( "USUARIOS", "ID='". $_SESSION["SUPERID"]. "'", "EMPRESA");
			$emailEmisor= consultar_datos_general( "USUARIOS", "ID='". $_SESSION["SUPERID"]. "'", "EMAIL");
			$from_mail= $website->getWebLicenciaMail();

			if( strstr($from, "factura") || strstr($from, "cotizacion") || strstr($from, "oc") || strstr($from, "nomina") || strstr($from, "recibo") || strstr($from, "pagos") ) {
				$mail->setMailFrom($from_mail, desproteger_cadena_src($emisor));
				# replay_to - responder a/hacia
				if( $replayTo ) {
					$mail->setReplyTo($replayTo, $emisor);
				}
				else if( $emailEmisor ) {
					$mail->setReplyTo($emailEmisor, $emisor);
				}
				else {
					$mail->setReplyTo($from_mail, $emisor);
				}
			}
			else { 
				$mail->setMailFrom($from_mail, $website->getWebTitulo());

				# replay_to - responder a/hacia
				if( $replayTo ) {
					$mail->setReplyTo($replayTo, $emisor);
				}

				# bcc - responder a/hacia
				if( $bccTo ) {
					$mail->setBccTo($bccTo, $emisor);
				}

				# cc - responder a/hacia
				if( $ccTo ) {
					$mail->setCcTo($ccTo, $emisor);
				}
			}
		}

		# to - para
		if( strstr($to, ",") ) { # si son varios
			$x= explode(",", $to);
			foreach( $x as $key ) {
				if( strstr($key, "<") && strstr($key, ">") ) {
					$patron= '/([a-zA-Z0-9._:\-\s\w]{1,})<([a-zA-Z0-9._@\-\w]{1,})\>/i';
					preg_match_all($patron, $key, $buf, PREG_OFFSET_CAPTURE );
					$mail->setMailTo($buf[2][0][0], desproteger_cadena_src($buf[1][0][0]));
					unset($patron, $buf);
				}
				else 	$mail->setMailTo($key);
			}
			unset($x);
		}
		else { # solo uno
			if( strstr($to, "<") && strstr($to, ">") ) {
				$patron= '/([a-zA-Z0-9._:\-\s\w]{1,})<([a-zA-Z0-9._@\-\w]{1,})\>/i';
				preg_match_all($patron, $to, $buf, PREG_OFFSET_CAPTURE );
				$mail->setMailTo($buf[2][0][0], desproteger_cadena_src($buf[1][0][0]));
				unset($patron, $buf);
			}
			else 	$mail->setMailTo($to);
		}
	}

	$html= file_get_contents(($onShell ? CWD:'').'newsletter.html'); # obtenemos el stream del HTML Plantilla
	$html= preg_replace( '/\[EMPRESA\]/', desproteger_cadena_src(SLOGAN), $html );
	$html= preg_replace( '/\[EMPRESA_URL\]/', $website->getWebDevUrl(), $html );
	$html= preg_replace( '/\[BLOG_URL\]/', $website->getWebDevUrl().'/blog/#blog', $html );
	$html= preg_replace( '/\[TERMINOSDEUSO_URL\]/', $website->getWebDevUrl().'/terminos_de_uso/#terminos_de_uso', $html );
	$html= preg_replace( '/\[PRIVACIDAD_URL\]/', $website->getWebDevUrl().'/privacidad/#privacidad', $html );
	$html= preg_replace( '/\[PRIVACIDAD_URL\]/', $website->getWebDevUrl().'/privacidad/#privacidad', $html );
	$html= preg_replace( '/\[TITULO\]/', $asunto, $html );
	$html= preg_replace( '/\[IMG_POST\]/', 'data:image/'. (strstr($log, ".jpg") ? "jpg":"webp"). ';base64,'.base64_encode(file_get_contents($log)), $html );
	$html= preg_replace( '/\[BOTONES\]/', $link_custom, $html );
	$html= preg_replace( '/\[LOGO\]/', 'data:image/'. substr(LOGO_PNG, -3). ';base64,'. base64_encode(file_get_contents(LOGO_PNG)), $html );

	$cuerpo='';

	if( $modo==0 ) { //enviar correo para comentarios
		//Cuerpo o contexto del mensaje, la esencia del correo, el todo ;) 
		$cuerpo .= "Han publicado un nuevo comentario en <b>". $_SERVER['HTTP_HOST']. "</b>.<br>Enlace al tema: ";
		$cuerpo .= "<b></b><a href=\"". $enlace. "\" target=\"_blank\">". $enlace. "</a></b>";
	}
	else if( $modo==1 ) { //error en correo para comentarios
		$trama_deficiente= $_POST;
		//Cuerpo o contexto del mensaje, la esencia del correo, el todo ;) 
		$cuerpo .= "Se produjo un error en el servidor <b>". $_SERVER['HTTP_HOST']. "</b> al interntar enviar aviso de notificacion a las bandejas, ";
		$cuerpo .= "la notificacion se intento enviar en blanco o sin u enlace hacia la noticia donde se publico el comentario.";
		$cuerpo .= "<p>A continuacion de muestra la trama deficiente obtenida: <br>". $trama_deficiente;
		unset($trama_deficiente);
	}
	else if( $modo==2 ) { //enviar correo de nuevo usuario
		$cuerpo .= "Un nuevo usuario se ha registrado al sitio <b>". $_SERVER['HTTP_HOST']. "</b>, los datos del usuario son:";
		$cuerpo .= "<p>Usuario <b>". $enlace. "</b></p>";
	}
	else if( $modo==3 ) { //enviar correo de Envio de Factura Digital al Cliente 
		if( strstr($from, "nominas") ) { # enviando adjunto recibo de nomina
			$fecha= explode(",", $link_custom);	# tomamos lapso fechas de pago
			$idemp= consultar_datos_general("RECIBOS_NOMINA", "ID='". proteger_cadena($adjunto). "'", "ID_EMPLEADO");
			$nombre= consultar_datos_general( "EMPLEADOS", "ID='". $idemp. "'", "NOMBRE"). ' '. consultar_datos_general( "EMPLEADOS", "ID='". $idemp. "'", "APELLIOD_P"). ' '. consultar_datos_general( "EMPLEADOS", "ID='". $idemp. "'", "APELLIOD_M");
			$cuerpo .= 'Estimado <b>'. $nombre. '</b>.<p>Le hacemos envio de su recibo de nomina referente al pago de sus servicios del <b>'. $fecha[0]. '</b> al <b>'. $fecha[1]. '</b>, agradecemos enormemente su compromiso con nuestra empresa.</p><br>Saludos !.';
			# echo '<br>To:'. $to. '<br>File'. $adjunto. '<br>'. $cuerpo. '<br>';
		}
		else if( strstr($from, "cotizacion") ) {
			$id_f= consultar_datos_general( "COTIZACIONES", "ID='". proteger_cadena($adjunto). "'", "ID_FOLIO" ); # obtenemos ID de la factura
			$cuerpo .= 'Hola.<br><br>Nos complace escribile nuevamente, en esta ocasi'. acento("o"). 'n le hacemos env'. acento("i"). 'o de su <b>Cotizaci'. acento("o"). 'n '. $id_f. '</b>, esperamos nuestra propuesta sea lo que usted esta buscando.<br><br>En caso de no poder descargar adjuntos utilice los botones de descarga.<br><br>Gracias por su confianza !';

			unset($id_f);
		}
		else if( strstr($from, "pagos") ) {
			$id_f= consultar_datos_general( "PAGOS", "ID='". proteger_cadena($adjunto). "'", "ID_FOLIO" ); # obtenemos ID de la factura
			$f= 'R - '. $id_f; # factura
			
			$cuerpo .= 'Hola.<br><br>Como sabemos que "el tiempo es oro", te hemos enviado inmediatamente tu <b>Recibo de Pago Electr'. acento("o"). 'nico '. $f. '</b> por adquisici'. acento("o"). 'n que realizo con nosotros.<br><br>En caso que algo raro pase con los adjuntos, puedes usar los botones de descarga indicados mas abajo.<br><br>Gracias por su preferencia !';

			unset($f);
		}
		else if( strstr($from, "notacredito") ) {
			$id_f= consultar_datos_general( "NOTA_CREDITO", "ID='". proteger_cadena($adjunto). "'", "ID_FOLIO" ); # obtenemos ID de la factura
			$f= consultar_datos_general( "FOLIO", "ID='". $id_f. "' && ID_USUARIO='". $_SESSION["SUPERID"]. "'", "SERIE" ). ' - '. consultar_datos_general( "FOLIO", "ID='". $id_f. "'", "FOLIO" ); # factura
			$mailSop= consultar_datos_general( "USUARIOS", "ID='". $_SESSION["SUPERID"]. "'", "EMAIL" );
			
			$cuerpo .= 'Hola.<br><br>Como sabemos que "el tiempo es oro", te hemos enviado inmediatamente tu <b>Nota de Credito '. $f. '</b> por adquisici'. acento("o"). 'n que realizo con nosotros.<br><br>En caso que algo raro pase con los adjuntos, puedes usar los botones de descarga indicados mas abajo o bien refiera cualquier duda a nuestro correo <b>'. $mailSop. '</b><br><br>Gracias por su preferencia !';

			unset($f, $mailSop);
		}
		else {
			$id_f= consultar_datos_general( "FACTURACION", "ID='". proteger_cadena($adjunto). "'", "ID_FOLIO" ); # obtenemos ID de la factura
			$f= consultar_datos_general( "FOLIO", "ID='". $id_f. "' && ID_USUARIO='". $_SESSION["SUPERID"]. "'", "SERIE" ). ' - '. consultar_datos_general( "FOLIO", "ID='". $id_f. "'", "FOLIO" ); # factura
			$mailSop= consultar_datos_general( "USUARIOS", "ID='". $_SESSION["SUPERID"]. "'", "EMAIL" );
			
			$cuerpo .= 'Hola.<br><br>Como sabemos que "el tiempo es oro", te hemos enviado inmediatamente tu <b>Factura '. $f. '</b> por adquisici'. acento("o"). 'n que realizo con nosotros.<br><br>En caso que algo raro pase con los adjuntos, puedes usar los botones de descarga indicados mas abajo o bien refiera cualquier duda a nuestro correo <b>'. $mailSop. '</b><br><br>Gracias por su preferencia !';

			unset($f, $mailSop);
		}
	}
	else if( $modo==4 ) { //enviar correo de recuperacion de datos del usuario
		$cuerpo .= "<b>Sistema de Recuperacion de Datos.</b>";
		$cuerpo .= "<p>Te informamos que hemos obtenido una solicitud de datos de tu cuenta en nuestra pagina <b>". $_SERVER['HTTP_HOST']. "</b>, con motivos de ";
		$cuerpo .= "<b>recuperacion de usuario y password</b> dicha solicitud provino de:";
		$cuerpo .= "<p>IP: <b>". $_SERVER['REMOTE_ADDR']. "</b><br>";
		$cuerpo .= "Nombre Host: <b>";
		//obteniendo nombre del host
		$info= gethostbyaddr($_SERVER['REMOTE_ADDR']);
		if( strcmp( $info, $_SERVER['REMOTE_ADDR']) )
			$cuerpo .= $info;
		else $cuerpo.= "<b>no se pudo obtener</b>";
		unset($info);
			
		$cuerpo .= "</b><br>";
		$cuerpo .= "Fecha: <b>". date( "d/m/y", time() ). " a las ". date( "g:i a", time() ). "</b>";
		$cuerpo .= "<p>Los datos solicitados referentes a tu cuenta son:";
			
		$x= explode( "|", $enlace );
		$cuerpo .= "<p><b>Username: </b>". $x[0];
		$cuerpo .= "<br><b>Password: </b>". $x[1];
		unset($x);
	}
	else if( $modo==5 ) { //enviar correo de Mensaje Privado recivido
		$cuerpo .= "Te escribimos para informarte que has recivido un Mensaje Privado de <b>". $enlace. "</b> en tu cuenta con <b>". $_SERVER['HTTP_HOST']. "</b>, ";
		$cuerpo .= "gracias por preferir nuestra comunidad y esperamos que este <b>servicio informativo</b> te sea de utilidad.";
	}
	else if( $modo==6 ) { //enviar informacion de formulario de CONTACTO
		$cuerpo .='Ha recivido un mensaje del <b>formulario de contacto</b> (https://'. $_SERVER['HTTP_HOST']. '):<p>
		----------<br>'. $enlace. '</p>';
	}
	else if( $modo==7 )
		$cuerpo .= $enlace;
	
	if( consultar_datos_general("SMTP_SERVER", "ID_USUARIO='". $_SESSION["SUPERID"]. "'", "ID") ) { # si tiene SMTP personalizado activo
		$html= preg_replace( '/\[MENSAJE\]/', $cuerpo, $html );
		$mail->msgHTML($html, NULL);
		$mail->AltBody = NULL;
	}
	else  {
		$html= preg_replace( '/\[MENSAJE\]/', $cuerpo, $html );
		$mail->setHtmlContent($html);
	}
		
	//archivos adjuntos
	if( strcmp($adjunto, "0") && strcmp($adjunto, "vacio") && strcmp($adjunto, "") ) {
		# set_time_limit(600);
		# si es factura digital 
		if( !strcmp(consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_SESSION["SUPERID"]). "'", "SERVICIO"), "fd") ) {
			if( strstr($from, "nominas") )	{ # enviando adjunto recibo de nomina
				# $archivo= "clientes/". $_SESSION["SUPERID"]. "/empleados/". consultar_datos_general("RECIBOS_NOMINA", "ID='". proteger_cadena($adjunto). "'", "ID_EMPLEADO"). "/". $adjunto. ".zip";

				$archivo= array( 
					'clientes/'. $_SESSION["SUPERID"]. '/empleados/'. consultar_datos_general("RECIBOS_NOMINA", "ID='". proteger_cadena($adjunto). "'", "ID_EMPLEADO"). '/'. $adjunto. '.xml', 
						'clientes/'. $_SESSION["SUPERID"]. '/empleados/'. consultar_datos_general("RECIBOS_NOMINA", "ID='". proteger_cadena($adjunto). "'", "ID_EMPLEADO"). '/'. $adjunto. '.pdf'
				);

				$adjunto= 'Recibo_'. consultar_datos_general("RECIBOS_NOMINA", "ID='". proteger_cadena($adjunto). "'", "FOLIO");
			}
			else if( strstr($from, "cotizacion") )
				$archivo= 'clientes/'. $_SESSION["SUPERID"]. '/cotizaciones/'. $adjunto. '.pdf';
			else if( strstr($from, "pagos") ) {
				if( !strcmp($_SESSION["pais"], "47") ) { # colombia
					$archivo= array( 'clientes/'. $_SESSION["SUPERID"]. '/pagos/'. $adjunto. '.pdf' );
				}
				else {
					$archivo= array( 'clientes/'. $_SESSION["SUPERID"]. '/pagos/'. $adjunto. '.pdf', 
						'clientes/'. $_SESSION["SUPERID"]. '/pagos/'. $adjunto. '.xml' );
				}
			}
			else if( strstr($from, "notacredito") ) { # notas de credito
				if( !strcmp($_SESSION["pais"], "47") ) { # colombia
					$archivo= array( 'clientes/'. $_SESSION["SUPERID"]. '/notacredito/'. $adjunto. '.pdf', 
						'clientes/'. $_SESSION["SUPERID"]. '/notacredito/'. $adjunto. '.xml', 
						'clientes/'. $_SESSION["SUPERID"]. '/notacredito/'. $adjunto. '_acuse.xml' );
				}
				else {
					$archivo= array( 'clientes/'. $_SESSION["SUPERID"]. '/notacredito/'. $adjunto. '.pdf', 
							'clientes/'. $_SESSION["SUPERID"]. '/notacredito/'. $adjunto. '.xml' );
				}
			}
			else { # facturas
				if( !strcmp($_SESSION["pais"], "47") ) { # colombia
					$archivo= array( 'clientes/'. $_SESSION["SUPERID"]. '/facturas/'. $adjunto. '.zip' );
					# 'clientes/'. $_SESSION["SUPERID"]. '/facturas/'. $adjunto. '.xml', 
					# 'clientes/'. $_SESSION["SUPERID"]. '/facturas/'. $adjunto. '_attached.xml' );
				}
				else  {
					$archivo= array( 'clientes/'. $_SESSION["SUPERID"]. '/facturas/'. $adjunto. '.pdf', 
						'clientes/'. $_SESSION["SUPERID"]. '/facturas/'. $adjunto. '.xml' );
				}
			}
		}
		else if( !strcmp(consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_SESSION["SUPERID"]). "'", "SERVICIO"), "cdb") || !strcmp(consultar_datos_general("USUARIOS", "ID='". proteger_cadena($_SESSION["SUPERID"]). "'", "SERVICIO"), "nota") )
			$archivo= 'clientes/'. $_SESSION["SUPERID"]. '/facturas/'. $adjunto. '.pdf';
		$buf_type= (is_array($archivo) ? 0:obtener_extencion_stream_archivo($archivo)); //obtenemos tipo archivo
			
		if( !is_array($archivo) ) { # si es uno solo
			if( consultar_datos_general("SMTP_SERVER", "ID_USUARIO='". $_SESSION["SUPERID"]. "'", "ID") ) # si tiene SMTP personalizado activo
				$mail->addAttachment($archivo, $adjunto.substr($archivo, -4) );
			else
				$mail->setAttached($adjunto.substr($archivo, -4), file_get_contents($archivo)); /* adjuntando flujo de datos*/
		}
		else { # es arreglo, son varios
			foreach( $archivo as $key ) {
				if( consultar_datos_general("SMTP_SERVER", "ID_USUARIO='". $_SESSION["SUPERID"]. "'", "ID") ) # si tiene SMTP personalizado activo
					$mail->addAttachment($key, $adjunto.substr($key, -4) );
				else
					$mail->setAttached($adjunto.substr($key, -4), file_get_contents($key)); /* adjuntando flujo de datos*/
			}
		}
	}

	if( consultar_datos_general("SMTP_SERVER", "ID_USUARIO='". $_SESSION["SUPERID"]. "'", "ID") ) { # si tiene SMTP personalizado activo
		if (!$mail->send()) {
			echo '['. $mail->ErrorInfo. ']';
			return 0;
		}
		else 	return 1;
	}
	else  {
		$mail->send();
		if( $mail->getError() ) {
			return 0;
		}
		else  {
			$r= $mail->getResponse("array"); /* response json default */
			return $r["code"];
		}
	}
}

# adjunta archivos a un mail en:
# attached - modalidad de adjunto
# inbody	- dentro del cuerpo del mail (se despliega)	
function adjuntar_archivo( $file, $mod, $boundary )
	{
	if( !strcmp($mod, "attached") || !strcmp($mod, "inbody") )
		{
		$fp= fopen( $file, "r" ); //abrimos archivo
		$buf= fread( $fp, filesize($file) ); //leemos archivo completamente
		fclose($fp); //cerramos apuntador;
				
		$data .= "--". $boundary. "\r\n";
		$data .= "Content-Type: ". get_mime_format( $file ). "; name=\"". dump_filename($file). "\"\r\n"; //envio directo de datos
		$data .= "Content-Transfer-Encoding: base64\r\n";
		if( !strcmp($mod, "attached") ) # adjuntar al mail
			$data .= "Content-Disposition: attachment; filename=\"". dump_filename($file). "\"\r\n\r\n";
		else
			{
			$data .= "Content-ID: <". dump_filename($file). "@". $_SERVER['HTTP_HOST']. ">\r\n";
			$data .= "Content-Location: http://". $_SERVER['HTTP_HOST']. "/". $file. "\r\n\r\n";
			}
		$data .= chunk_split(base64_encode($buf)). "\r\n\r\n";
		unset($file, $fp, $boundary, $buf);
		return $data;
		}
	return 0;
	}

function dump_filename($archivo)
	{
	$x= explode( "/", $archivo ); # explotamos 
	return $x[count($x)-1]; # retornamos nombre del archivo 
	}
	
function validar_dominio( $dominio )
	{
	$dom='';
	# si tiene arroba, hay que validar dominio de un correo
	if( strstr( $dominio, "@") ) # si tiene arroba, hay que validar dominio de un correo
		{
		$x= explode("@", $dominio);
		$dom= $x[1]; # tomamos dominio 
		}
	else		$dom= $dominio;
	
	if( gethostbyname($dom) ) # comprobando dominio sin www
		return 1; # exito
	else if( gethostbyname('www.'.$dom) ) # comprobando dominios con www
		return 1;
	return 0;
	}

function validar_email( $mail )
	{
	if( !strstr($mail, "@") ) # si no contiene la arroba
		return 0;
	else
		{
		$validinbox= array("hotmail.com", "gmail.com", "yahoo.com"); #mails comerciales
		$x= explode( "@", $mail );
		
		# comprobaremos si lo que esta la izquierda del @ no tenga caracteres invalidos
		if( !$x[0] )	return 0; # si no existe dato, entonces esta erroneo el mail
		$novalid= array( "|", "/", "\\", ",", "+", "?", "\'", "(", ")", "&", "%", "$", "\"", "@", "!", "~", "{", "}", "[", "]", "<", ">" );
		foreach( $novalid as $key )
			{
			if( strstr($x[0], $key) ) # si tiene el caracter
				return 0; # error
			}
		
		# comprobamos que el dominio tenga si quiera un punto
		if( !strstr($x[1], ".") )		return 0;

		# comprobamos que no sea un proveedor de correo publico conocido 
		foreach($validinbox as $key)
			{
			if( !strcmp($key, $x[1]) )
				return 1;
			}
			
		# comprobamos lo que esta a la derecha del @
		if( !validar_dominio($x[1]) )
			return 0;
		return 1;
		}
	}

function ver_contenido_directorio( $target_dir, $arg, $tipo )
	{
	$abc= "../".$target_dir;
	if( $dir= opendir( $abc ) )
		{
		switch($tipo)
			{
			case 'select':
				if( strcmp($arg, "" ) )
					echo "<option value=\"". $arg. "\">". $arg. " <i>(cargado)</i></option>";
					
				while( ($buf_dir=readdir($dir))!==FALSE )
					{
					if( strcmp( $target_dir.$buf_dir, $arg ) && strcmp( $buf_dir, "." ) && strcmp( $buf_dir, ".." ) )
						echo "<option value=\"". $target_dir.$buf_dir. "\">". $buf_dir. "</option>";
					unset($buf_dir);
					}
				break;
			}
		}
	else
		echo "<option>Error al leer carpeta: ". $target_dir. "...</option>";
	
	closedir($dir);
	}
	
//Funcion que corta el contenido de un mensaje a un numero limitado de caracteres
function cortar_cadena( $msg, $limite )
	{
	$total= strlen($msg); //tomamos longitud del mensaje
	
	if( $total>$limite ) //si el mensaje es mayor al limite establecido
		return substr( $msg, 0, $limite );
	else //retornamos el mensaje
		return $msg;
	}

//Funcion para visualizar imagenes
function imagenes_rollout( $path, $img_nom, $modo )
	{
	/*
	$modo indica la forma de visualizar las imagenes, estan pueden ser:
	
	0 = Solo se visualiza la primer imagen del conjunto de imagenes
	1 = se visualiza imagen por imagen en un solo plano (div)
	2 = se visualiza imagen por imagen es un solo recuadro (efecto rollout)
	*/
	
	$buf= explode( ":", $img_nom );
	
	switch( $modo )
		{
		case 0:
			echo "<img src=\"imagenes/noticias/". $buf[0]. "\">";
			break;
		case 1:
			for( $i=0; $i<count($buf); $i++ )
				echo "<img src=\"imagenes/noticias/". $buf[$i]. "\" border=\"0\" title=\"Click para descargar\" alt=\"Click para descargar\">";
			break;
		case 2:
			break;
		}
	
	unset($buf);
	}

function imprimir_numeros( $inicio, $maximo, $modo )
	{
	switch( $modo )
		{
		case 'select':
			for( $i=$inicio; $i<($maximo+1); $i++ )
				echo "<option value=\"". $i. "\">". $i. "</option>";
			break;
		default:
			for( $i=$inicio; $i<($maximo+1); $i++ )
				echo $i. "<br>";
			break;
		}
	}





 
?>
