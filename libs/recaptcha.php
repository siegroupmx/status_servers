<?php
class googleService {
	const RECAPTCHA_SERVER= 'https://www.google.com/recaptcha/api/siteverify';
	const RECAPTCHA_PUBLIK_KEY= '6Le7-6wkAAAAAHLeOxi5exh9XwSO6_d6FRRH_2LH';
	const RECAPTCHA_SECRET= '6Le7-6wkAAAAAOvn2Ep5Xm7l3SXT_L98j1hoasPb';
	const OTP_BRAND_NAME= 'FacturaElectronica.sv';
	const OTP_SERVICE_NAME= 'status';

	private $recaptchaKey=NULL;
	private $recaptchaSecret=NULL;
	private $headerResponse=NULL;
	private $headerRequest=NULL;
	private $error=NULL;
	private $sucess=NULL;
	private $serviceType=NULL;
	private $otp=NULL;
	private $otpSecret=NULL;
	private $otpQrCode=NULL;
	private $otpBrandName=NULL;
	private $otpServiceName=NULL;
	private $serviceSupport= array(
		"recaptcha", 
		"2factor"
	);

	/**
	* Inicializa el OTP o Google Autenticator para nuevo uso o uso continuo
	* 
	* @param string $a el codigo secreto
	*/
	public function initOTP($secret=NULL, $otpName=NULL, $serviceName=NULL) {
		$this->otp= new \Sonata\GoogleAuthenticator\GoogleAuthenticator();

		if( !$secret ) { // nuevo secreto
			$this->otpSecret= $this->otp->generateSecret();
		}
		else { // cargar secreto actual
			$this->otpSecret= $secret;
		}

		$this->setOtpBrandName($otpName); // nombre del OTP
		$this->setOtpServiceName($serviceName); // nombre del servicio OTP
		$this->setOtpQrCode(); // crea el codigo QR
	}

	/**
	* Devuelve la validacion del codigo de OTP
	* 
	* @return boolean estatus de la validacion
	*/
	public function otpValidate($code=NULL) {
		if( !strcmp($this->getOtpSecret(), $code) ) {
			$this->setSucess("la clave OTP es correcta");
			$this->setError(NULL);
			return true;
		}
		else {
			$this->setError("la clave OTP no es correcta");
			$this->setSucess(NULL);
			return false;
		}
	}

	/**
	* Devuelve el codigo hash secreto para resguardo interno (privado)
	* 
	* @return string el codigo secreto
	*/
	public function getOtpRawSecret() {
		return $this->otpSecret;
	}

	/**
	* Devuelve el codigo esperado para autenticacion
	* 
	* @return string el codigo de autenticacion
	*/
	public function getOtpSecret() {
		return $this->otp->getCode($this->getOtpRawSecret());
	}

	/**
	* Establece la ruta del QR secreto en formato imagen
	*/
	public function setOtpQrCode() {
		$this->otpQrCode= \Sonata\GoogleAuthenticator\GoogleQrUrl::generate($this->getOtpServiceName(), $this->getOtpRawSecret(), $this->getOtpBrandName());
	}

	/**
	* Establece el nombre del otp
	* 
	* @param string $a el nombre del OTP
	*/
	public function setOtpBrandName($a=NULL) {
		$this->otpBrandName= ($a ? $a : self::OTP_BRAND_NAME );
	}

	/**
	* Devuelve el nombre del otp
	* 
	* @return string el nombre del OTP
	*/
	public function getOtpBrandName($a=NULL) {
		return $this->otpBrandName;
	}

	/**
	* Establece el nombre del servicio otp
	* 
	* @param string $a el nombre del servicio OTP
	*/
	public function setOtpServiceName($a=NULL) {
		$this->otpServiceName= ($a ? $a : self::OTP_SERVICE_NAME );
	}

	/**
	* Devuelve el nombre del servicio otp
	* 
	* @return string el nombre del servicio OTP
	*/
	public function getOtpServiceName($a=NULL) {
		return $this->otpServiceName;
	}

	/**
	* Devuelve el codigo QR
	* 
	* @return string codigo QR en forma de URL
	*/
	public function getOtpQrCode() {
		return $this->otpQrCode;
	}

	public function setError($a=NULL) {
		$this->error= $a;
	}

	public function getError() {
		return $this->error;
	}

	public function setSucess($a=NULL) {
		$this->sucess= $a;
	}

	public function getSucess() {
		return $this->sucess;
	}

	public function getRespuesta() {
		return $this->getSucess();
	}

	public function getRecaptchaAPI() {
		return self::RECAPTCHA_SERVER;
	}

	public function setRecaptchaKey($a=NULL) {
		$this->recaptchaKey= ($a ? $a : self::RECAPTCHA_PUBLIK_KEY);
	}

	public function getRecaptchaKey() {
		return $this->recaptchaKey;
	}

	public function setRecaptchaSecret($a=NULL) {
		$this->recaptchaSecret= ($a ? $a : self::RECAPTCHA_SECRET);
	}

	public function getRecaptchaSecret() {
		return $this->recaptchaSecret;
	}

	public function printRecaptchaDiv() {
		return '<div class="g-recaptcha" data-sitekey="'. $this->getRecaptchaKey(). '"></div>';
	}

	public function seralize($a=NULL) {
		$r=NULL;

		if( is_array($a) ) {
			if( count($a) ) {
				$r='';

				foreach( $a as $k=>$v ) {
					$r .= ($r ? '&':''). $k. '='. $v; # serializamos
				}
			}
		}

		return $r;
	}

	public function setRecaptchaPost($a=NULL) {
		$this->recaptchaPost= $a;
	}

	public function getRecaptchaPost() {
		return $this->recaptchaPost;
	}

	public function getHeaderRequest() {
		return $this->headerRequest;
	}

	/**
	* Establece el contenido del Request
	*
	* @param string contenido de variable
	*/
	public function setHeaderRequest($a=NULL) {
		$this->headerRequest= ($a ? $a:NULL);
	}

	/**
	* Retorna contenido de la variable $headerResponse
	*
	* @return string contenido de variable
	*/
	public function getHeaderResponse() {
		return $this->headerResponse;
	}

	/**
	* Establece el contenido del Response
	*
	* @param string contenido de variable
	*/
	public function setHeaderResponse($a=NULL) {
		$this->headerResponse= ($a ? $a:NULL);
	}

	public function setServiceType($a=NULL) {
		$r=false;
		foreach( $this->serviceSupport as $k ) {
			if( !strcmp($a, $k) ) // se encuentra el servicio
				$r= true; // habilitamos
		}
		
		$this->serviceType= ($r ? $a:NULL);
	}

	public function getServiceType() {
		return $this->serviceType;
	}

	public function sendRecaptcha() {
		$headers= array("Content-Type: application/x-www-form-urlencoded");
		$dataSend= $this->seralize(array(
			"secret"=>$this->getRecaptchaSecret(), 
			"response"=>$this->getRecaptchaPost()
		));
		$s= curl_init();
		curl_setopt($s, CURLOPT_URL, $this->getRecaptchaAPI());
		curl_setopt($s, CURLOPT_HTTPHEADER, $headers );
		curl_setopt($s, CURLOPT_HEADER, 1 );
		curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($s, CURLOPT_VERBOSE, true);
		curl_setopt($s, CURLINFO_HEADER_OUT, true);
		curl_setopt($s, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		curl_setopt($s, CURLOPT_POST, 1);
		curl_setopt($s, CURLOPT_POSTFIELDS, $dataSend);
		$resp= curl_exec($s);
		$rq= curl_getinfo($s);
		$this->setHeaderRequest($rq["request_header"]); // request
		$this->setHeaderResponse($resp); // response
		$this->debugResponse();

		curl_close($s);
		unset($rq, $resp, $s);
	}

	public function debugResponse() {
		$r= explode("\r\n", $this->getHeaderResponse());
		$json= json_decode($r[(count($r)-1)]);

		if( !strcmp($this->getServiceType(), "recaptcha") ) {
			// echo '<br>RespuestaServer:<br>';
			// echo '<br>Sucess: '. $json->success;
			// echo '<br>Change: '. $json->challenge_ts;
			// echo '<br>Host: '. $json->hostname;
			// echo '<br>Score: '. $json->score;
			// echo '<br>Accion: '. $json->action;

			if( $json->success==1 ) {
				$this->setError(NULL);
				$this->setSucess("validacion exitosa");
			}
			else {
				$this->setSucess(NULL);
				$this->setError("problemas para validar, score: ". $json->score);
			}
			unset($json, $r);
		}
	}

	/**
	* Constructor principal
	*
	* @param string $postData infomormacion del post que lleva el hash
	* @param string $key la clave
	* @param string $secret el secreto
	*/
	public function __construct($postData=NULL, $key=NULL, $secret=NULL) {
		$this->setRecaptchaKey($key);
		$this->setRecaptchaSecret($secret);
		$this->setRecaptchaPost($postData);
	}
}
?>