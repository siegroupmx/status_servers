<?php
#
# $n= numero
# $c= cantidad digitos a la derecha del punto (centavos o decimales)
# $del= delimitador para los centavos
# $miles= delimitador para los miles o mas...
#
function mbnumber_format( $n, $c, $del='.', $miles=',', $admin=false) {
	$d= explode(".", $n);
	$c= ($admin ? $c:($_SESSION["round_decimals"] ? $_SESSION["round_decimals"]:$c)); # numero de decimales, default: 2

	$centavos='';
	for( $i=0; $i<$c; $i++ ) {
		if( $i<$c )
			$centavos .= (isset($d[1][$i]) ? $d[1][$i]:0);
	}

	if( $_SESSION["round_asyn"] ) {
		$num= round($n).'.00'; # redondeo asincrono, hacia arriba con seteo a 0 (cero) centavos
	}
	else  {
		$num= $d[0].$del.($admin ? $centavos:($_SESSION["round_cero"] ? '':$centavos));
	}
	return number_format($num, $c, $del, $miles);
}

function uso_reload( $tiempo )
	{
	echo '
	<script type="text/javascript">
		setTimeout( function()	{
			location.reload();
		}, '. $tiempo. ');
	</script>
	';
	}

function uso_reloadurl( $url, $tiempo )
	{
	echo '
	<script type="text/javascript">
		setTimeout( function()	{
			document.location.assign(\''. $url. '\');
		}, '. $tiempo. ');
	</script>
	';
	}
	
function uso_redirect( $url, $layer )
	{
	echo '
		<script type="text/javascript">
				setTimeout( function()	{
					cargar_datos( \''. $url. '\', \''. $layer. '\', \'GET\', \'0\');
				}, 2000);
			</script>';
	}

function currentURL()
	{
	return proteger_cadena($_SERVER["REQUEST_URI"]);
	}

function currentScript()
	{
	return proteger_cadena($_SERVER["SCRIPT_NAME"]);
	}

# retorna informacion del sistema
# sys=>sistema/modulo, op=>operacion/accion, p=valor/variable
function get_systeminfo( $data )
	{
	$r=0;

	if( is_array($data) || count($data) )
		{
		if( !strcmp($data["sys"], "traslados") ) # traslados
			{
			if( !strcmp($data["op"], "get_list") ) # obtener lista de traslados
				{
				}
			else if( !strcmp($data["op"], "get_list_novisto") ) # obtener lista de traslados no visto
				{
				$data2=array( "secret"=>0, "data"=>array("sucursal"=>$_SESSION["sucursal"]["id"], "visto"=>"no") );
				$r2= moneybox( "pdv_lvl2/transfer/list", $data2 ); # consultamos sobre el producto
				if( $r2->error_code )
					$r= 0;
				else //exito
					$r= count($r2->result); # contador
				unset($data2, $r2);
				}
			else if( !strcmp($data["op"], "get_list_vistos") ) # obtener lista de traslados vistos
				{
				}
			else if( !strcmp($data["op"], "get_confirm") ) # obtener el que confirma un estatus
				{
				$tmp= $_SESSION["usuarios"]->$data["p"]->nombre;
				if( !$data["p"] )		$r=0;
				else if( !strstr($tmp, "_") )	$r=0;
				else
					{
					$a= explode("_", $tmp);
					$r= $a[1];
					unset($a);
					}
				unset($tmp);
				}
			else if( !strcmp($data["op"], "update_visto") ) # obtener lista de traslados vistos
				{
				if( !$data["p"] )	$r=0;
				else
					{
					$data2=array( "secret"=>0, "data"=>array("sucursal"=>$_SESSION["sucursal"]["id"], "id"=>$data["p"]) );
					$r2= moneybox( "pdv_lvl2/transfer/visto", $data2 ); # consultamos sobre el producto
					unset($data2, $r2);
					}
				}
			}
		}
	return $r;
	}

# determinar tipo de perfil de clientes
function get_clienteprofile( $data, $op, $printmode )
	{
	if( !$data ) return 0;
	
	if( !strcmp($op, "metodopago") ) # metodo de pago
		{
		$arr= array(
			"1"=>"Efectivo", 
			"2"=>"No Identificado", 
			"3"=>"Transferencia Electronica de Fondos", 
			"4"=>"Tarjeta Credito/Debido", 
			"5"=>"Cheque Nominativo"
			);
		}
	else if( !strcmp($op, "moneda_letra") ) {  	# tipo de moneda - solo denominacion
		$arr= array(
			"1"=>"M.N.", 
			"2"=>"US", 
			"3"=>"EUR", 
			"4"=>"PAB", 
			"5"=>"COP", 
			"6"=>"ARS", 
			"7"=>"BOB", 
			"8"=>"CLP", 
			"9"=>"GTQ", 
			"10"=>"PEN", 
			"11"=>"VEF", 
			"12"=>"BTC"
		);
	}
	else if( !strcmp($op, "moneda_deno") ) {	# tipo de moneda - denominacion
		$arr= array(
			"1"=>"PESOS", 
			"2"=>"DOLARES", 
			"3"=>"EUROS", 
			"4"=>"BALBOAS", 
			"5"=>"PESOS", 
			"6"=>"PESOS", 
			"7"=>"BOLIVIANOS", 
			"8"=>"PESOS", 
			"9"=>"QUETZALES", 
			"10"=>"SOLES", 
			"11"=>"BOLIVARES", 
			"12"=>"BITCOINS"
		);
	}
	else if( !strcmp($op, "formato_gentype") ) # tipo de formato para facturas: 1=image, 2=html
		{
		$arr= array(
			"1"=>"Formato Imagen", 
			"2"=>"Formato HTML"
			);
		}
	else if( !strcmp($op, "formato_prenomina") ) # tipo de formato para prenomina
		{
		$arr= array(
			"1"=>"Prenomina", 
			"2"=>"Asistencia", 
			"3"=>"Empleados"
			);
		}
	else if( !strcmp($op, "formato_reportes_gastos") ) # tipo de formato para prenomina
		{
		$arr= array(
			"1"=>"Gastos/Egresos", 
			"2"=>"Orden de Compra", 
			"3"=>"Almacen"
			);
		}
	else if( !strcmp($op, "formato_gastos") ) # tipo de formato para prenomina
		{
		$arr= array(
			"1"=>"Completo", 
			"2"=>"Emisor", 
			"3"=>"Forma de Pago"
			);
		}
	else if( !strcmp($op, "formato_ordencompra") ) # tipo de formato para prenomina
		{
		$arr= array(
			"1"=>"Completo", 
			"2"=>"Por Proveedor", 
			"3"=>"Forma de Pago"
			);
		}
	else if( !strcmp($op, "formato_almacen") ) # tipo de formato para prenomina
		{
		$arr= array(
			"1"=>"Completo", 
			"2"=>"Por Categoria", 
			"3"=>"Por Existencia/Stock", 
			"4"=>"Por ID Producto"
			);
		}
	else if( !strcmp($op, "formato_reportes_facturas") ) # tipo de formato para facturas
		{
		$arr= array(
			"1"=>"Todos", 
			"2"=>"Datos Receptor", 
			"3"=>"Forma de Pago"
			);
		}
	else if( !strcmp($op, "cortenomina") )
		{
		$arr= array(
			"1"=>"Diario", 
			"5"=>"Semanal - 5 dias", 
			"7"=>"Semanal - 7 dias",
			"10"=>"Decena", 
			"14"=>"Catorcenal", 
			"15"=>"Quincenal", 
			"30"=>"Mensual", 
			"60"=>"Bimenstral", 
			"90"=>"Trimestral", 
			);
		}
	else if( !strcmp($op, "cortenomina_xml") )
		{
		$arr= array(
			"1"=>"Diario", 
			"5"=>"Semanal", 
			"7"=>"Semanal",
			"10"=>"Decena", 
			"14"=>"Catorcenal", 
			"15"=>"Quincenal", 
			"30"=>"Mensual", 
			"60"=>"Bimenstral", 
			"90"=>"Trimestral", 
			);
		}
	else if( !strcmp($op, "how_nomigen") )
		{
		$arr= array(
			"1"=>"Automático por Sistema", 
			"2"=>"Usare Checador", 
			"3"=>"Usare XML Personal"
			);
		}
	else if( !strcmp($op, "how_idempleado") )
		{
		$arr= array(
			"1"=>"Numérico", 
			"2"=>"Mixto (Números y Letras)", 
			"3"=>"Manualmente"
			);
		}
	else if( !strcmp($op, "sino") )
		{
		$arr= array(
			"1"=>"Si", 
			"2"=>"No"
			);
		}
	else if( !strcmp($op, "almacen_retry") ) # almacen reenvio de alertas
		{
		$arr= array(
			"1"=>"Diario", 
			"2"=>"Semanal", 
			"3"=>"Mensual"
			);
		}
	else if( !strcmp($op, "almacen_barcodegen") ) # almacen tipo de codigo a generar
		{
		$arr= array(
			"1"=>"No Generar", 
			"2"=>"Codigo de Barras", 
			"3"=>"QR Code", 
			"4"=>"Todos los Formatos"
			);
		}
	else if( !strcmp($op, "almacen_statusprod") ) # almacen estado producto
		{
		$arr= array(
			"1"=>"Nuevo", 
			"2"=>"Seminuevo", 
			"3"=>"Usado"
			);
		}
	else if( !strcmp($op, "almacen_scannercount") ) # almacen contador de unidades escaneadas
		{
		if( !$_SESSION["almacen"]["scanner"] )	$arr=0;
		else
			{
			$arr= array( "cont"=>0 );
			foreach( $_SESSION["almacen"]["codigos"] as $key=>$val)
				$arr["cont"] += $val; # incrementamos
			}
		}
	else if( !strcmp($op, "almacen_db2scanner") ) # almacen de la DB al scanner
		{
		preg_match_all( '/\[([a-zA-Z0-9]{1,})\|([0-9]{1,})\]/is', $data, $aux );
		if( count($aux[0]) )
			{
			foreach( $aux[0] as $key=>$val )
				$arr[$aux[1][$key]]= $aux[2][$key];
			}
		}
	else if( !strcmp($op, "almacen_printcode") ) # almacen tipo hoja impresion etiquetas
		{
		$arr= array(
			"1"=>"Hoja Carta", 
			"2"=>"Hoja Legal (Oficio)", 
			"3"=>"0913", 
			"4"=>"1622", 
			"5"=>"1717", 
			"6"=>"0820", 
			"7"=>"2525", 
			"8"=>"1338", 
			"9"=>"2538", 
			"10"=>"2513", 
			"11"=>"6747", 
			"12"=>"1950", 
			"13"=>"5050", 
			"14"=>"File", 
			"15"=>"0534", 
			"16"=>"1319", 
			"17"=>"1938",
			"18"=>"3264", 
			"19"=>"5050"
			);
		}
	else		# causante mayor o menor
		$arr= array( "1"=>"Causante Menor", "2"=>"Causante Mayor" );

	if( !$printmode )	$r= '0';
	else		$r='';

	foreach( $arr as $key=>$val )
		{
		if( !$printmode ) # sin imprimirse, obtener valor
			{
			if( !strcmp($key, $data ) ) # si es el mismo ID
				$r= $val; # obtenemos valor
			}
		else # imprimirse en variable (concatenar)
			{
			if( !strcmp($printmode, "option") ) # impresion options
				$r .= '<option value="'. $key. '">'. desproteger_cadena($val). '</option>';
			else if( !strcmp($printmode, "list") ) # impresion options
				$r .= '<li>'. desproteger_cadena($val). '</li>';
			else if( !strcmp($printmode, "arrdata") ) # impresion options
				$r[$key]= $val;
			}
		}
	unset($arr, $printmode, $data, $op);
	
	# if( !$printmode )
		return $r;
	}
	
# carga sesiones de conceptos para modificacion de facturas
function init_conceptos( $data )
	{
	if( is_array($data) )	# elementos extras para aduana (cuenta_gastos)
		{
		$r=0;
		if( !strcmp($data[0], "extra2conceptos_extras") ) # extras (aduana y comercializadora)
			{
			if( strstr($data[1], "|") ) # aduana (gastos)
				{
				$x= explode( "|", $data[1]); # todo el buffer
				$y= explode( ",", $x[0] ); # los conceptos
				}
			else		# comercializadora
				$y= explode( ",", $data[1] ); # los conceptos
			
			$r='';
			if( strstr($data[1], "|") ) # es aduana (gastos)
				{
				$dividendo= 4;
				$info=3;
				$cos=2;
				}
			else if( !(count($y)%4) ) # es comercializadora
				{
				$dividendo= 4;
				$info=3;
				$cos=1;
				}

			for( $i=0; $i<floor(count($y)/$dividendo); $i++ )
				$r .= '1|1|'. $y[((($i*$dividendo)+$dividendo)-$info)]. '|'. $y[((($i*$dividendo)+$dividendo)-$cos)]. '|'. ( (strstr($data[1], "|")) ? $y[((($i*$dividendo)+$dividendo)-1)].'|':'');
			unset($x, $y, $dividendo, $info);
			}
		else if( !strcmp($data[0], "extra2conceptos_dos") && strstr($data[1], ",") ) # dos valores (aduana y comercializadora)
			{
			$x= explode( ",", $data[1]); # todo el buffer
			return $x[1];
			}
		else if( !strcmp($data[0], "extra2conceptos_comer") && strstr($data[1], ",") ) # comercializadora
			{
			$x= explode( "|", $data[1]); # todo el buffer
			return $x[1];
			}
		return $r;
		}
	else if( !strstr($data, "|") ) # si no hay delimitador
		return;
	else if( !$data )		return;		# vacio
	else # cargando datos a la session
		{
		if( $_SESSION["conceptos"] )	unset($_SESSION["conceptos"]); # destruimos si existe
		
		$x= explode("|", desproteger_cadena_src($data) );
		for( $i=0; $i<floor(count($x)/5); $i++ ) 
			{
			$_SESSION["conceptos"][$i. '_'.generar_idtrack()]= array( 
				"cantidad"=>$x[((($i*5)+5)-5)], 
				"unidad"=>$x[((($i*5)+5)-4)], 
				"concepto"=>$x[((($i*5)+5)-3)], 
				"pu"=>$x[((($i*5)+5)-2)], 
				"impuesto"=>$x[((($i*5)+5)-1)] 
				);
			}
		return;
		}
	}

# imprime unidades de medida
function get_unidadesmedida( $op, $pais = NULL )
	{
	$unidades= array(
		"1"=>"NA - No Aplica",
		"2"=>"Cm - Centimetros", 
		"3"=> "Ft - Pies", 
		"4"=>"Gal - Galones", 
		"5"=>"Hrs - Horas", 
		"6"=>"In - Pulgadas", 
		"7"=>"Km - Kilometros", 
		"8"=>"Lb - Libras", 
		"9"=>"Lts - Litros", 
		"10"=>"Mi - Millas", 
		"11"=>"Mm - Milimetros", 
		"12"=>"Ms - Minutos", 
		"13"=>"Mts - Metros",  
		"14"=>"Nm - Nanometros", 
		"15"=>"Pza - Piezas", 
		"16"=>"Gr - Gramos", 
		"17"=>"Kg - Kilo gramos"
		);

	$unidadescode= array(
		"1"=>"NA",
		"2"=>"Cm", 
		"3"=> "Ft", 
		"4"=>"Gal", 
		"5"=>"Hrs", 
		"6"=>"In", 
		"7"=>"Km", 
		"8"=>"Lb", 
		"9"=>"Lts", 
		"10"=>"Mi", 
		"11"=>"Mm", 
		"12"=>"Ms", 
		"13"=>"Mts",  
		"14"=>"Nm", 
		"15"=>"Pza", 
		"16"=>"Gr", 
		"17"=>"Kg"
		);
		
	$unidadesElSalvador= array(
		"01"=>"Mts - Metros",
		"02"=>"Yd - Yardas", 
		"03"=>"Vr - Varas", 
		"04"=>"Ft - Pies", 
		"05"=>"In - Pulgadas", 
		"06"=>"Mm - Milimetros", 
		"08"=>"Mi2 - Millas Cuadradas", 
		"09"=>"Km2 - Kilometros cuadrados", 
		"10"=>"Ha - Hectareas", 
		"11"=>"Mz - Manzanas", 
		"12"=>"Ac - Acres", 
		"13"=>"Mts2 - Metros Cuadrados", 
		"14"=>"Yd2 - Yardas Cuadradas",  
		"15"=>"Vr2 - Varas Cuadradas", 
		"16"=>"Ft2 - Pies Cuadrados", 
		"17"=>"In2 - Pulgadas Cuadradas", 
		"18"=>"Mts3 - Metros Cubicos",
		"19"=>"Yd3 - Yardas Cubicas",
		"20"=>"Bar - Barril",
		"21"=>"Ft3 - Pies Cubicos",
		"22"=>"Gal - Galones",
		"23"=>"Lt - Litros",
		"24"=>"Bot - Botellas",
		"25"=>"In3 - Pulgada Cubica",
		"26"=>"Mm - Milimetro"
	);
		
	$unidadescodeElSalvador= array(
		"01"=>"Mts",
		"02"=>"Yd", 
		"03"=>"Vr", 
		"04"=>"Ft", 
		"05"=>"In", 
		"06"=>"Mm", 
		"08"=>"Mi2", 
		"09"=>"Km2", 
		"10"=>"Ha", 
		"11"=>"Mz", 
		"12"=>"Ac", 
		"13"=>"Mts2", 
		"14"=>"Yd2",  
		"15"=>"Vr2", 
		"16"=>"Ft2", 
		"17"=>"In2", 
		"18"=>"Mts3",
		"19"=>"Yd3",
		"20"=>"Bar",
		"21"=>"Ft3",
		"22"=>"Gal",
		"23"=>"Lt",
		"24"=>"Bot",
		"25"=>"In3",
		"26"=>"Mm"
	);
	$unidad = "unidades";
	$unidadC = "unidadescode";
	
	if ($pais) {
		if ($pais == "elsalvador") {
			$unidad = "unidadesElSalvador";
			$unidadC = "unidadescodeElSalvador";
		}
	}
	
	if( is_array($op) )
		{
		if( !strcmp($op[0], "get_number") )		# obtiene numero
			{
			foreach( $$unidad as $key=>$val )
				{
				if( strstr($val, $op[1]) )		return $key;
				}
			}
		else if( !strcmp($op[0], "get_name") )		# obtiene nombre a partir del numero
			{
			foreach( $$unidad as $key=>$val )
				{
				if( !strcmp($key, $op[1]) )		return $val;
				}
			}
		else if( !strcmp($op[0], "get_namecode") )		# obtiene nombre especial a partir del numero
			{
			foreach( $$unidadC as $key=>$val )
				{
				if( !strcmp($key, $op[1]) )		return $val;
				}
			}
		}
	else
		{
		if( !strcmp($op, "print") )		# imprimir
			{
			foreach( $$unidad as $key=>$val )
				echo '<option value="'. $key. '">'. $val. '</option>';
			}
		}
	}

function spanish_daymonth( $dato )
	{
	$mes= array( 
			"january"=>"enero", 
			"february"=>"febrero", 
			"march"=>"marzo", 
			"april"=>"abril", 
			"may"=>"mayo", 
			"june"=>"junio", 
			"july"=>"julio", 
			"august"=>"agosto", 
			"september"=>"septiembre", 
			"october"=>"octubre", 
			"november"=>"noviembre", 
			"december"=>"diciembre"
			);
	$dias= array(
			"monday"=>"lunes", 
			"tuesday"=>"martes", 
			"wednesday"=>"miercoles", 
			"thursday"=>"jueves", 
			"friday"=>"viernes", 
			"saturday"=>"sabado", 
			"sunday"=>"domingo"
			);
	
	$m= strtolower($dato);

	# recorriendo meses	
	foreach($mes as $key=>$val)
		{
		if( !strcmp($key, $m) )
			return $val;
		}

	# recorriendo dias
	foreach($dias as $key=>$val)
		{
		if( !strcmp($key, $m) )
			return $val;
		}
	
	return '';
	}
?>